package com.masterstrokepune.embress.model;

public class Chargesodel {
    String title;
    String amount;
    int type;

    public Chargesodel(String title, String amount, int type) {
        this.title = title;
        this.amount = amount;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
