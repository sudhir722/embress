package com.masterstrokepune.embress.model;

public class UserModel {
    int $id;
    int Pkcustomerid;
    String CustomerName;
    String Consumerno;
    String Address;
    String MobileNo;
    String EmailId;
    float UsedUnits;

    public UserModel(int $id, int pkcustomerid, String customerName, String consumerno, String address, String mobileNo, String emailId, float usedUnits) {
        this.$id = $id;
        Pkcustomerid = pkcustomerid;
        CustomerName = customerName;
        Consumerno = consumerno;
        Address = address;
        MobileNo = mobileNo;
        EmailId = emailId;
        UsedUnits = usedUnits;
    }

    public int get$id() {
        return $id;
    }

    public void set$id(int $id) {
        this.$id = $id;
    }

    public int getPkcustomerid() {
        return Pkcustomerid;
    }

    public void setPkcustomerid(int pkcustomerid) {
        Pkcustomerid = pkcustomerid;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getConsumerno() {
        return Consumerno;
    }

    public void setConsumerno(String consumerno) {
        Consumerno = consumerno;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public float getUsedUnits() {
        return UsedUnits;
    }

    public void setUsedUnits(float usedUnits) {
        UsedUnits = usedUnits;
    }
}
