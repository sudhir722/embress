package com.masterstrokepune.embress.model;

import java.util.List;

public class TicketList {

    String $id;
    int PkTicketDetailsId;
    String CategoryName;
    String Queryexplanation;
    String date;
    List<ComplaintModel> complaintModelList;
    String status;


    public TicketList(String $id, int pkTicketDetailsId, String categoryName, String queryexplanation, String date) {
        this.$id = $id;
        PkTicketDetailsId = pkTicketDetailsId;
        CategoryName = categoryName;
        Queryexplanation = queryexplanation;
        this.date = date;
    }




    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ComplaintModel> getComplaintModelList() {
        return complaintModelList;
    }

    public void setComplaintModelList(List<ComplaintModel> complaintModelList) {
        this.complaintModelList = complaintModelList;
    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getPkTicketDetailsId() {
        return PkTicketDetailsId;
    }

    public void setPkTicketDetailsId(int pkTicketDetailsId) {
        PkTicketDetailsId = pkTicketDetailsId;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getQueryexplanation() {
        return Queryexplanation;
    }

    public void setQueryexplanation(String queryexplanation) {
        Queryexplanation = queryexplanation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
