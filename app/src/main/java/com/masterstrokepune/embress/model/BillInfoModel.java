package com.masterstrokepune.embress.model;

public class BillInfoModel {

    String name;
    String cource;
    int status;
    String call;

    public BillInfoModel(String name, String cource, int status, String call) {
        this.name = name;
        this.cource = cource;
        this.status = status;
        this.call = call;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCource() {
        return cource;
    }

    public void setCource(String cource) {
        this.cource = cource;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }
}
