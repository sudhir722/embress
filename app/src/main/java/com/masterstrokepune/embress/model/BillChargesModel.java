package com.masterstrokepune.embress.model;

public class BillChargesModel {
    int pkbillvoucherentryid;
    String BillDate;
    double previousreading;
    String previousreadingdate;
    double currentreading;
    double consumedunits;
    double rate;
    double servicecharges;
    String currentreadingdate;
    String previousbal;
    String latefeecharges;
    String gst;
    String Chequebouncecharges;
    double PkReceiptNo;


    public double getPkReceiptNo() {
        return PkReceiptNo;
    }

    public void setPkReceiptNo(double pkReceiptNo) {
        PkReceiptNo = pkReceiptNo;
    }

    public int getPkbillvoucherentryid() {
        return pkbillvoucherentryid;
    }

    public void setPkbillvoucherentryid(int pkbillvoucherentryid) {
        this.pkbillvoucherentryid = pkbillvoucherentryid;
    }

    public String getBillDate() {
        return BillDate;
    }

    public void setBillDate(String billDate) {
        BillDate = billDate;
    }

    public double getPreviousreading() {
        return previousreading;
    }

    public void setPreviousreading(double previousreading) {
        this.previousreading = previousreading;
    }

    public String getPreviousreadingdate() {
        return previousreadingdate;
    }

    public void setPreviousreadingdate(String previousreadingdate) {
        this.previousreadingdate = previousreadingdate;
    }

    public double getCurrentreading() {
        return currentreading;
    }

    public void setCurrentreading(double currentreading) {
        this.currentreading = currentreading;
    }

    public double getConsumedunits() {
        return consumedunits;
    }

    public void setConsumedunits(double consumedunits) {
        this.consumedunits = consumedunits;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getServicecharges() {
        return servicecharges;
    }

    public void setServicecharges(double servicecharges) {
        this.servicecharges = servicecharges;
    }

    public String getCurrentreadingdate() {
        return currentreadingdate;
    }

    public void setCurrentreadingdate(String currentreadingdate) {
        this.currentreadingdate = currentreadingdate;
    }

    public String getPreviousbal() {
        return previousbal;
    }

    public void setPreviousbal(String previousbal) {
        this.previousbal = previousbal;
    }

    public String getLatefeecharges() {
        return latefeecharges;
    }

    public void setLatefeecharges(String latefeecharges) {
        this.latefeecharges = latefeecharges;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getChequebouncecharges() {
        return Chequebouncecharges;
    }

    public void setChequebouncecharges(String chequebouncecharges) {
        Chequebouncecharges = chequebouncecharges;
    }
}
