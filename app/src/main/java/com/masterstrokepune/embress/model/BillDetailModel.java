package com.masterstrokepune.embress.model;

import java.io.Serializable;

public class BillDetailModel implements Serializable {

    int PkBillvoucherEntryId;
    String CustomerUserNo;
    String VoucherDate;
    double BillDueAmount;
    double ReceiptAmount;
    String Receiptdate;
    float UsedUnits;
    String Staus;
    double previousreading;
    double currentreading;
    long PkReceiptNo;




    public BillDetailModel(int pkBillvoucherEntryId, String customerUserNo, String voucherDate, double billDueAmount, float usedUnits, String staus) {
        PkBillvoucherEntryId = pkBillvoucherEntryId;
        CustomerUserNo = customerUserNo;
        VoucherDate = voucherDate;
        BillDueAmount = billDueAmount;
        UsedUnits = usedUnits;
        Staus = staus;
    }

    public double getReceiptAmount() {
        return ReceiptAmount;
    }

    public void setReceiptAmount(double receiptAmount) {
        ReceiptAmount = receiptAmount;
    }

    public String getReceiptdate() {
        return Receiptdate;
    }

    public void setReceiptdate(String receiptdate) {
        Receiptdate = receiptdate;
    }

    public double getPreviousreading() {
        return previousreading;
    }

    public void setPreviousreading(double previousreading) {
        this.previousreading = previousreading;
    }

    public double getCurrentreading() {
        return currentreading;
    }

    public void setCurrentreading(double currentreading) {
        this.currentreading = currentreading;
    }

    public long getPkReceiptNo() {
        return PkReceiptNo;
    }

    public void setPkReceiptNo(long pkReceiptNo) {
        PkReceiptNo = pkReceiptNo;
    }

    public int getPkBillvoucherEntryId() {
        return PkBillvoucherEntryId;
    }

    public void setPkBillvoucherEntryId(int pkBillvoucherEntryId) {
        PkBillvoucherEntryId = pkBillvoucherEntryId;
    }

    public String getCustomerUserNo() {
        return CustomerUserNo;
    }

    public void setCustomerUserNo(String customerUserNo) {
        CustomerUserNo = customerUserNo;
    }

    public String getVoucherDate() {
        return VoucherDate;
    }

    public void setVoucherDate(String voucherDate) {
        VoucherDate = voucherDate;
    }

    public double getBillDueAmount() {
        return BillDueAmount;
    }

    public void setBillDueAmount(double billDueAmount) {
        BillDueAmount = billDueAmount;
    }

    public void setUsedUnits(float usedUnits) {
        UsedUnits = usedUnits;
    }

    public float getUsedUnits() {
        return UsedUnits;
    }

    public String getStaus() {
        return Staus;
    }

    public void setStaus(String staus) {
        Staus = staus;
    }
}
