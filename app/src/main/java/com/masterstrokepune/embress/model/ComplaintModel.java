package com.masterstrokepune.embress.model;

import java.io.Serializable;

public class ComplaintModel implements Serializable {
    String $id;
    String CustomerName;
    String CustomerUserNo;
    String Ticketsolution;
    String StatusName;
    String CategoryName;
    int PkTicketDetailsId;
    String Queryexplanation;
    int Flag;

    public ComplaintModel(String $id, String customerName, String customerUserNo, String ticketsolution, String statusName, String categoryName, int pkTicketDetailsId, String queryexplanation) {
        this.$id = $id;
        CustomerName = customerName;
        CustomerUserNo = customerUserNo;
        Ticketsolution = ticketsolution;
        StatusName = statusName;
        CategoryName = categoryName;
        PkTicketDetailsId = pkTicketDetailsId;
        Queryexplanation = queryexplanation;
    }

    public int getFlag() {
        return Flag;
    }

    public void setFlag(int flag) {
        Flag = flag;
    }

    public String get$id() {
        return $id;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public String getCustomerUserNo() {
        return CustomerUserNo;
    }

    public String getTicketsolution() {
        return Ticketsolution;
    }

    public String getStatusName() {
        return StatusName;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public int getPkTicketDetailsId() {
        return PkTicketDetailsId;
    }

    public String getQueryexplanation() {
        return Queryexplanation;
    }
}
