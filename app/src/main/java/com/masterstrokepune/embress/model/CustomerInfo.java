package com.masterstrokepune.embress.model;

public class CustomerInfo {
    int Pkcustomerid;
    String CustomerName;
    String Consumerno;
    String Address;
    String MobileNo;
    String EmailId;
    float UsedUnits;

    public int getPkcustomerid() {
        return Pkcustomerid;
    }

    public void setPkcustomerid(int pkcustomerid) {
        Pkcustomerid = pkcustomerid;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getConsumerno() {
        return Consumerno;
    }

    public void setConsumerno(String consumerno) {
        Consumerno = consumerno;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public float getUsedUnits() {
        return UsedUnits;
    }

    public void setUsedUnits(float usedUnits) {
        UsedUnits = usedUnits;
    }
}
