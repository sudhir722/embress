package com.masterstrokepune.embress.model;

import java.io.Serializable;

public class CustomerModel implements Serializable {
    
    int AreaId;
    
    String AreaName;
    int projectId;
    String Projectname;
    String flatno;
    String CustomerUserNo;
    String status;

    public CustomerModel(int areaId, String areaName, int projectId, String projectname, String flatno, String customerUserNo, String status) {
        AreaId = areaId;
        AreaName = areaName;
        this.projectId = projectId;
        Projectname = projectname;
        this.flatno = flatno;
        CustomerUserNo = customerUserNo;
        this.status = status;
    }

    public int getAreaId() {
        return AreaId;
    }

    public void setAreaId(int areaId) {
        AreaId = areaId;
    }

    public String getAreaName() {
        return AreaName;
    }

    public void setAreaName(String areaName) {
        AreaName = areaName;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectname() {
        return Projectname;
    }

    public void setProjectname(String projectname) {
        Projectname = projectname;
    }

    public String getFlatno() {
        return flatno;
    }

    public void setFlatno(String flatno) {
        this.flatno = flatno;
    }

    public String getCustomerUserNo() {
        return CustomerUserNo;
    }

    public void setCustomerUserNo(String customerUserNo) {
        CustomerUserNo = customerUserNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
