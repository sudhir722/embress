package com.masterstrokepune.embress.model;

public class CategoryModel {
    String $id;
    String CategoryName;

    public CategoryModel(String $id, String categoryName) {
        this.$id = $id;
        CategoryName = categoryName;
    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }
}
