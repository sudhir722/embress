package com.masterstrokepune.embress.model;

import android.net.Uri;

public class DocInfo {
    String $id;
    int PkDocumentId;
    String DocumentName;
    Uri imagePath;
    String DocumentUrl;
    boolean isUploaded;

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setUploaded(boolean uploaded) {
        isUploaded = uploaded;
    }

    public Uri getImagePath() {
        return imagePath;
    }

    public void setImagePath(Uri imagePath) {
        this.imagePath = imagePath;
    }

    public String getDocumentUrl() {
        return DocumentUrl;
    }

    public void setDocumentUrl(String documentUrl) {
        DocumentUrl = documentUrl;
    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getPkDocumentId() {
        return PkDocumentId;
    }

    public void setPkDocumentId(int pkDocumentId) {
        PkDocumentId = pkDocumentId;
    }

    public String getDocumentName() {
        return DocumentName;
    }

    public void setDocumentName(String documentName) {
        DocumentName = documentName;
    }
}
