package com.masterstrokepune.embress.bs;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.util.LocalConstants;


public class BottomSheetErrorDialogFragment extends BottomSheetDialogFragment {
    String message,title;
    OnDialogClickListener mListener;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            message = getArguments().getString(LocalConstants.CONST_MESSAGE);
            title = getArguments().getString(LocalConstants.CONST_TITLE);
            if(mListener==null)
                mListener = (OnDialogClickListener) getArguments().getSerializable(LocalConstants.CONST_LISTENER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_bottom_sheet_errordialog, container, false);
        ((TextView) view.findViewById(R.id.dialog_title)).setText(title);
        ((TextView) view.findViewById(R.id.dialog_message)).setText(message);

        view.findViewById(R.id.bs_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null){
                    mListener.onOkClicked(null);
                }
                getDialog().dismiss();
            }
        });
        return view;
    }
}
