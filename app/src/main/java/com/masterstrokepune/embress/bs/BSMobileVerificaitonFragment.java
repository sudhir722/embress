package com.masterstrokepune.embress.bs;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.masterstrokepune.embress.BaseBottomSheet;
import com.masterstrokepune.embress.ConnectionKycStatusActivity;
import com.masterstrokepune.embress.NewCustomerActivity;
import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.request.MobileVerificationRequest;
import com.masterstrokepune.embress.retrofit.response.ConnectionResponse;
import com.masterstrokepune.embress.retrofit.response.GenericResponse;
import com.masterstrokepune.embress.util.EmbracePref;
import com.masterstrokepune.embress.util.LocalConstance;
import com.masterstrokepune.embress.util.Utility;

import static com.masterstrokepune.embress.util.LocalConstance.ACTION_OK;
import static com.masterstrokepune.embress.util.LocalConstance.IS_LOYOUT;


public class BSMobileVerificaitonFragment extends BaseBottomSheet implements RetrofitServiceListener {
    String message, title;
    EditText edt_mobileno;

    OnDialogClickListener mListener;
    RetrofitServiceListener mServiceListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            message = getArguments().getString(LocalConstance.CONST_MESSAGE);
            title = getArguments().getString(LocalConstance.CONST_TITLE);
            mListener = (OnDialogClickListener) getArguments().getSerializable(LocalConstance.CONST_LISTENER);
        }
        mServiceListener = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.bs_mobile_verificaiotn_dialog, container, false);
        edt_mobileno = (EditText) view.findViewById(R.id.edt_mobileno);

        view.findViewById(R.id.action_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    if(TextUtils.isEmpty(edt_mobileno.getText().toString())){
                        getDialog().dismiss();
                        Utility.showErrorMessageSheet(getChildFragmentManager(),"Alert","Please Enter Valid Mobile Number");
                    }else {
                        MobileVerificationRequest request = new MobileVerificationRequest(edt_mobileno.getText().toString());
                        RetrofitService.getInstance(getContext()).getMobileVerificationStatus(request,mServiceListener);;
                        //startActivity(new Intent(getContext(), NewCustomerActivity.class));
                    }
                }
                //
            }
        });
        return view;
    }


    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < 16) {
                    view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
                FrameLayout bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
                BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                //behavior.setPeekHeight(0); // Remove this line to hide a dark background if you manually hide the dialog.
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null && context instanceof OnDialogClickListener) {
            mListener = (OnDialogClickListener) context;
        }
    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
        if(mObject instanceof String){
            String response = (String) mObject;
            if(response.equalsIgnoreCase("Mobile Number Not Found")){
                /*GenericResponse mResponse = new GenericResponse(IS_LOYOUT, "logout");
                mListener.onOkClicked(mResponse);*/
                EmbracePref.setMobileNo(getContext(),edt_mobileno.getText().toString());
                getActivity().startActivity(new Intent(getContext(), NewCustomerActivity.class));
                getActivity().finish();
            }else {
                getDialog().dismiss();
               showErrorAlert("Alert",response);
            }
        }else if(mObject instanceof ConnectionResponse){
            EmbracePref.setMobileNo(getContext(),edt_mobileno.getText().toString());
            ConnectionResponse connectionResponse = (ConnectionResponse) mObject;
            Intent intent = new Intent(getContext(), ConnectionKycStatusActivity.class);
            intent.putExtra("data",connectionResponse);
            startActivity(intent);
            getDialog().dismiss();
        }

    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
        getDialog().dismiss();
        EmbracePref.setMobileNo(getContext(),edt_mobileno.getText().toString());
        if(mObject instanceof String) {
            String response = (String) mObject;
            Utility.showErrorMessageSheet(getChildFragmentManager(),"Error",response);

        }
        getActivity().startActivity(new Intent(getContext(), NewCustomerActivity.class));
        getActivity().finish();
    }
}