package com.masterstrokepune.embress;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.adapter.TicketDetailsAdapter;
import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.iface.onClick;
import com.masterstrokepune.embress.model.ComplaintModel;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.request.AddCommantRequest;
import com.masterstrokepune.embress.util.EmbracePref;

import java.util.List;

public class TicketDetails extends BaseActivity implements onClick , RetrofitServiceListener {

    private RecyclerView eventRecycler;
    TicketDetailsAdapter eventAdapter;
    List<ComplaintModel> modelList;
    TextView txt_customer_name,desc,date,section,txt_categoty;
    Button action_status;
    String mDate;
    ImageView sendButton;
    EditText msg;
    int mTicketId;
    String description;
    RetrofitServiceListener mServiceListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_view);
        mServiceListener = this;
        eventRecycler = findViewById(R.id.chat_list);
        action_status = findViewById(R.id.action_status);
        txt_customer_name = findViewById(R.id.txt_customer_name);
        msg = findViewById(R.id.input_edit_text);
        txt_categoty = findViewById(R.id.txt_categoty);
        sendButton = findViewById(R.id.sendbutton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(msg.getText().toString())){
                    showErrorMessageSheet(getSupportFragmentManager(),"Alert","Please Enter your comments");
                }else{
                    AddCommantRequest request = new AddCommantRequest(mTicketId,
                            msg.getText().toString(),1,
                            EmbracePref.getCustomerId(getApplicationContext()),1);
                    RetrofitService.getInstance(getApplicationContext()).addComments(request,mServiceListener);
                }
            }
        });
        desc = findViewById(R.id.desc);
        date = findViewById(R.id.date);
        section = findViewById(R.id.section);
        if(getIntent()!=null && getIntent().getExtras().containsKey("list")){
            modelList = (List<ComplaintModel>) getIntent().getExtras().getSerializable("list");
            mDate =  getIntent().getExtras().getString("date");
            mTicketId =  getIntent().getExtras().getInt("tid");
            description =  getIntent().getExtras().getString("extras");
        }
        desc.setText(description);
        txt_customer_name.setText(EmbracePref.getDisplayName(getApplicationContext()));
        if(modelList!=null) {
            txt_customer_name.setText(modelList.get(0).getCustomerName());
            desc.setText("");
            section.setText("");
            date.setText(mDate);
            String status = modelList.get(modelList.size() - 1).getStatusName();
            switch (status) {
                case "Open":
                    action_status.setText("OPEN");
                    action_status.setBackgroundColor(getResources().getColor(R.color.open));
                    action_status.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "Closed":
                    action_status.setText("Closed");
                    action_status.setBackgroundColor(getResources().getColor(R.color.closed));
                    action_status.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "Forward":
                    action_status.setText("Forward");
                    action_status.setBackgroundColor(getResources().getColor(R.color.repoen));
                    action_status.setTextColor(getResources().getColor(R.color.white));
                    break;
            }
            init();
        }
    }

    public void init(){
        eventAdapter = new TicketDetailsAdapter(getApplicationContext(),modelList,this);
        eventRecycler.setAdapter(eventAdapter);
        eventRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        eventAdapter.notifyDataSetChanged();
    }

    @Override
    public void onOkClicked(Object object) {

    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
        showErrorMessageSheet(getSupportFragmentManager(), "Success", "Ticket has been updated", false, new OnDialogClickListener() {
                    @Override
                    public void onOkClicked(Object object) {
                        Intent in =new Intent(getApplicationContext(),HomeScreen.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(in);
                    }

                    @Override
                    public void onCancelClicked() {

                    }
                }
        );
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
    }
}
