package com.masterstrokepune.embress;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.masterstrokepune.embress.bs.BottomSheetErrorDialogFragment;
import com.masterstrokepune.embress.util.LocalConstants;


public class BaseBottomSheet extends BottomSheetDialogFragment {
    ProgressDialog mProgressBar = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void showMessageNoInternet() {
        showErrorMessageSheet(getFragmentManager(), getString(R.string._title_alert), getString(R.string.msg_no_internet));
    }

    protected void showErrorMessage(String message) {
        showErrorMessageSheet(getFragmentManager(), getString(R.string._title_alert), message);
    }

    public static void showErrorMessageSheet(FragmentManager manager, String title, String message) {
        BottomSheetErrorDialogFragment bottomSheetFragment = new BottomSheetErrorDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LocalConstants.CONST_TITLE, title);
        bundle.putString(LocalConstants.CONST_MESSAGE, message);
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(manager, bottomSheetFragment.getTag());
    }

    /**
     * Inilise Dialog
     * */
    private void initDialog(){
        if(isAdded()) {
            mProgressBar = new ProgressDialog(getContext());
            mProgressBar.setCancelable(true);//you can cancel it by pressing back button
        }
    }
    /**
     * Show Error Message
     * */
    public void showErrorAlert(String title, String message){
        if(isAdded()) {
            if (builder == null) {
                initAlertDialog();
            }
            builder.setTitle(title).setMessage(message).show();
        }
    }


    AlertDialog.Builder builder;
    /**
     * Alert Dialog not doing anything in onclick listener
     * */
    private void initAlertDialog(){
        if(isAdded()) {
            builder = new AlertDialog.Builder(getContext());
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            // Create the AlertDialog object and return it
            builder.create();
        }
    }

    /**
     * Show Progress Dialog
     * */
    public void showProgressDialog(String message){
        if(isAdded()) {
            if (mProgressBar == null) {

                initDialog();
            }
            mProgressBar.setMessage(message);
            mProgressBar.show();
        }
    }
    /**
     * hide Progress Dialog
     * */
    public void hideDialog(){
        if(isAdded()) {
            if (mProgressBar != null && mProgressBar.isShowing()) {
                mProgressBar.dismiss();
            }
        }
    }


}
