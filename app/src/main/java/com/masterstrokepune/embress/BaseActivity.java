package com.masterstrokepune.embress;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentManager;

import com.masterstrokepune.embress.bs.BSMobileVerificaitonFragment;
import com.masterstrokepune.embress.bs.BottomSheetDialogFragment;
import com.masterstrokepune.embress.bs.BottomSheetErrorDialogFragment;
import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.util.LocalConstants;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class BaseActivity extends AppCompatActivity {
    ProgressDialog mProgressBar = null;
    public TextView mTitle;

    public String KEY_BG_COLOR = "bg_color";
    public String KEY_TITLE = "title";
    public String KEY_EXTRAS = "extras";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public void openHelpFile(){

        String fileName = "EmbraceHelpFile.pdf";
        AssetManager assetManager = getAssets();

        InputStream in = null;
        OutputStream out = null;
        File file = new File(getFilesDir()+"/"+fileName, "");
        try {
            in = assetManager.open(fileName);
            out = openFileOutput(file.getName(), MODE_PRIVATE);

            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("TAG", e.getMessage());
        }
        try {
            File mFile = new File(getCacheDir(), "EmbraceHelpFile.pdf");

            Uri uri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName()+".provider", mFile);
            //File mFile = new File(getFilesDir()+"/EmbraceHelpFile.pdf");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(uri, "application/pdf");
            startActivity(intent);
        }catch (RuntimeException ex){
            Toast.makeText(getApplicationContext(), "There's no PDF Reader found in your device", Toast.LENGTH_SHORT).show();
        }
        /**/
    }

    public void getIsValidUser(Boolean isValid){
        
    }

    public boolean isPermited(String permission)
    {
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }


    private void changeStatusBarColor(String color){
        if (!TextUtils.isEmpty(color) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }

    public void changeStatusBarColor(int color){
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    public void showPopup(View v) {
        /*PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_example, popup.getMenu());
        popup.show();*/
    }

    public void changeStatusBarColor(Toolbar toolbar){
        if(getIntent()!=null && getIntent().getExtras()!=null && getIntent().getExtras().containsKey(KEY_BG_COLOR)){
            String bgColor = getIntent().getExtras().getString(KEY_BG_COLOR);
            if(mTitle!=null)
                mTitle.setText(getIntent().getExtras().getString(KEY_TITLE));
            bgColor = "#039BE5";
            if(TextUtils.isEmpty(bgColor)) {
                changeStatusBarColor("#FDEDEC");
            }else{
                changeStatusBarColor(bgColor);
                //Toolbar toolbar = findViewById(R.id.toolbar);
                toolbar.setBackgroundColor(Color.parseColor(bgColor));
            }
        }else{
            changeStatusBarColor("#039BE5");
        }
    }

    /**
     * Inilise Dialog
     * */
    private void initDialog(){
        if(!isFinishing()) {
            mProgressBar = new ProgressDialog(this);
            mProgressBar.setCancelable(false);//you can cancel it by pressing back button
        }
    }
    /**
     * Show Error Message
     * */
    public void showErrorAlert(String title, String message){
        if(!isFinishing()) {
            if (builder == null) {
                initAlertDialog();
            }
            builder.setTitle(title).setMessage(message).show();
        }
    }

    /**
     * Show Error Message
     * */
    public void showAlertMessage(String message){
        if(!isFinishing()) {
            if (builder == null) {
                initAlertDialog();
            }
            builder.setTitle("Alert").setMessage(message).show();
        }
    }


    AlertDialog.Builder builder;
    /**
     * Alert Dialog not doing anything in onclick listener
     * */
    private void initAlertDialog(){
        if(!isFinishing()) {
            builder = new AlertDialog.Builder(this);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            // Create the AlertDialog object and return it
            builder.create();
        }
    }

    /**
     * Show Progress Dialog
     * */
    public void showProgressDialog(String message){
        if(!isFinishing()) {
            if (mProgressBar == null) {
                initDialog();
            }
            mProgressBar.setMessage(message);
            if(!mProgressBar.isShowing())
                mProgressBar.show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * hide Progress Dialog
     * */
    public void hideDialog(){
        if(!isFinishing()) {
            if (mProgressBar != null && mProgressBar.isShowing()) {
                mProgressBar.dismiss();
            }
        }
    }

    public static int getPermissionStatus(Activity mActivity, int status, String permission) {

        if (mActivity != null) {
            if (status == PackageManager.PERMISSION_GRANTED) {
                return 0;
            } else if (status == PackageManager.PERMISSION_DENIED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission)) {
                    //Show permission explanation dialog...
                    return -1;
                } else {
                    //Never ask again selected, or device policy prohibits the app from having that permission.
                    //So, disable that feature, or fall back to another situation...

                    return -2;
                }
            }
        } else if (mActivity != null) {
            if (ContextCompat.checkSelfPermission(mActivity, permission) == PackageManager.PERMISSION_GRANTED) {
                return 0;
            } else {
                //Do the stuff that requires permission...
                return -1;
            }
        }
        return -3;
    }

    protected void showMessageNoInternet() {
        showErrorMessageSheet(getSupportFragmentManager(), getString(R.string._title_alert), getString(R.string.msg_no_internet));
    }

    public static void showErrorMessageSheet(FragmentManager manager, String title, String message) {
        BottomSheetErrorDialogFragment bottomSheetFragment = new BottomSheetErrorDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LocalConstants.CONST_TITLE, title);
        bundle.putString(LocalConstants.CONST_MESSAGE, message);
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(manager, bottomSheetFragment.getTag());
    }

    protected void showMessage(String title, String message,final OnDialogClickListener listener) {
        showErrorMessageSheet(getSupportFragmentManager(), title, message, false, new OnDialogClickListener() {
            @Override
            public void onOkClicked(Object obj) {
                listener.onOkClicked(obj);
            }

            @Override
            public void onCancelClicked() {
                listener.onCancelClicked();
            }
        });
    }

    protected void showErrormessageAndFinish(String message) {
        showErrorMessageSheet(getSupportFragmentManager(), "Alert", message, false, new OnDialogClickListener() {
            @Override
            public void onOkClicked(Object obj) {
                finish();
            }

            @Override
            public void onCancelClicked() {
                finish();
            }
        });
    }

    protected void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }


    public static void showErrorMessageSheet(FragmentManager manager, String title, String message, boolean isCancelable, OnDialogClickListener mListener) {
        BottomSheetErrorDialogFragment bottomSheetFragment = new BottomSheetErrorDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LocalConstants.CONST_TITLE, title);
        bundle.putString(LocalConstants.CONST_MESSAGE, message);

        bundle.putSerializable("listener", mListener);
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.setCancelable(isCancelable);
        bottomSheetFragment.show(manager, bottomSheetFragment.getTag());
    }
    public static void showMobileVerificaiton(FragmentManager manager,  OnDialogClickListener mListener) {
        BSMobileVerificaitonFragment bottomSheetFragment = new BSMobileVerificaitonFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LocalConstants.CONST_TITLE, "");
        bundle.putString(LocalConstants.CONST_MESSAGE, "");

        bundle.putSerializable("listener", mListener);
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.setCancelable(true);
        bottomSheetFragment.show(manager, bottomSheetFragment.getTag());
    }
    public static void showSuccessMessageSheet(FragmentManager manager, String title, String message, boolean isCancelable, OnDialogClickListener mListener) {
        BottomSheetDialogFragment bottomSheetFragment = new BottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LocalConstants.CONST_TITLE, title);
        bundle.putString(LocalConstants.CONST_MESSAGE, message);

        bundle.putSerializable("listener", mListener);
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.setCancelable(isCancelable);
        bottomSheetFragment.show(manager, bottomSheetFragment.getTag());
    }
}
