package com.masterstrokepune.embress;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.masterstrokepune.embress.bs.BottomSheetErrorDialogFragment;
import com.masterstrokepune.embress.util.LocalConstants;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static android.content.Context.MODE_PRIVATE;


public class BaseFragment extends Fragment {
    ProgressDialog mProgressBar = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public void openHelpFile(){

        String fileName = "EmbraceHelpFile.pdf";
        AssetManager assetManager = getContext().getAssets();

        InputStream in = null;
        OutputStream out = null;
        File file = new File(getContext().getFilesDir()+"/"+fileName, "");
        try {
            in = assetManager.open(fileName);
            out = getContext().openFileOutput(file.getName(), MODE_PRIVATE);

            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("TAG", e.getMessage());
        }
        try {
            File mFile = new File(getContext().getCacheDir(), "EmbraceHelpFile.pdf");

            Uri uri = FileProvider.getUriForFile(getContext().getApplicationContext(), getContext().getPackageName()+".provider", mFile);
            //File mFile = new File(getFilesDir()+"/EmbraceHelpFile.pdf");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(uri, "application/pdf");
            startActivity(intent);
        }catch (RuntimeException ex){
            Toast.makeText(getContext(), "There's no PDF Reader found in your device", Toast.LENGTH_SHORT).show();
        }
        /**/
    }

    protected void showMessageNoInternet() {
        showErrorMessageSheet(getFragmentManager(), getString(R.string._title_alert), getString(R.string.msg_no_internet));
    }

    protected void showErrorMessage(String message) {
        showErrorMessageSheet(getFragmentManager(), getString(R.string._title_alert), message);
    }

    public static void showErrorMessageSheet(FragmentManager manager, String title, String message) {
        BottomSheetErrorDialogFragment bottomSheetFragment = new BottomSheetErrorDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LocalConstants.CONST_TITLE, title);
        bundle.putString(LocalConstants.CONST_MESSAGE, message);
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(manager, bottomSheetFragment.getTag());
    }

    /**
     * Inilise Dialog
     * */
    private void initDialog(){
        if(isAdded()) {
            mProgressBar = new ProgressDialog(getContext());
            mProgressBar.setCancelable(true);//you can cancel it by pressing back button
        }
    }
    /**
     * Show Error Message
     * */
    public void showErrorAlert(String title, String message){
        if(isAdded()) {
            if (builder == null) {
                initAlertDialog();
            }
            builder.setTitle(title).setMessage(message).show();
        }
    }


    AlertDialog.Builder builder;
    /**
     * Alert Dialog not doing anything in onclick listener
     * */
    private void initAlertDialog(){
        if(isAdded()) {
            builder = new AlertDialog.Builder(getContext());
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            // Create the AlertDialog object and return it
            builder.create();
        }
    }

    /**
     * Show Progress Dialog
     * */
    public void showProgressDialog(String message){
        if(isAdded()) {
            if (mProgressBar == null) {

                initDialog();
            }
            mProgressBar.setMessage(message);
            mProgressBar.show();
        }
    }
    /**
     * hide Progress Dialog
     * */
    public void hideDialog(){
        if(isAdded()) {
            if (mProgressBar != null && mProgressBar.isShowing()) {
                mProgressBar.dismiss();
            }
        }
    }


}
