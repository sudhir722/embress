package com.masterstrokepune.embress;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.response.ForgotPasswordResponse;

public class ForgotPasswordActivity extends BaseActivity implements RetrofitServiceListener {

    EditText userName;
    EditText mail;
    RetrofitServiceListener mServiceListener;
    RadioButton rbnUser,rbnEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_forgot);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            changeStatusBarColor(getColor(R.color.app_theme_color));
        }
        mServiceListener = this;
        userName = findViewById(R.id.editTextName);
        mail = findViewById(R.id.editTextEmail);
        rbnEmail = findViewById(R.id.rbn_mail);
        rbnUser = findViewById(R.id.rbn_user);
        rbnEmail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mail.setVisibility(View.VISIBLE);
                    userName.setVisibility(View.GONE);
                }else{
                    mail.setVisibility(View.GONE);
                    userName.setVisibility(View.VISIBLE);
                }
            }
        });
        rbnUser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    mail.setVisibility(View.VISIBLE);
                    userName.setVisibility(View.GONE);
                }else{
                    mail.setVisibility(View.GONE);
                    userName.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    public boolean isValidForm(){
        if(TextUtils.isEmpty(userName.getText().toString()) && TextUtils.isEmpty(mail.getText().toString())){
            showErrorMessageSheet(getSupportFragmentManager(),"Alert","PLease Enter User Name or Email id to recover your password..");
            return false;
        }
        return true;
    }
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(getResources().getColor(R.color.white));
        }
    }

    public void onLoginClick(View view){
        startActivity(new Intent(this,LoginActivity.class));
        //overridePendingTransition(R.anim.slide_in_left,android.R.anim.slide_out_right);

    }

    public void onReset(View view) {
        if(isValidForm()){
            if(rbnUser.isChecked()){
                RetrofitService.getInstance(getApplicationContext()).forgotPassword(userName.getText().toString(),mServiceListener);
            }else{
                RetrofitService.getInstance(getApplicationContext()).forgotUserName(mail.getText().toString(),mServiceListener);
            }
        }
    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

    @Override
    public void onResponse(Object mObject) {
        if(mObject instanceof ForgotPasswordResponse){
            ForgotPasswordResponse response = (ForgotPasswordResponse) mObject;
            if(response!=null){
                showErrormessageAndFinish(response.getMsg());
            }else{
                showErrormessageAndFinish("Unable to reach server, Please try again later..");
            }
        }
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {

    }
}