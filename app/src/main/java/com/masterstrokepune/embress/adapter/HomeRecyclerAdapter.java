package com.masterstrokepune.embress.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.iface.onMenuClicked;
import com.masterstrokepune.embress.model.MenuInfo;

import java.util.List;

public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.ViewHolder>{

    private List<MenuInfo> mMenu;
    private LayoutInflater mInflater;
    private onMenuClicked mClickListener;

    // data is passed into the constructor
    public HomeRecyclerAdapter(Context context,List<MenuInfo> data,onMenuClicked listener) {
        this.mInflater = LayoutInflater.from(context);
        this.mMenu = data;
        this.mClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.grid_cell, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.myTextView.setText(mMenu.get(position).getTitle());
        holder.img.setBackgroundResource(mMenu.get(position).getBackgoundIconPath());
        holder.contenar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onMenuClick(mMenu.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMenu.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;
        ImageView img;
        LinearLayout contenar;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.title);
            img = itemView.findViewById(R.id.img);
            contenar = itemView.findViewById(R.id.contenar);

        }

    }
}
