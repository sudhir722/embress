package com.masterstrokepune.embress.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.iface.onClick;
import com.masterstrokepune.embress.model.ComplaintModel;
import com.masterstrokepune.embress.model.TicketList;

import java.util.Date;
import java.util.List;


public class ComplaintHistoryAdapter extends RecyclerView.Adapter<ComplaintHistoryAdapter.EventHolder> {

    Context context;
    List<TicketList> modelList;
    onClick onItemClickListener;
    public static int IS_ENROLLED =1;
    public static String ACTION_LIVE ="LIVE";
    public static String ACTION_COMPLETED ="COMPLETED";
    public static String ACTION_UPCOMING ="Scheduled";
    public static String ACTION_CANCELED ="CANCELED";
    public static String ACTION_RESCHEUDE ="RE-SCHEDULE";

    public ComplaintHistoryAdapter(Context context, List<TicketList> list, onClick listener) {
        this.context = context;
        this.modelList = list;
        this.onItemClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public ComplaintHistoryAdapter.EventHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new EventHolder(inflater.inflate(R.layout.complaint_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull EventHolder holder, final int position) {
        final TicketList event = modelList.get(position);
        holder.txtTitle.setText(event.getCategoryName());
        if(event.getComplaintModelList()!=null) {
            holder.txtCustName.setText(event.getComplaintModelList().get(0).getCustomerName());
        }else{
            holder.txtCustName.setText("-");
        }
        holder.section.setVisibility(View.GONE);
        holder.txtDesc.setText(event.getQueryexplanation());
        holder.txtDate.setText(event.getDate());
        Date currDate = new Date();
//        Date endDate = Utility.getDateTime(event.getMeetingdate()+" "+event.getEndtime());
//        Date startDate = Utility.getDateTime(event.getMeetingdate()+" "+event.getStarttime());
//        String diff = "";
//        if(endDate!=null && startDate!=null) {
//            if (!Utility.getDateFormat(startDate).equalsIgnoreCase(Utility.getDateFormat(endDate))) {
//                diff = Utility.getDateFormat(startDate) + " " + Utility.getTime(startDate) + " to " + Utility.getDateFormat(endDate) + " " + Utility.getTime(endDate) + " (" + Utility.getTimeDifference(startDate, endDate) + ")";
//            } else {
//                diff = Utility.getDateFormat(startDate) + " " + Utility.getTime(startDate) + " to " + Utility.getTime(endDate) + " (" + Utility.getTimeDifference(startDate, endDate) + ")";
//
//            }
//            holder.txtDuration.setText(diff);
//        }
        /*if(startDate.after(currentDate) && endDate.before(currentDate)){
            event.setStatus(ACTION_LIVE);
        }else if(currentDate.after(endDate)){
            event.setStatus(ACTION_COMPLETED);
        }else if(!Utility.getDate(currentDate).equalsIgnoreCase(Utility.getDate(endDate))){
            event.setStatus("COMPLETED");
        }*/
        if(event.getStatus()!=null) {
            switch (event.getStatus()) {
                case "Open":
                    holder.btnStatus.setText("OPEN");
                    holder.btnStatus.setBackgroundColor(context.getResources().getColor(R.color.open));
                    holder.btnStatus.setTextColor(context.getResources().getColor(R.color.white));
                    break;
                case "Closed":
                    holder.btnStatus.setText("Closed");
                    holder.btnStatus.setBackgroundColor(context.getResources().getColor(R.color.closed));
                    holder.btnStatus.setTextColor(context.getResources().getColor(R.color.white));
                    break;
                case "Forward":
                    holder.btnStatus.setText("Forward");
                    holder.btnStatus.setBackgroundColor(context.getResources().getColor(R.color.repoen));
                    holder.btnStatus.setTextColor(context.getResources().getColor(R.color.white));
                    break;
            }
            holder.btnStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                /*if(!holder.btnApprove.getText().toString().equalsIgnoreCase("COMPLETED")) {
                    onItemClickListener.onSchedule(position, event);
                }*/
                }
            });

            holder.btnStatus.setVisibility(View.VISIBLE);
        }else{
            holder.btnStatus.setVisibility(View.GONE);
        }


        holder.contenar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onOkClicked(event);
            }
        });



    }

    class EventHolder extends RecyclerView.ViewHolder{
        TextView txtTitle;
        TextView section;
        TextView txtCustName;
        TextView txtDesc;
        TextView txtDate;
        Button btnStatus;
        ImageView action_ppt;
        LinearLayout contenar;

        public EventHolder(View view) {
            super(view);
            contenar = view.findViewById(R.id.contenar);
            txtTitle = view.findViewById(R.id.txt_categoty);
            section = view.findViewById(R.id.section);
            txtCustName = view.findViewById(R.id.txt_customer_name);
            txtDesc = view.findViewById(R.id.desc);
            txtDate = view.findViewById(R.id.date);
            btnStatus = view.findViewById(R.id.action_status);
        }

    }
}
