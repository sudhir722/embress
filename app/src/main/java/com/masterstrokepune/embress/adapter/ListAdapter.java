package com.masterstrokepune.embress.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.iface.onClick;
import com.masterstrokepune.embress.model.BillDetailModel;
import com.masterstrokepune.embress.model.BillInfoModel;

import java.util.List;


public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private Context mContext;
    List<BillDetailModel> mEnquiryList;

    onClick mListener;

    public ListAdapter(Context ctx, List<BillDetailModel> list, onClick listener) {
        inflater = LayoutInflater.from(ctx);
        this.mContext = ctx;
        this.mEnquiryList = list;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {

        View view = inflater.inflate(R.layout.user_row, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final BillDetailModel model = mEnquiryList.get(position);
        holder.title.setText(String.valueOf(model.getBillDueAmount()));
        holder.desc.setText(model.getVoucherDate());
        holder.txt_status.setText(model.getStaus());
        holder.txt_usedunit.setText(model.getUsedUnits()+"");

        if(model.getStaus().equalsIgnoreCase("Bill Paid")) {
            //holder.status.setBackgroundResource(R.drawable.circle_bg_close);
            holder.bill_status.setBackgroundResource(R.drawable.ic_paid);
        }else  {
            //holder.status.setBackgroundResource(R.drawable.circle_bg_active);
            holder.bill_status.setBackgroundResource(R.drawable.ic_not_paid);
        }
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null){
                    mListener.onOkClicked(model);
                }
            }
        });

    }



    public void updateAllData() {
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mEnquiryList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title,desc,txt_status,txt_usedunit;
        ImageView bill_status;
        LinearLayout container;

        public MyViewHolder(View itemView) {
            super(itemView);
            //status = itemView.findViewById(R.id.status);
            bill_status = itemView.findViewById(R.id.bill_status);
            title = itemView.findViewById(R.id.txt_header);
            txt_status = itemView.findViewById(R.id.txt_status);
            txt_usedunit = itemView.findViewById(R.id.txt_usedunit);
            desc = itemView.findViewById(R.id.txt_desc);
            container = itemView.findViewById(R.id.container);

        }

    }

}
