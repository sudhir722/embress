package com.masterstrokepune.embress.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.iface.ItemClickListener;
import com.masterstrokepune.embress.model.DocInfo;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DocUploadRecyclerAdapter extends RecyclerView.Adapter<DocUploadRecyclerAdapter.MyViewHolder>{

    private List<DocInfo> documnetInfoList;
    Context mContext;
    ItemClickListener mListener;

    public DocUploadRecyclerAdapter(Context context, List<DocInfo> documnetInfoList, ItemClickListener mDocUploadListener) {
        this.documnetInfoList = documnetInfoList;
        this.mContext = context;
        this.mListener = mDocUploadListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        if(viewType==0) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.doc_upload_row, parent, false);
        }else{
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.doc_row, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        DocInfo docInfo = documnetInfoList.get(position);
        //docInfo.setPosition(position);
        holder.title.setText(docInfo.getDocumentName());
        if(TextUtils.isEmpty(docInfo.getDocumentUrl())){
            if(!docInfo.isUploaded()) {
                holder.desc.setText("Upload " + docInfo.getDocumentName() + ". Make sure picture is not blurry otherwise you need to upload again.");
                holder.actionUpload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onItemClickListener(docInfo);
                    }
                });
            }else{
                holder.icon.setImageURI(docInfo.getImagePath());
            }

        }else{
            Picasso.get().load(docInfo.getDocumentUrl()).into(holder.icon);
        }

    }

    @Override
    public int getItemCount() {
        return documnetInfoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(documnetInfoList.get(position).isUploaded() || !TextUtils.isEmpty(documnetInfoList.get(position).getDocumentUrl())){
            return 1;
        }else{
            return 0;
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc;
        RelativeLayout actionUpload;
        ImageView icon;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.desc);
            actionUpload = (RelativeLayout) view.findViewById(R.id.rel_upload);
            icon = (ImageView) view.findViewById(R.id.icon);

        }
    }
}
