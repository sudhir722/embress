package com.masterstrokepune.embress.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.iface.ItemClickListener;
import com.masterstrokepune.embress.model.CustomerModel;

import java.util.List;

public class ConnectionRequestRecyclerAdapter extends RecyclerView.Adapter<ConnectionRequestRecyclerAdapter.MyViewHolder>{

    private List<CustomerModel> requestModelList;
    Context mContext;
    ItemClickListener mListener;

    public ConnectionRequestRecyclerAdapter(Context context, List<CustomerModel> requestModel, ItemClickListener itemClickListener) {
        this.requestModelList = requestModel;
        this.mContext = context;
        this.mListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.kyc_request_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        CustomerModel model = requestModelList.get(position);
        //docInfo.setPosition(position);
        holder.title.setText(model.getFlatno());
        holder.area.setText(model.getAreaName());
        holder.project.setText(model.getProjectname());
        holder.status.setText(model.getStatus());
        holder.action_set_user.setVisibility(View.GONE);
        if(model.getStatus().equalsIgnoreCase("Success")){
            holder.action_set_user.setVisibility(View.VISIBLE);
            holder.action_set_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null){
                    mListener.onItemClickListener(model);
                }
            }
        });
            holder.status.setBackgroundResource(R.color.status_success);
        }else if(model.getStatus().equalsIgnoreCase("Pending")){
            holder.action_set_user.setVisibility(View.GONE);
            holder.status.setBackgroundResource(R.color.status_pending);
        }else if(model.getStatus().equalsIgnoreCase("Reject")){
            holder.status.setBackgroundResource(R.color.status_fail);
            holder.action_set_user.setVisibility(View.GONE);
        }





    }

    @Override
    public int getItemCount() {
        return requestModelList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, project,area,status,action_set_user;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.txt_flatno);
            project = (TextView) view.findViewById(R.id.project);
            area = (TextView) view.findViewById(R.id.area);
            status = (TextView) view.findViewById(R.id.status);
            action_set_user = (TextView) view.findViewById(R.id.action_set_user);

        }
    }
}
