package com.masterstrokepune.embress.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.model.Chargesodel;
import com.masterstrokepune.embress.util.LocalConstance;

import java.util.List;

public class BillChargesAdapter extends RecyclerView.Adapter<BillChargesAdapter.ItemViewHolder> {
    Context mContext;
    List<Chargesodel> mModelList;

    public class ItemViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        TextView txtTitle;
        TextView txtAmount;

        public ItemViewHolder(View convertView) {
            super(convertView);
            this.txtTitle = (TextView) convertView.findViewById(R.id.txt_detail);
            this.txtAmount = (TextView) convertView.findViewById(R.id.txt_amount);

        }

        public void onClick(View view) {
        }
    }

    public BillChargesAdapter(Context context, List<Chargesodel> modelList) {
        this.mContext = context;
        this.mModelList = modelList;
    }

    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LocalConstance.VIEW_TYPE_CELL) {
            return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_detils, parent, false));
        }else if (viewType == LocalConstance.VIEW_TYPE_EVEN) {
            return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_detils_white, parent, false));
        }

        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_headers, parent, false));
    }


    public int getItemViewType(int position) {
        /*if (((Chargesodel) this.mModelList.get(position)).getType() == LocalConstant.VIEW_TYPE_SPAING) {
            return LocalConstant.VIEW_TYPE_SPAING;
        }*/
        if (((Chargesodel) this.mModelList.get(position)).getType() == LocalConstance.VIEW_TYPE_HEADER) {
            return LocalConstance.VIEW_TYPE_HEADER;
        }
        if (((Chargesodel) this.mModelList.get(position)).getType() == LocalConstance.VIEW_TYPE_EVEN) {
            return LocalConstance.VIEW_TYPE_EVEN;
        }
        return LocalConstance.VIEW_TYPE_CELL;
    }

    public void onBindViewHolder(ItemViewHolder holder, int position) {
        String str = "SELECT";
        try {
            holder.txtTitle.setText(this.mModelList.get(position).getTitle());
            holder.txtAmount.setText(this.mModelList.get(position).getAmount());
//            if (getItemViewType(position) == LocalConstance.VIEW_TYPE_CELL) {
//
//                holder.txt_question.setText(((FacilitatorsModels) this.mModelList.get(position)).getQuestion());
//                holder.txtCoad.setText(((FacilitatorsModels) this.mModelList.get(position)).getCode());
//            } else if (getItemViewType(position) == LocalConstant.VIEW_TYPE_HEADER) {
//                holder.txtHeader.setText(((FacilitatorsModels) this.mModelList.get(position)).getHeader());
//                holder.txtTerm.setText(((FacilitatorsModels) this.mModelList.get(position)).getTerm());
//            } else if (getItemViewType(position) == LocalConstant.VIEW_TYPE_MESSAGE) {
//                holder.txtmessage.setText(((FacilitatorsModels) this.mModelList.get(position)).getHeader());
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getItemCount() {
        return this.mModelList.size();
    }
}
