package com.masterstrokepune.embress.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.iface.onClick;
import com.masterstrokepune.embress.model.ComplaintModel;
import com.masterstrokepune.embress.model.TicketList;

import java.util.Date;
import java.util.List;


public class TicketDetailsAdapter extends RecyclerView.Adapter<TicketDetailsAdapter.EventHolder> {

    Context context;
    List<ComplaintModel> modelList;
    onClick onItemClickListener;
    public static int IS_ENROLLED =1;
    public static String ACTION_LIVE ="LIVE";
    public static String ACTION_COMPLETED ="COMPLETED";
    public static String ACTION_UPCOMING ="Scheduled";
    public static String ACTION_CANCELED ="CANCELED";
    public static String ACTION_RESCHEUDE ="RE-SCHEDULE";

    int TYPE_SENDER;
    int TYPE_RECEIVER;

    public TicketDetailsAdapter(Context context, List<ComplaintModel> list, onClick listener) {
        this.context = context;
        this.modelList = list;
        this.onItemClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public TicketDetailsAdapter.EventHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if(viewType == TYPE_SENDER)
            return new EventHolder(inflater.inflate(R.layout.receiver, parent, false));
        else{
            return new EventHolder(inflater.inflate(R.layout.sender, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous

        return modelList.get(position).getFlag();
    }


    @Override
    public void onBindViewHolder(@NonNull EventHolder holder, final int position) {
        final ComplaintModel event = modelList.get(position);
        holder.sender_text_view.setText(event.getCustomerName());
        holder.message_text_view.setText(event.getTicketsolution());
        holder.timestamp_text_view.setText(event.getStatusName());
        //holder.txtDate.setText(event.getDate());

    }

    class EventHolder extends RecyclerView.ViewHolder{
        TextView sender_text_view,message_text_view,timestamp_text_view;


        public EventHolder(View view) {
            super(view);
            sender_text_view = view.findViewById(R.id.sender_text_view);
            message_text_view = view.findViewById(R.id.message_text_view);
            timestamp_text_view = view.findViewById(R.id.timestamp_text_view);

        }

    }
}
