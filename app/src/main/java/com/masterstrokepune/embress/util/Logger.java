package com.masterstrokepune.embress.util;

import android.util.Log;

import com.masterstrokepune.embress.BuildConfig;


public class Logger {

    public static final String TAG = "zee_";

    public static void d(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG + tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG + tag, msg);
        }
    }

}
