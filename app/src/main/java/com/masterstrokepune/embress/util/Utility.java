package com.masterstrokepune.embress.util;

import android.os.Bundle;

import androidx.fragment.app.FragmentManager;

import com.masterstrokepune.embress.bs.BottomSheetErrorDialogFragment;

import java.text.DecimalFormat;

public class Utility {
    private static DecimalFormat df2 = new DecimalFormat("#.##");

    public static String formatDouble(double value){
        return df2.format(value);
    }

    public static double formatDouble(String value){
        double val = Double.parseDouble(value);
        return val;
    }

    public static void showErrorMessageSheet(FragmentManager manager, String title, String message) {
        BottomSheetErrorDialogFragment bottomSheetFragment = new BottomSheetErrorDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LocalConstants.CONST_TITLE, title);
        bundle.putString(LocalConstants.CONST_MESSAGE, message);
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(manager, bottomSheetFragment.getTag());
    }
}
