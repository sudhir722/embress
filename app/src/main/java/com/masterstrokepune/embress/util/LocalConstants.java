package com.masterstrokepune.embress.util;

public class LocalConstants
{

    public static final String CONST_STORETITLE = "title";
    public static final String CONST_TITLE = "title";
    public static final String CONST_MESSAGE = "message";
    public static final String CONST_LISTENER = "listener";

}
