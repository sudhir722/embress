package com.masterstrokepune.embress;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.adapter.MenuAdapter;
import com.masterstrokepune.embress.bs.BSLogoutDialogFragment;
import com.masterstrokepune.embress.fragments.BillHistoryFragment;
import com.masterstrokepune.embress.fragments.DashboardFragment;
import com.masterstrokepune.embress.fragments.ProfileFragment;
import com.masterstrokepune.embress.fragments.SmartReadingFragment;
import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.iface.onMenuClicked;
import com.masterstrokepune.embress.model.MenuInfo;
import com.masterstrokepune.embress.model.MenuModel;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.response.GenericResponse;
import com.masterstrokepune.embress.util.EmbracePref;
import com.masterstrokepune.embress.util.LocalConstance;

import java.util.ArrayList;
import java.util.List;

import static com.masterstrokepune.embress.util.LocalConstance.IS_LOYOUT;


public class DashboardActivity extends BaseActivity implements onMenuClicked, RetrofitServiceListener, OnDialogClickListener, PopupMenu.OnMenuItemClickListener {

    onMenuClicked mMenuClickListener;
    List<MenuInfo> mFilterList = new ArrayList<>();
    MenuAdapter mFilterAdapter;

    ImageView icon;

    public  void rotateImage(){
        RotateAnimation rotate = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(5000);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setInterpolator(new LinearInterpolator());

        icon.startAnimation(rotate);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        icon = findViewById(R.id.action_home);
        rotateImage();

        mMenuClickListener = this;

        ((TextView)findViewById(R.id.footer)).setText(BuildConfig.VERSION_NAME);
        ((TextView)findViewById(R.id.usertype)).setText(EmbracePref.getDisplayName(this));
//        ((TextView)findViewById(R.id.usertype)).setText(EmbracePref.getUserTypeName(this));

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setHomeScreen();
            }
        });
        //initMenu();

       setHomeScreen();
        startActivity(new Intent(getApplicationContext(),SmartMeterActivity.class));
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        popup.setOnMenuItemClickListener(this);
        inflater.inflate(R.menu.menu_main, popup.getMenu());
        popup.show();
    }


    public void setHomeScreen(){
        DashboardFragment fragmentDemo = new DashboardFragment();
        //above part is to determine which fragment is in your frame_container
        setFragment(fragmentDemo);
        if(mFilterList!=null){
            for (int index=0;index<mFilterList.size();index++){
                mFilterList.get(index).setSelection(false);
            }
        }
        if(mFilterAdapter!=null){
            mFilterAdapter.notifyDataSetChanged();
        }
    }



    // This could be moved into an abstract BaseActivity
    // class for being re-used by several instances
    protected void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.home_fragment, fragment);
        fragmentTransaction.commit();
    }


    public static final int MENU_HOME = 0;
    public static final int MENU_HISTORY = 1;
    public static final int MENU_READING = 2;
    public static final int MENU_PROFILE = 3;


//    public void initMenu(){
//        if(mFilterList==null){
//            mFilterList = new ArrayList<>();
//        }
//        mFilterList.clear();
//        mFilterList.add(new MenuInfo("HOME","#fcf3cf", "#fcf3cf",R.drawable.ic_home, MENU_HOME));
//        mFilterList.add(new MenuInfo("History","#d1f2eb", "#d1f2eb",R.drawable.ic_history, MENU_HISTORY));
//        mFilterList.add(new MenuInfo("Smart Reading","#E8F5E9", "#E8F5E9",R.drawable.ic_meter, MENU_READING));
//        mFilterList.add(new MenuInfo("Profile","#ebdef0", "#ebdef0",R.drawable.ic_profile, MENU_PROFILE));
//
//
//        RecyclerView mRecycler = findViewById(R.id.rec_menu);
//        mFilterAdapter = new MenuAdapter(getApplicationContext(), mFilterList, mMenuClickListener);
//        mRecycler.setAdapter(mFilterAdapter);
//        /*FilterAdapter adapter = new FilterAdapter(getContext(), mFilterList,null);
//        mRecycler.setAdapter(adapter);*/
//
//        mRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//    }

    private List<MenuModel> getAllMenus() {
        List<MenuModel> list = new ArrayList<>();
        return list;
    }


    public void showLogOutDialog(){
        BSLogoutDialogFragment bottomSheetFragment = new BSLogoutDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LocalConstance.CONST_TITLE, getString(R.string.app_name));
        bundle.putString(LocalConstance.CONST_MESSAGE, getString(R.string.txt_logout_message));
        bundle.putSerializable(LocalConstance.CONST_LISTENER, this);
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onMenuClick(MenuInfo filtersInfo) {
        /*if(filtersInfo.getMenuId()!=MENU_ENQUIRY_NEW) {
            if(mFilterList!=null && mFilterList.size()>0) {
                for (int index=0;index<mFilterList.size();index++) {
                    if(mFilterList.get(index).getMenuId() == filtersInfo.getMenuId()) {
                        mFilterList.get(index).setSelection(true);
                        mFilterAdapter.notifyDataSetChanged();
                    }
                }
            }
        }*/
        if(mFilterList!=null && mFilterList.size()>0) {
            for (int index=0;index<mFilterList.size();index++) {
                if(mFilterList.get(index).getMenuId() == filtersInfo.getMenuId()) {
                    mFilterList.get(index).setSelection(true);
                    mFilterAdapter.notifyDataSetChanged();
                }
            }
        }
        switch (filtersInfo.getMenuId()){
            case MENU_HOME:
                setFragment(new DashboardFragment());
                break;
             case MENU_READING:
                setFragment(new SmartReadingFragment());
                break;
             case MENU_HISTORY:
                setFragment(new BillHistoryFragment());
                break;
            case MENU_PROFILE:
                setFragment(new ProfileFragment());
                break;
        }
    }

    @Override
    public void onRequestStarted(Object mObject) {
        hideDialog();
    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
    }

    @Override
    public void onOkClicked(Object object) {
        if(object instanceof GenericResponse){
            GenericResponse response = (GenericResponse) object;
            if(((GenericResponse) object).getRequestCode()==IS_LOYOUT){
                EmbracePref.clear(getApplicationContext());
                //navigateLogin();
                finish();
            }
        }
    }

    @Override
    public void onCancelClicked() {

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        showLogOutDialog();
        return false;
    }

    public void showprofile(View view) {

    }

    public void onaboutus(View view) {

    }

    public void showHistory(View view) {
        startActivity(new Intent(getApplicationContext(),BillHistoryActivity.class));
    }

    public void onReading(View view) {
        startActivity(new Intent(getApplicationContext(),SmartMeterActivity.class));
    }

    public void onComplentList(View view) {
        startActivity(new Intent(getApplicationContext(),ComplantActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideDialog();
    }
}
