package com.masterstrokepune.embress;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.model.CategoryModel;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.request.ComplantRequest;
import com.masterstrokepune.embress.retrofit.response.CategoryResponse;
import com.masterstrokepune.embress.retrofit.response.TicketResponse;
import com.masterstrokepune.embress.util.EmbracePref;

import java.util.ArrayList;
import java.util.List;

public class AddComplentActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, RetrofitServiceListener, View.OnClickListener {

    Spinner spinner;
    RetrofitServiceListener mServiceListener;
    EditText edtcompaint;
    Button btnSubmit;

    List<CategoryModel> mCategoryList;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_complent);
        spinner = (Spinner) findViewById(R.id.spn_category);
        edtcompaint = findViewById(R.id.edtcompaint);
        btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(this);
        mServiceListener = this;
        RetrofitService.getInstance(getApplicationContext()).getCategoryList(mServiceListener);

    }

    public void initCategoryList(List<CategoryModel> list){
        // Spinner element
        mCategoryList = list;
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        for (int index=0;index<list.size();index++){
            categories.add(list.get(index).getCategoryName());
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    public boolean validForm(){
        if(TextUtils.isEmpty(edtcompaint.getText().toString())){
            showErrorMessageSheet(getSupportFragmentManager(),"Alert","Please Enter query description");
            return false;
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
        if(mObject instanceof CategoryResponse){
            CategoryResponse mResponse = (CategoryResponse) mObject;
            initCategoryList(mResponse.getCategoryModelsList());
        }else if(mObject instanceof TicketResponse){
            TicketResponse response = (TicketResponse) mObject;
            if(response.getMsg().contains("successfully")){
                showErrorMessageSheet(getSupportFragmentManager(), "SUCCESS", response.getMsg(), false, new OnDialogClickListener() {
                    @Override
                    public void onOkClicked(Object object) {
                            finish();
                    }

                    @Override
                    public void onCancelClicked() {

                    }
                });
            }
        }
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
    }

    @Override
    public void onClick(View v) {
        if(validForm()){
            ComplantRequest request = new ComplantRequest(mCategoryList.get(spinner.getSelectedItemPosition()).getCategoryName(),
                    edtcompaint.getText().toString(),
                    EmbracePref.getCustomerId(getApplicationContext()));
            RetrofitService.getInstance(getApplicationContext()).addTicket(request,mServiceListener);
        }
    }
}