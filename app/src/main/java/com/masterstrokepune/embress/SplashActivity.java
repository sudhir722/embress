package com.masterstrokepune.embress;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.masterstrokepune.embress.ocr.ScannerActivity;
import com.masterstrokepune.embress.util.EmbracePref;

public class SplashActivity extends AppCompatActivity {

    ImageView icon;
    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private FirebaseAnalytics mFirebaseAnalytics;

    public  void rotateImage(){
        RotateAnimation rotate = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(8000);
        rotate.setInterpolator(new LinearInterpolator());

        icon.startAnimation(rotate);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        icon = findViewById(R.id.icon);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        rotateImage();


        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                navigateLogin();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void navigateLogin() {
        if(EmbracePref.getUserId(getApplicationContext())>0){
            startActivity(new Intent(getApplicationContext(), HomeScreen.class));
        }else{
            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
        }
        finish();
    }
}