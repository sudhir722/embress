package com.masterstrokepune.embress;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.request.RegisterRequest;
import com.masterstrokepune.embress.retrofit.response.RegistrationResponse;
import com.masterstrokepune.embress.util.EmbracePref;

public class RegistrationActivity extends BaseActivity implements View.OnClickListener , RetrofitServiceListener {

    RetrofitServiceListener mServiceListener;
    EditText edtConsumerNo,edtMobileNo,edtEmail,edtUserName,edtPassword,edtCOnfirmPassword;
    Button register;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mServiceListener = this;
        register = findViewById(R.id.register);
        edtConsumerNo = findViewById(R.id.edt_consumerno);
        edtMobileNo = findViewById(R.id.edt_mobileno);
        edtEmail = findViewById(R.id.edt_email);
        edtUserName = findViewById(R.id.edt_username);
        edtPassword = findViewById(R.id.edt_password);
        edtCOnfirmPassword = findViewById(R.id.edt_confirm_password);
        register.setOnClickListener(this);
        edtConsumerNo.setEnabled(false);
        if(getIntent()!=null && getIntent().getExtras()!=null && getIntent().getExtras().containsKey("consu")){
            edtConsumerNo.setText(getIntent().getExtras().getString("consu"));
        }
        edtMobileNo.setText(EmbracePref.getMobileNo(getApplicationContext()));
        edtMobileNo.setEnabled(false);
    }

    public boolean isValidForm(){
        if(TextUtils.isEmpty(edtConsumerNo.getText().toString())){
            showErrorMessageSheet(getSupportFragmentManager(),"Alert","Please Enter Consumer Number");
            return false;
        }else if(TextUtils.isEmpty(edtMobileNo.getText().toString())){
            showErrorMessageSheet(getSupportFragmentManager(),"Alert","Please Enter Mobile Number");
            return false;
        }else if(edtMobileNo.getText().toString().length()<9){
            showErrorMessageSheet(getSupportFragmentManager(),"Alert","Please Enter Valid Mobile Number");
            return false;
        }else if(TextUtils.isEmpty(edtEmail.getText().toString())){
            showErrorMessageSheet(getSupportFragmentManager(),"Alert","Please Enter E-mail Address");
            return false;
        }else if(TextUtils.isEmpty(edtPassword.getText().toString())){
            showErrorMessageSheet(getSupportFragmentManager(),"Alert","Please Enter Password");
            return false;
        }else if(TextUtils.isEmpty(edtCOnfirmPassword.getText().toString())){
            showErrorMessageSheet(getSupportFragmentManager(),"Alert","Please Enter Confirm Password");
            return false;
        }else if(!edtCOnfirmPassword.getText().toString().equalsIgnoreCase(edtPassword.getText().toString())){
            showErrorMessageSheet(getSupportFragmentManager(),"Alert","Password Mismatch, Please enter same password");
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        if(isValidForm()){
            RegisterRequest registerRequest = new RegisterRequest(edtConsumerNo.getText().toString(),
                    edtMobileNo.getText().toString(),
                    edtEmail.getText().toString(),
                    edtUserName.getText().toString(),
                    edtPassword.getText().toString(),
                    edtPassword.getText().toString(),0);
            RetrofitService.getInstance(getApplicationContext()).register(registerRequest,mServiceListener);
        }
    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
        if(mObject instanceof RegistrationResponse){
            RegistrationResponse response = (RegistrationResponse) mObject;
            //if(response.getMsg().equalsIgnoreCase("SUCCESS")){
                showErrorMessageSheet(getSupportFragmentManager(), "Alert", response.getMsg(), true, new OnDialogClickListener() {
                    @Override
                    public void onOkClicked(Object object) {
                        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                        intent.putExtra("uname",edtUserName.getText().toString());
                        intent.putExtra("pass",edtPassword.getText().toString());
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onCancelClicked() {

                    }
                });
            /*}else{
                showErrorMessageSheet(getSupportFragmentManager(),"ERROR",response.getUserMsg());
            }*/
        }
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
    }

    public void signin(View view) {
        finish();
    }
}