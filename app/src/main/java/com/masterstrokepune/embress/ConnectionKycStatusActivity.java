package com.masterstrokepune.embress;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.masterstrokepune.embress.adapter.ConnectionRequestRecyclerAdapter;
import com.masterstrokepune.embress.adapter.DocUploadRecyclerAdapter;
import com.masterstrokepune.embress.iface.ItemClickListener;
import com.masterstrokepune.embress.model.CustomerModel;
import com.masterstrokepune.embress.retrofit.response.ConnectionResponse;

import java.util.ArrayList;
import java.util.List;

public class ConnectionKycStatusActivity extends BaseActivity implements ItemClickListener {

    ConnectionResponse mConnectionList;
    RecyclerView recyclerView;
    ItemClickListener itemClickListener;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_kyc_status);
        itemClickListener = this;
        recyclerView = findViewById(R.id.recyclerView);
        if(getIntent()!=null && getIntent().getExtras()!=null && getIntent().getExtras().containsKey("data")){
            mConnectionList = (ConnectionResponse) getIntent().getExtras().getSerializable("data");
            init();
        }else{
            showErrormessageAndFinish("Something went wrong, Please try again..");
        }
        ((TextView)findViewById(R.id.action_new)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), NewCustomerActivity.class);
                in.putExtra("existing",true);
                startActivity(in);
            }
        });

    }

    public void init(){
        List<CustomerModel> modelList=new ArrayList<>();
        if(mConnectionList.getConnectionStatusList()!=null){
            modelList.addAll(mConnectionList.getConnectionStatusList());
        }
        if(mConnectionList.getConnectionStatusList1()!=null){
            modelList.addAll(mConnectionList.getConnectionStatusList1());
        }
        ConnectionRequestRecyclerAdapter mAdapter = new ConnectionRequestRecyclerAdapter(getApplicationContext(), modelList,itemClickListener);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClickListener(Object object) {
        if(object instanceof CustomerModel){
            CustomerModel model = (CustomerModel) object;
            Intent intent = new Intent(getApplicationContext(),RegistrationActivity.class);
            intent.putExtra("consu",model.getCustomerUserNo());
            startActivity(intent);
            finish();
        }
    }
}