package com.masterstrokepune.embress;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.ocr.Camera2Activity;
import com.masterstrokepune.embress.ocr.utils.BitmapUtils;
import com.masterstrokepune.embress.ocr.utils.CommonUtils;
import com.masterstrokepune.embress.ocr.utils.FrescoUtils;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.request.MeterReadingRequest;
import com.masterstrokepune.embress.retrofit.response.MeterReadingResponse;
import com.masterstrokepune.embress.retrofit.response.UploadImageResponse;
import com.masterstrokepune.embress.util.EmbracePref;
import com.masterstrokepune.embress.util.Utility;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SmartMeterActivity extends BaseActivity implements View.OnClickListener , RetrofitServiceListener,OnDialogClickListener {

    TextView edtName,edt_consumerno,edt_address,edt_unit;
    EditText edt_comp;
    TextView btnSubmit;
    RetrofitServiceListener mServiceListener;
    OnDialogClickListener onDialogClickListener;

    /*******************************/
    private static final int CAMERA_REQUEST_CODE = 200;
    private static final int STORAGE_REQUEST_CODE = 400;
    private static final int IMAGE_PICK_GALLERY_CODE = 1000;
    private static final int IMAGE_PICK_CAMERA_CODE = 2001;
    private static final int IMAGE_PICK_CODE = 3001;

    String cameraPermission[];
    String storagePermission[];

    Uri image_uri;

    private EditText mResultEt;
    private ImageView mPreviewIv;
    /*********************************/
    SimpleDraweeView mImageView;
    File mFile;
    boolean mHasSelectedOnce;
    private Uri mUri;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smart_meter);
        mImageView = findViewById(R.id.iv);
        //mResultEt   = findViewById(R.id.resultEt);
        mPreviewIv  = findViewById(R.id.imageIv);

        ((ImageView)findViewById(R.id.action_help)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHelpFile();
                //9637727729
            }
        });
        edtName = findViewById(R.id.edt_name);
        edt_consumerno = findViewById(R.id.edt_consumerno);
        edt_address = findViewById(R.id.edt_address);
        edt_unit = findViewById(R.id.edt_unit);
        edt_comp = findViewById(R.id.edt_comp);
        btnSubmit = findViewById(R.id.actionsubmit);
        mServiceListener = this;
        onDialogClickListener = this;
        edtName.setText(EmbracePref.getDisplayName(getApplicationContext()));
        edt_consumerno.setText(EmbracePref.getConsumerNo(getApplicationContext()));
        edt_address.setText(EmbracePref.getAddress(getApplicationContext()));
        edt_unit.setText(""+EmbracePref.getUsedUnit(getApplicationContext()));
        btnSubmit.setOnClickListener(this);
        ImageView imgScan = findViewById(R.id.img);
        imgScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //scanMeter();
                if (!checkCameraPermission()) {
                    //camera permission not allowed, request it
                    requestCameraPermission();
                } else {
                    //permission allowed, take picture
                    pickCamera();
                }
            }
        });

        /*****************************/
        //camera permission
        cameraPermission = new String[] {Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE};
        //storage permission
        storagePermission = new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        /*****************************/

    }

    private void pickCamera() {
        //intent to take image from camera, it will also be save to storage to get high quality image
        /*ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "NewPick"); //title of the picture
        values.put(MediaStore.Images.Media.DESCRIPTION, "Image To Text"); //title of the picture
        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_CODE);*/

        ImagePicker.Companion.with(this)
                .crop()	    			//Crop image(Optional), Check Customization for more option
                .cameraOnly()
                .compress(720)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(720, 720)	//Final image resolution will be less than 1080 x 1080(Optional)
                .start(IMAGE_PICK_CODE);
    }

    private boolean checkStoragePermission() {
        boolean result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return result;
    }

    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, cameraPermission, CAMERA_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        boolean result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) == (PackageManager.PERMISSION_GRANTED);

        boolean result1 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return result && result1;
    }

    @Override
    public void onClick(View v) {
        if(TextUtils.isEmpty(edt_comp.getText().toString())){
            showErrorMessageSheet(getSupportFragmentManager(),"Alert","Please Enter current Meter reading..");
        }else{
            if(mFile==null){
                showErrorMessageSheet(getSupportFragmentManager(),"Alert","Please capture Meter Image");
            }else {
                //RetrofitService.getInstance(getApplicationContext()).uploadDocument(mFile,mServiceListener);
               // RetrofitService.getInstance(getApplicationContext()).addMeterReadingRequest();

                RetrofitService.getInstance(getApplicationContext()).uploadMeterPicture(getApplicationContext(),
                        mUri, mFile,
                        EmbracePref.getCustomerId(getApplicationContext()),
                        Utility.formatDouble(edt_comp.getText().toString()), mServiceListener);
                /*RetrofitService.getInstance(getApplicationContext()).upload(mFile,EmbracePref.getCustomerId(getApplicationContext()),
                        Utility.formatDouble(edt_comp.getText().toString()));*/
            }
            //MeterReadingRequest request = new MeterReadingRequest(EmbracePref.getCustomerId(getApplicationContext()),edt_comp.getText().toString());
            //RetrofitService.getInstance(getApplicationContext()).addMeterReadingRequest(request,mServiceListener);
        }
    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
        if(mObject instanceof UploadImageResponse){
            UploadImageResponse response = (UploadImageResponse) mObject;
            String url = response.getImagepathh();
            String localUrl = getString(R.string.local_url);
            if(url.contains(localUrl)){
                url ="http:\\\\"+ url.substring(38,url.length());
                url = url.replaceAll("//","");
                //url = url.replaceAll(localUrl,"http://");
            }

            MeterReadingRequest request = new MeterReadingRequest(EmbracePref.getCustomerId(getApplicationContext()),
                    edt_comp.getText().toString(),url);
            RetrofitService.getInstance(getApplicationContext()).addMeterReadingRequest(request,mServiceListener);
        }else if(mObject instanceof MeterReadingResponse){
            MeterReadingResponse response = (MeterReadingResponse) mObject;
            if(response!=null){
                showSuccessMessageSheet(getSupportFragmentManager(), "SUCCESS", response.getMsg(), false, onDialogClickListener);

            }else{
                showErrorMessageSheet(getSupportFragmentManager(),"Alert","Unable to save reading, Please try again later..");
            }
        }
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
            hideDialog();
            showErrorMessageSheet(getSupportFragmentManager(),"Alert","Unable to save reading, Please try again later");
    }

    @Override
    public void onOkClicked(Object object) {
       /* Intent in = new Intent(getApplicationContext(), BillInfoActivity.class);
        in.putExtra("iscurrent","yes");
        startActivity(in);*/
       finish();
    }

    @Override
    public void onCancelClicked() {

    }

    //handle permission result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean writeStorageAccepted = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (cameraAccepted && writeStorageAccepted) {
                        pickCamera();
                    } else {
                        Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case STORAGE_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean writeStorageAccepted = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (writeStorageAccepted) {
                        //pickGallery();
                    } else {
                        Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            Uri fileUri = data.getData();
            mPreviewIv.setImageURI(fileUri);
            mFile = new File(fileUri.getPath());

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
    }

    //handle image result
    protected void onActivityResultTemp(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_PICK_CODE) {
            //mFile = new File(data.getStringExtra("file"));
            Uri fileUri = data.getData();
            mPreviewIv.setImageURI(fileUri);
            mFile = new File(fileUri.getPath());
            Observable.just(mFile)
                    //Read in File and compress to Bitmap of specified size
                    .map(file -> BitmapUtils.compressToResolution(file, 820 * 500))
                    //The orientation of the photo taken by the system camera may be vertical. If it is judged here, it will be rotated 90 degrees to horizontal
                    //.map(BitmapUtils::crop)
                    //将Bitmap写入文件
                    .map(bitmap -> BitmapUtils.writeBitmapToFile(getApplicationContext(),bitmap, "mFile.png"))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(file -> {
                        mFile = file;
                        Uri uri = Uri.parse("file://" + mFile.toString());
                        ImagePipeline imagePipeline = Fresco.getImagePipeline();
                        //清除该Uri的Fresco缓存. 若不清除，对于相同文件名的图片，Fresco会直接使用缓存而使得Drawee得不到更新.
                        imagePipeline.evictFromMemoryCache(uri);
                        imagePipeline.evictFromDiskCache(uri);
                        FrescoUtils.load("file://" + mFile.toString()).into(mImageView);

                        mHasSelectedOnce = true;
                        mUri = uri;
                        //set image to image view
                        mPreviewIv.setImageURI(uri);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                BitmapDrawable bitmapDrawable = (BitmapDrawable) mPreviewIv.getDrawable();
                                Bitmap bitmap = bitmapDrawable.getBitmap();
                                bitmap = BitmapUtils.crop(getApplicationContext(),bitmap);
                                mPreviewIv.setImageBitmap(bitmap);
                                TextRecognizer recognizer = new TextRecognizer.Builder(getApplicationContext()).build();

                                if (!recognizer.isOperational()) {
                                    Toast.makeText(SmartMeterActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                } else {
                                    Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                                    SparseArray<TextBlock> items = recognizer.detect(frame);

                                    StringBuilder sb = new StringBuilder();
                                    //get text from sb until there is no text
                                    for (int i = 0; i < items.size(); i++) {
                                        TextBlock myItem = items.valueAt(i);
                                        sb.append(myItem.getValue());
                                        sb.append("\n");
                                    }

                                    //set text to edit text
                                    String currentReading = convertValue(sb.toString());
                                    if(TextUtils.isEmpty(currentReading)){
                                        Toast.makeText(getApplicationContext(),"Unable to detect Meter Reading, Please try again later",Toast.LENGTH_LONG).show();
                                    }else {
                                        Toast.makeText(getApplicationContext(),"Kindly verify your meter reading before submitting.",Toast.LENGTH_LONG).show();
                                        edt_comp.setText(currentReading);
                                    }
                                    //btnSubmit.performClick();
                                }
                            }
                        });
                        //get drawable bitmap for text recognition

                    });
        }else if (resultCode == RESULT_OK) {


            if (requestCode == IMAGE_PICK_GALLERY_CODE) {
                //got image from gallery now crop it
                CropImage.activity(data.getData())
                        .setGuidelines(CropImageView.Guidelines.ON) //enable image guid lines
                        .start(this);
            }

            if (requestCode == IMAGE_PICK_CAMERA_CODE) {
                //got image from camera now crop it
                /*CropImage.activity(image_uri)
                        .setGuidelines(CropImageView.Guidelines.ON) //enable image guid lines
                        .setAspectRatio(5,5)
                        .start(this);*/
                //Uri resultUri = image_uri.getUri(); //get image uri
                //set image to image view
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                mPreviewIv.setImageBitmap(photo);


//                Bitmap myPictureBitmap = BitmapFactory.decodeFile(image_uri.getPath());
//                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
//                    myPictureBitmap = Bitmap.createScaledBitmap(myPictureBitmap, mPreviewIv.getWidth(),mPreviewIv.getHeight(),true);
//                }
//                mPreviewIv.setImageBitmap(myPictureBitmap);


//                mPreviewIv.setImageURI(image_uri);
//                //get drawable bitmap for text recognition
//                BitmapDrawable bitmapDrawable = (BitmapDrawable) mPreviewIv.getDrawable();
//                Bitmap bitmap = bitmapDrawable.getBitmap();
//                bitmap = BitmapUtils.crop(getApplicationContext(),bitmap);
//                mPreviewIv.setImageBitmap(bitmap);
                mPreviewIv.setVisibility(View.VISIBLE);
            }
        }

        //get cropped image
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri(); //get image uri
                //set image to image view
                mPreviewIv.setImageURI(resultUri);

                //get drawable bitmap for text recognition
                BitmapDrawable bitmapDrawable = (BitmapDrawable) mPreviewIv.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();

                TextRecognizer recognizer = new TextRecognizer.Builder(getApplicationContext()).build();

                if (!recognizer.isOperational()) {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                } else {
                    Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                    SparseArray<TextBlock> items = recognizer.detect(frame);
                    StringBuilder sb = new StringBuilder();
                    //get text from sb until there is no text
                    for (int i = 0; i < items.size(); i++) {
                        TextBlock myItem = items.valueAt(i);
                        sb.append(myItem.getValue());
                        sb.append("\n");
                    }

                    //set text to edit text
                    edt_comp.setText(convertValue(sb.toString()));
                    btnSubmit.performClick();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                //if there is any error show it
                Exception error = result.getError();
                Toast.makeText(this, "" + error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public String convertValue(String val){
        StringBuffer sbf = new StringBuffer();
        Log.d("METERREAD",val);
        for (int index=0;index<val.length();index++){
            if(isInteger(String.valueOf(val.charAt(index)))){
                sbf.append(String.valueOf(val.charAt(index)));
            }
        }
        try {
            int reading = Integer.parseInt(sbf.toString());
            DecimalFormat df = new DecimalFormat("#.000"); // Set your desired format here.
            System.out.println(df.format(reading / 100.0));
            return df.format(reading / 100.0);
        }catch (Exception e){
            e.printStackTrace();
            return sbf.toString();
        }

    }

    public boolean isInteger( String input ) { //Pass in string
        try { //Try to make the input into an integer
            Integer.parseInt( input );
            return true; //Return true if it works
        }
        catch( Exception e ) {
            return false; //If it doesn't work return false
        }
    }


    public void scanMeter(){
        TedPermission.with(App.sApp)
                .setRationaleMessage("\n" +
                        "We need to use the camera on your device to complete the photo.\n" +
                        "When the Android system requests to grant the camera permission, please select \"Allow\"。")
                .setDeniedMessage("If you don't grant camera permission , you will not be able to take pictures.")
                .setRationaleConfirmText("OK")
                .setDeniedCloseButtonText("Cancel")
                .setGotoSettingButtonText("Setting")
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        Intent intent;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent = new Intent(SmartMeterActivity.this, Camera2Activity.class);
                        } else {
                            new AlertDialog
                                    .Builder(SmartMeterActivity.this)
                                    .setTitle("Unsupported API Level")
                                    .setMessage("Camera2 API is only available above API Level 21, current API Level : " + Build.VERSION.SDK_INT)
                                    .setPositiveButton("OK", (dialog, which) -> dialog.dismiss())
                                    .show();
                            return;
                        }
                        mFile = CommonUtils.createImageFile(getApplicationContext(),"mFile");
                        //文件保存的路径和名称
                        intent.putExtra("file", mFile.toString());
                        //拍照时的提示文本
                        intent.putExtra("hint", "Please put Meter Reading rectangulat in the box. The picture will be cropped, leaving only the image in the area inside the frame");
                        //是否使用整个画面作为取景区域(全部为亮色区域)
                        intent.putExtra("hideBounds", false);
                        //最大允许的拍照尺寸（像素数）
                        intent.putExtra("maxPicturePixels", 3840 * 2160);
                        startActivityForResult(intent, App.TAKE_PHOTO_CUSTOM);
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {

                    }

                    public void onPermissionDenied(ArrayList<String> arrayList) {}
                }).setPermissions(new String[]{Manifest.permission.CAMERA})
                .check();
    }

}