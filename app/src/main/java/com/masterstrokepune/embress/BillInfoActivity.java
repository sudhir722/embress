package com.masterstrokepune.embress;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.adapter.BillChargesAdapter;
import com.masterstrokepune.embress.bs.BSLogoutDialogFragment;
import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.model.BillChargesModel;
import com.masterstrokepune.embress.model.BillDetailModel;
import com.masterstrokepune.embress.model.Chargesodel;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.request.BillDetailRequest;
import com.masterstrokepune.embress.retrofit.request.BillInfoRequest;
import com.masterstrokepune.embress.retrofit.request.GenerateReceiptRequest;
import com.masterstrokepune.embress.retrofit.response.BillHistoryResponse;
import com.masterstrokepune.embress.retrofit.response.BillInfoResponse;
import com.masterstrokepune.embress.retrofit.response.GenerateReceiptResponse;
import com.masterstrokepune.embress.retrofit.response.GenericResponse;
import com.masterstrokepune.embress.util.EmbracePref;
import com.masterstrokepune.embress.util.LocalConstance;
import com.masterstrokepune.embress.util.Utility;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.masterstrokepune.embress.util.LocalConstance.IS_LOYOUT;

public class BillInfoActivity extends BaseActivity implements PaymentResultListener, View.OnClickListener, RetrofitServiceListener, PopupMenu.OnMenuItemClickListener, OnDialogClickListener {

    TextView edtName,edt_consumerno,edt_address,edt_unit;
    TextView edt_amt;
    BillDetailModel mBillInfo;
    TextView actionPay;
    ImageView img_paid;
    RetrofitServiceListener mServiceListener;
    private RecyclerView mRecyclerView;
    private BillChargesAdapter mAdapter;
    private String isCurrent;
    TextView txtBillNumber;

    LinearLayout lin_receipt;
    TextView previous_reading,currentReading,receipt_no;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billview);
        edtName = findViewById(R.id.edt_name);
        img_paid = findViewById(R.id.img_paid);
        edt_consumerno = findViewById(R.id.edt_consumerno);
        edt_address = findViewById(R.id.edt_address);
        edt_unit = findViewById(R.id.edt_unit);
        edt_amt = findViewById(R.id.edt_amt);
        actionPay = findViewById(R.id.action_pay);
        previous_reading = findViewById(R.id.previous_reading);
        currentReading = findViewById(R.id.current_reading);
        lin_receipt = findViewById(R.id.lin_receipt);
        receipt_no = findViewById(R.id.receipt_no);
        txtBillNumber = findViewById(R.id.txt_billno);
        mServiceListener = this;

        edtName.setText(EmbracePref.getDisplayName(getApplicationContext()));
        edt_consumerno.setText(EmbracePref.getConsumerNo(getApplicationContext()));
        edt_address.setText(EmbracePref.getAddress(getApplicationContext()));
        edt_unit.setText(""+EmbracePref.getUsedUnit(getApplicationContext()));
        if(getIntent() !=null && getIntent().getExtras()!=null && getIntent().getExtras().containsKey("iscurrent")){
            isCurrent =  getIntent().getExtras().getString("iscurrent");
        }
        if(getIntent() !=null && getIntent().getExtras()!=null && getIntent().getExtras().containsKey("data")){
            mBillInfo = (BillDetailModel) getIntent().getExtras().getSerializable("data");
            txtBillNumber.setText(""+mBillInfo.getPkBillvoucherEntryId());
            isCurrent =  getIntent().getExtras().getString("iscurrent");
            init(mBillInfo);

        }
        actionPay.setOnClickListener(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        if( !TextUtils.isEmpty(isCurrent)) {
            RetrofitService.getInstance(getApplicationContext()).getBillDetails(new BillInfoRequest(EmbracePref.getCustomerId(getApplicationContext())), mServiceListener);
            actionPay.setVisibility(View.VISIBLE);
        }
    }

    public void updateList(List<Chargesodel> charges){
        this.mAdapter = new BillChargesAdapter(getApplicationContext(), charges);
        this.mRecyclerView.setAdapter(this.mAdapter);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        this.mRecyclerView.setHasFixedSize(true);
    }
    private List<Chargesodel>  loadData(){
        List<Chargesodel> modellist = new ArrayList<>();
        modellist.add(new Chargesodel("Details","Amount/Rate", LocalConstance.VIEW_TYPE_HEADER));
        modellist.add(new Chargesodel("Service Charges","124.00", LocalConstance.VIEW_TYPE_EVEN));
        modellist.add(new Chargesodel("Previous Balance","00.00", LocalConstance.VIEW_TYPE_CELL));
        modellist.add(new Chargesodel("Late Fee","40.00", LocalConstance.VIEW_TYPE_EVEN));
        modellist.add(new Chargesodel("GST","22.00", LocalConstance.VIEW_TYPE_CELL));
        modellist.add(new Chargesodel("Cheque Bounce Charges","250.00", LocalConstance.VIEW_TYPE_EVEN));
        return modellist;
    }
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
// After successful payment Razorpay send back a unique id
        hideDialog();
        //Toast.makeText(BillInfoActivity.this, "Transaction Successful: " + razorpayPaymentID, Toast.LENGTH_LONG).show();
        String narration ="Rezorpay(Android) "+razorpayPaymentID+" for "+mBillInfo.getPkBillvoucherEntryId();
        GenerateReceiptRequest request = new GenerateReceiptRequest(mBillInfo.getCustomerUserNo(),mBillInfo.getPkBillvoucherEntryId(),mBillInfo.getBillDueAmount(),narration,mBillInfo.getBillDueAmount(),EmbracePref.getCustomerId(getApplicationContext()),mBillInfo.getPkBillvoucherEntryId());
        RetrofitService.getInstance(getApplicationContext()).addReceipt(request,razorpayPaymentID,mServiceListener);
        Toast.makeText(BillInfoActivity.this, "Transaction Successful: " + razorpayPaymentID, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPaymentError(int i, String error) {
// Error message
        hideDialog();
        Toast.makeText(BillInfoActivity.this, "Transaction unsuccessful: "+ error , Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        double amount = toDouble(edt_amt.getText().toString());
        String convertedAmount=String.valueOf((int)(amount*100));
        rezorpayCall(convertedAmount);
    }

    public void rezorpayCall(String convertedAmount){
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        String razorpayKey = "rzp_live_7GG2IWz7oph7DN"; //Generate your razorpay key from Settings-> API Keys-> copy Key Id
        Checkout chackout = new Checkout();
        chackout.setKeyID(razorpayKey);
        try {
            JSONObject options = new JSONObject();
            options.put("name", EmbracePref.getDisplayName(getApplicationContext()));
            options.put("description", "Bill Payment for rs "+toDouble(edt_amt.getText().toString()));
            options.put("currency", "INR");
            options.put("amount", convertedAmount);

            JSONObject preFill = new JSONObject();
            preFill.put("email", EmbracePref.getEmailId(getApplicationContext()));
            preFill.put("contact", EmbracePref.getMobileNo(getApplicationContext()));
            options.put("prefill", preFill);

            chackout.open(BillInfoActivity.this, options);
        } catch (Exception e) {
            Toast.makeText(BillInfoActivity.this, "Error in payment: " + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

    public double toDouble(String value){
        try {
            return Double.parseDouble(value);
        }catch (Exception e){
            return 0;
        }
    }

    public int toInt(String value){
        try {
            return Integer.parseInt(value);
        }catch (Exception e){
            return 0;
        }
    }

    @Override
    public void onResponse(Object mObject) {
        if(mObject instanceof BillHistoryResponse){
            BillHistoryResponse response = (BillHistoryResponse) mObject;
            if(response!=null && response.getBillList()!=null){
                int mBillNumbe = toInt(txtBillNumber.getText().toString());
                for (int index=0;index<response.getBillList().size();index++){
                    if(mBillNumbe==response.getBillList().get(index).getPkBillvoucherEntryId()){
                        mBillInfo = response.getBillList().get(index);
                        init(mBillInfo);
                    }
                }
            }
        }else if(mObject instanceof BillInfoResponse){
            BillInfoResponse response = (BillInfoResponse) mObject;
            if(response!=null){

                BillChargesModel charges = response.getModels().get(0);
                List<Chargesodel> modellist = new ArrayList<>();
                modellist.add(new Chargesodel("Details","Amount/Rate", LocalConstance.VIEW_TYPE_HEADER));
                modellist.add(new Chargesodel("Rate",Utility.formatDouble(charges.getRate()), LocalConstance.VIEW_TYPE_EVEN));
                modellist.add(new Chargesodel("Current Reading",String.valueOf(charges.getCurrentreading()), LocalConstance.VIEW_TYPE_CELL));
                modellist.add(new Chargesodel("Previous Reading",String.valueOf(charges.getPreviousreading()), LocalConstance.VIEW_TYPE_CELL));
                double amount = (charges.getCurrentreading() - charges.getPreviousreading())*charges.getRate();
                modellist.add(new Chargesodel("Consumption Charges", Utility.formatDouble(amount), LocalConstance.VIEW_TYPE_CELL));
                modellist.add(new Chargesodel("Service Charges",Utility.formatDouble(charges.getServicecharges()), LocalConstance.VIEW_TYPE_EVEN));
                modellist.add(new Chargesodel("Previous Balance",charges.getPreviousbal(), LocalConstance.VIEW_TYPE_CELL));
                modellist.add(new Chargesodel("Late Fee",charges.getLatefeecharges(), LocalConstance.VIEW_TYPE_EVEN));
                modellist.add(new Chargesodel("GST",charges.getGst(), LocalConstance.VIEW_TYPE_CELL));
                modellist.add(new Chargesodel("Cheque Bounce Charges",charges.getChequebouncecharges(), LocalConstance.VIEW_TYPE_EVEN));
                updateList(modellist);
                double billAmount = amount;
                billAmount+=toDouble(charges.getPreviousbal());
                billAmount+=toDouble(charges.getPreviousbal());
                billAmount+=toDouble(charges.getLatefeecharges());
                billAmount+=toDouble(charges.getGst());
                billAmount+=toDouble(charges.getChequebouncecharges());
                edt_unit.setText(""+(charges.getCurrentreading() - charges.getPreviousreading()));
                edt_amt.setText(""+amount);
                txtBillNumber.setText(String.valueOf(charges.getPkbillvoucherentryid()));


                RetrofitService.getInstance(getApplicationContext()).history(new BillDetailRequest(EmbracePref.getCustomerId(getApplicationContext())),mServiceListener);
            }
        }else if(mObject instanceof GenerateReceiptResponse){
            GenerateReceiptResponse response = (GenerateReceiptResponse) mObject;
            if(response!=null){
                showErrorMessageSheet(getSupportFragmentManager(), "SUCCESS", response.getMessage() + " " + response.getReceiptId(), false, new OnDialogClickListener() {
                    @Override
                    public void onOkClicked(Object object) {
                        finish();
                    }

                    @Override
                    public void onCancelClicked() {

                    }
                });
            }
        }
        hideDialog();
    }

    private void init(BillDetailModel mBillInfo) {
        if(mBillInfo!=null){
            edt_amt.setText(mBillInfo.getBillDueAmount()+"");
            edt_unit.setText(mBillInfo.getUsedUnits()+"");

            previous_reading.setText(""+mBillInfo.getPreviousreading());
            currentReading.setText(String.valueOf(mBillInfo.getCurrentreading()));
            if(mBillInfo.getPkReceiptNo()>0){
                lin_receipt.setVisibility(View.VISIBLE);
                receipt_no.setText(String.valueOf(mBillInfo.getPkReceiptNo()));
            }else{
                lin_receipt.setVisibility(View.GONE);
            }
            if(mBillInfo.getStaus().equalsIgnoreCase("Bill Paid")) {
                //holder.status.setBackgroundResource(R.drawable.circle_bg_close);
                actionPay.setVisibility(View.GONE);
                img_paid.setVisibility(View.VISIBLE);
            }else  {
                img_paid.setVisibility(View.GONE);
                //holder.status.setBackgroundResource(R.drawable.circle_bg_active);
                actionPay.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        popup.setOnMenuItemClickListener(this);
        inflater.inflate(R.menu.menu_main, popup.getMenu());
        popup.show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        showLogOutDialog();
        return false;
    }

    public void showLogOutDialog(){
        BSLogoutDialogFragment bottomSheetFragment = new BSLogoutDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LocalConstance.CONST_TITLE, getString(R.string.app_name));
        bundle.putString(LocalConstance.CONST_MESSAGE, getString(R.string.txt_logout_message));
        bundle.putSerializable(LocalConstance.CONST_LISTENER, this);
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
    }

    @Override
    public void onOkClicked(Object object) {
        if(object instanceof GenericResponse){
            GenericResponse response = (GenericResponse) object;
            if(((GenericResponse) object).getRequestCode()==IS_LOYOUT){
                EmbracePref.clear(getApplicationContext());
                //navigateLogin();
                finish();
            }
        }
    }

    @Override
    public void onCancelClicked() {

    }
}