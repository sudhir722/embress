package com.masterstrokepune.embress.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.masterstrokepune.embress.BaseFragment;
import com.masterstrokepune.embress.NewCustomerActivity;
import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.retrofit.request.NewConnectionRequest;
import com.masterstrokepune.embress.util.EmbracePref;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ContactInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactInfoFragment extends BaseFragment implements View.OnClickListener {

    EditText edtFlatNumber,edtAddress,edtMobile,edt_mail,edtTower;
    Button btnSave;
    NewConnectionRequest mRequest;
    public ContactInfoFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ContactInfoFragment newInstance(NewConnectionRequest request) {
        ContactInfoFragment fragment = new ContactInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data",request);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null && getArguments().containsKey("data")){
            try {
                mRequest = (NewConnectionRequest) getArguments().getSerializable("data");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void setData() {
        try{
            edtFlatNumber.setText(mRequest.getFlatNo());
            edtAddress.setText(mRequest.getAddress());
            edtMobile.setText(mRequest.getMobileNo());
            edt_mail.setText(mRequest.getEmailId());
            edtTower.setText(mRequest.getTower());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_contact_info, container, false);
        edtFlatNumber = view.findViewById(R.id.edt_flat);
        edtAddress = view.findViewById(R.id.edt_address);
        edtFlatNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                edtAddress.setText("Area : "+EmbracePref.getAreaName(getContext())+", Project:- "+EmbracePref.getProjectName(getContext())+
                        ", FLAT No : "+edtTower.getText().toString()+"-"+edtFlatNumber.getText().toString() +" ");
            }
        });
        edtAddress.setText("Area : "+EmbracePref.getAreaName(getContext())+", Project:- "+EmbracePref.getProjectName(getContext()));
        edtMobile = view.findViewById(R.id.edt_mobile);
        edtMobile.setText(EmbracePref.getMobileNo(getContext()));
        edtMobile.setEnabled(false);
        edt_mail = view.findViewById(R.id.edt_mail);
        btnSave = view.findViewById(R.id.register);
        edtTower = view.findViewById(R.id.edt_towerno);
        btnSave.setOnClickListener(this);
        edtTower.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                edtAddress.setText("Area : "+EmbracePref.getAreaName(getContext())+", Project:- "+EmbracePref.getProjectName(getContext())+
                        ", FLAT No : "+edtTower.getText().toString()+"-"+edtFlatNumber.getText().toString() +" ");
            }
        });
        setData();
        return view;
    }

    public boolean isValidForm(){
        if(TextUtils.isEmpty(edtTower.getText().toString())){
            showErrorMessageSheet(getChildFragmentManager(),"Alert","Please Enter your Tower Number");
            edtTower.requestFocus();
        }else if(TextUtils.isEmpty(edtFlatNumber.getText().toString())){
            showErrorMessageSheet(getChildFragmentManager(),"Alert","Please Enter your Flat Number");
            edtFlatNumber.requestFocus();
        }else if(TextUtils.isEmpty(edtAddress.getText().toString())){
            showErrorMessageSheet(getChildFragmentManager(),"Alert","Please Enter your full address");
            edtAddress.requestFocus();
        }else if(TextUtils.isEmpty(edtMobile.getText().toString())){
            showErrorMessageSheet(getChildFragmentManager(),"Alert","Please Enter your Contact Number");
            edtMobile.requestFocus();
        }else if(TextUtils.isEmpty(edt_mail.getText().toString())){
            showErrorMessageSheet(getChildFragmentManager(),"Alert","Please Enter your Email Address");
            edt_mail.requestFocus();
        }else{
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if(isValidForm()){
            ((NewCustomerActivity) getActivity()).onContactDetailSaved(
                    edtTower.getText().toString(),
                    edtFlatNumber.getText().toString(),
                    edtAddress.getText().toString(),
                    edtMobile.getText().toString(),
                    edt_mail.getText().toString());
        }
    }
}