package com.masterstrokepune.embress.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.response.CustomerDetailsResponse;
import com.masterstrokepune.embress.util.EmbracePref;
import com.masterstrokepune.embress.util.Utility;


public class ProfileFragmentBs extends BottomSheetDialogFragment implements RetrofitServiceListener {
    String message, title;
TextView txtname,txtuname,mobile,txt_address,txt_crn;
RetrofitServiceListener mServiceListener;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceListener = this;
//        if (getArguments() != null) {
//            message = getArguments().getString(LocalConstance.CONST_MESSAGE);
//            title = getArguments().getString(LocalConstance.CONST_TITLE);
//            mListener = (OnDialogClickListener) getArguments().getSerializable(LocalConstance.CONST_LISTENER);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.bs_profile, container, false);
        txtname = view.findViewById(R.id.txtname);
        txtuname = view.findViewById(R.id.txtuname);
        mobile = view.findViewById(R.id.mobile);
        txt_address = view.findViewById(R.id.txt_address);
        txt_crn = view.findViewById(R.id.txt_crn);

        txtname.setText(EmbracePref.getDisplayName(getContext()));
        txtuname.setText(EmbracePref.getDisplayName(getContext()));
        mobile.setText(EmbracePref.getMobileNo(getContext()));
//        ((TextView) view.findViewById(R.id.dialog_title)).setText(title);
//        ((TextView) view.findViewById(R.id.dialog_message)).setText(message);

        RetrofitService.getInstance(getContext()).getCustomerDetails(EmbracePref.getCustomerId(getContext()),mServiceListener);

        return view;
    }


    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < 16) {
                    view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
                FrameLayout bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
                BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                behavior.setPeekHeight(0); // Remove this line to hide a dark background if you manually hide the dialog.
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context != null && context instanceof OnDialogClickListener) {
//            mListener = (OnDialogClickListener) context;
//        }
    }

    @Override
    public void onRequestStarted(Object mObject) {

    }

    @Override
    public void onResponse(Object mObject) {
        if(mObject instanceof CustomerDetailsResponse){
            CustomerDetailsResponse custInfo = (CustomerDetailsResponse) mObject;
            txtname.setText(custInfo.getCustomerInfo().getCustomerName());
            txtuname.setText(custInfo.getCustomerInfo().getCustomerName());
            mobile.setText(custInfo.getCustomerInfo().getMobileNo());
            txt_address.setText(custInfo.getCustomerInfo().getAddress());
            txt_crn.setText(custInfo.getCustomerInfo().getConsumerno());
        }
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {

    }
}