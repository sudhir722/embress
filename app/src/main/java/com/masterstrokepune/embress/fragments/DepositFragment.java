package com.masterstrokepune.embress.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.masterstrokepune.embress.BaseFragment;
import com.masterstrokepune.embress.NewCustomerActivity;
import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.util.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DepositFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DepositFragment extends BaseFragment implements View.OnClickListener {

    Spinner spn_mode;
    EditText edt_chq,edt_bank,edt_flat;
    Button register;
    public DepositFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static DepositFragment newInstance(String param1, String param2) {
        DepositFragment fragment = new DepositFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_deposit, container, false);
        spn_mode = view.findViewById(R.id.spn_mode);
        edt_chq = view.findViewById(R.id.edt_chq);
        edt_flat = view.findViewById(R.id.edt_flat);
        edt_bank = view.findViewById(R.id.edt_bank);
        register = view.findViewById(R.id.register);
        register.setOnClickListener(this);
        init();
        return view;
    }
    public void init(){
        String[] arealist = new String[1];
        /*arealist[0] = "Select Payment Mode";
        arealist[1] = "Online";*/
        arealist[0] = "Cheque";

        ArrayAdapter aa = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, arealist);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spn_mode.setAdapter(aa);
    }

    public boolean isValidForm(){
        return true;
    }

    @Override
    public void onClick(View v) {
        if(isValidForm()){
            ((NewCustomerActivity)getActivity()).onDeposited(Utility.formatDouble(edt_flat.getText().toString()),edt_chq.getText().toString(),edt_bank.getText().toString());
        }
    }
}