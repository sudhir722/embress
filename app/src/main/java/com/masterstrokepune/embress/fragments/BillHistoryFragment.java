package com.masterstrokepune.embress.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.masterstrokepune.embress.BaseFragment;
import com.masterstrokepune.embress.BillInfoActivity;
import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.adapter.ListAdapter;
import com.masterstrokepune.embress.iface.onClick;
import com.masterstrokepune.embress.model.BillDetailModel;
import com.masterstrokepune.embress.model.BillInfoModel;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.request.BillDetailRequest;
import com.masterstrokepune.embress.retrofit.response.BillHistoryResponse;
import com.masterstrokepune.embress.util.EmbracePref;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BillHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BillHistoryFragment extends BaseFragment implements onClick, RetrofitServiceListener {

    onClick mClickListener;
    RecyclerView mRecycler;
    RetrofitServiceListener mServiceListener;

    public BillHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BillHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BillHistoryFragment newInstance(String param1, String param2) {
        BillHistoryFragment fragment = new BillHistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mClickListener = this;
        mServiceListener = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_bill_history, container, false);
        mRecycler = view.findViewById(R.id.recyclerView);
        RetrofitService.getInstance(getContext()).history(new BillDetailRequest(EmbracePref.getCustomerId(getActivity())),mServiceListener);
        ((ImageView) view.findViewById(R.id.action_help)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHelpFile();
                //9637727729
            }
        });
        return view;
    }

    public void initlist(List<BillDetailModel> models){
        ListAdapter mFilterAdapter = new ListAdapter(getActivity(),models,mClickListener);
        mRecycler.setAdapter(mFilterAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onOkClicked(Object object) {
        if(object instanceof BillDetailModel){
            BillDetailModel model = (BillDetailModel) object;
            Intent in = new Intent(getActivity(), BillInfoActivity.class);
            in.putExtra("data",model);
            startActivity(in);
        }
    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
        if(mObject instanceof BillHistoryResponse){
            BillHistoryResponse response = (BillHistoryResponse) mObject;
            if(response!=null){
                initlist(response.getBillList());
            }
        }
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
    }
}