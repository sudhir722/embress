package com.masterstrokepune.embress.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.masterstrokepune.embress.BaseFragment;
import com.masterstrokepune.embress.BuildConfig;
import com.masterstrokepune.embress.NewCustomerActivity;
import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.adapter.DocUploadRecyclerAdapter;
import com.masterstrokepune.embress.iface.ItemClickListener;
import com.masterstrokepune.embress.model.DocInfo;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.response.DocumentListModel;
import com.masterstrokepune.embress.retrofit.response.UploadImageResponse;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static androidx.core.content.FileProvider.getUriForFile;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DocumentListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DocumentListFragment extends BaseFragment implements RetrofitServiceListener , ItemClickListener {

    RecyclerView recyclerView;
    DocUploadRecyclerAdapter mAdapter;
    private DocInfo selectedDocInfo=null;
    public static final int REQUEST_IMAGE_CAPTURE = 0;
    public static final int REQUEST_GALLERY_IMAGE = 1;
    RetrofitServiceListener mListener;

    public static String fileName;
    List<DocInfo> mDocList;

    public DocumentListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DocumentListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DocumentListFragment newInstance(String param1, String param2) {
        DocumentListFragment fragment = new DocumentListFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListener = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_document_list, container, false);
        recyclerView = view.findViewById(R.id.recycler);
        RetrofitService.getInstance(getContext()).getKycDocumentList(this);
        init();
        return view;
    }

    public void init(){
        mDocList = new ArrayList<>();
        mAdapter = new DocUploadRecyclerAdapter(getContext(),mDocList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("Please wait...");
    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
        if(mObject instanceof UploadImageResponse){
            UploadImageResponse response = (UploadImageResponse) mObject;
            selectedDocInfo.setUploaded(true);
            mDocList.add(selectedDocInfo);
            mAdapter.notifyDataSetChanged();
            String url = response.getImagepathh();
            String localUrl = getString(R.string.local_url);
            if(url.contains(localUrl)){
                url ="http:\\\\"+ url.substring(38,url.length());
                url = url.replaceAll("//","");
                //url = url.replaceAll(localUrl,"http://");
            }
            selectedDocInfo.setDocumentUrl(url);
            for(int index=0;index<mDocList.size();index++){
                if(mDocList.get(index).getPkDocumentId()==selectedDocInfo.getPkDocumentId()){
                    mDocList.get(index).setDocumentUrl(url);
                }
            }
            ((NewCustomerActivity)getActivity()).onDocumentUploaded(mDocList);
        }else if(mObject instanceof DocumentListModel){
            DocumentListModel model = (DocumentListModel) mObject;
            if(model!=null){
                mDocList.clear();
                //mDocList.addAll(model.getDocdetails());
                if(model!=null && model.getDocdetails()!=null){
                    for(int index=0;index<model.getDocdetails().size();index++) {
                        if(model.getDocdetails().get(index)!=null)
                            mDocList.add(model.getDocdetails().get(index));
                    }
                }
                /*Collections.sort(mDocList);*/
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
        if(mObject instanceof String) {
            Toast.makeText(getContext(), (String) mObject, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onItemClickListener(Object object) {
        selectedDocInfo = (DocInfo) object;
        showImagePickerOptions(getContext());
    }
    public void showImagePickerOptions(Context context) {
        // setup the alert builder
       /* AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.lbl_set_profile_photo));

        // add a list
        String[] animals = {context.getString(R.string.lbl_take_camera_picture), context.getString(R.string.lbl_choose_from_gallery)};
        builder.setItems(animals, (dialog, which) -> {
            switch (which) {
                case 0:
                    takeCameraImage();
                    break;
                case 1:
                    chooseImageFromGallery();
                    break;
            }
        });*/
        chooseImageFromGallery();

        // create and show the alert dialog
        /*AlertDialog dialog = builder.create();
        dialog.show();*/
    }
    String imageFilePath;
    private File createImageFile() throws IOException {
        String imageFileName =System.currentTimeMillis() + ".jpg";
        File storageDir = getActivity().getCacheDir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    private void takeCameraImage() {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            fileName = System.currentTimeMillis() + ".jpg";
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException ex) {
                                // Error occurred while creating the File
                                ex.printStackTrace();
                            }
                            fileName = photoFile.getAbsolutePath();
                            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(fileName));
                            Uri photoURI = getUriForFile(getContext(),  BuildConfig.APPLICATION_ID+".fileprovider", photoFile);
                            //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoURI);
                            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                            }
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private Uri getCacheImagePath(String fileName) {
        File path = new File(getActivity().getExternalCacheDir(), "camera");
        if (!path.exists()) path.mkdirs();
        File image = new File(path, fileName);
        return getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", image);
    }

    private void chooseImageFromGallery() {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            fileName = System.currentTimeMillis() + ".jpg";
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, REQUEST_GALLERY_IMAGE);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Toast.makeText(getContext(),"REceiverd",Toast.LENGTH_LONG).show();

        for(int index=0;index<mDocList.size();index++){
            if(mDocList.get(index).getPkDocumentId()==selectedDocInfo.getPkDocumentId()){
                mDocList.remove(index);

            }
        }
        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE:
                if (resultCode == getActivity().RESULT_OK) {
                    Uri imageUri =data.getData();// getCacheImagePath(fileName);
                    File path = new File(getActivity().getExternalCacheDir(), "camera");
                    if (!path.exists()) path.mkdirs();
                    File image = new File(path, fileName);
                    //RetrofitService.getInstance(getContext()).uploadFile(getContext(),imageUri,image,userType,mServiceListeer);
                    //cropImage(getCacheImagePath(fileName));
                    selectedDocInfo.setImagePath(imageUri);
                    /*selectedDocInfo.setUploaded(true);
                    mDocList.add(selectedDocInfo);*/
                    RetrofitService.getInstance(getContext()).uploadDocument(image,mListener);
                } else {
                    //setResultCancelled();
                }
                break;
            case REQUEST_GALLERY_IMAGE:
                if (resultCode == getActivity().RESULT_OK) {
                    Uri imageUri = data.getData();
                    //cropImage(imageUri);
                    //fileName = imageUri.getPath();
                    File image = new File(getPath(imageUri));
                    fileName = image.getAbsolutePath();
                    //File image = new File(fileName);
                    //RetrofitService.getInstance(getContext()).uploadFile(getContext(),imageUri,image,userType,mServiceListeer);
                    selectedDocInfo.setImagePath(imageUri);
                    /*selectedDocInfo.setUploaded(true);
                    mDocList.add(selectedDocInfo);*/
                    RetrofitService.getInstance(getContext()).uploadDocument(image,mListener);
                } else {
                    //setResultCancelled();
                }
                break;


        }
        mAdapter.notifyDataSetChanged();

    }

    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index  = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }
}