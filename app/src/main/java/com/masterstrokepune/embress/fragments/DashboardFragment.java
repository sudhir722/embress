package com.masterstrokepune.embress.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.masterstrokepune.embress.BaseFragment;
import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.adapter.ListAdapter;
import com.masterstrokepune.embress.iface.onClick;
import com.masterstrokepune.embress.model.BillDetailModel;
import com.masterstrokepune.embress.model.BillInfoModel;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.request.BillDetailRequest;
import com.masterstrokepune.embress.retrofit.response.BillHistoryResponse;
import com.masterstrokepune.embress.util.EmbracePref;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class DashboardFragment extends BaseFragment implements onClick , RetrofitServiceListener {


    onClick mClickListener;
    RetrofitServiceListener mServiceListener;
    RecyclerView mRecycler;
    private BarChart chart;

    public DashboardFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mClickListener = this;
        mServiceListener = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_dashboard, container, false);
        mRecycler = view.findViewById(R.id.recyclerView);
        chart = (BarChart) view.findViewById(R.id.barChart);
        RetrofitService.getInstance(getContext()).history(new BillDetailRequest(EmbracePref.getCustomerId(getActivity())),mServiceListener);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //initFollouplist(view);

        setSpinner(view);
        setFrancListSpinner(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void setSpinner(View view) {
        Spinner spinner = (Spinner) view.findViewById(R.id.spn_interval);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.report_interval, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    private void setFrancListSpinner(View view) {
        Spinner spinner = (Spinner) view.findViewById(R.id.spn_franc);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.sel_fran, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    private void drawBarChart(List<BillDetailModel> modelList) {

        float barWidth;
        float barSpace;
        float groupSpace;

        barWidth = 0.2f;
        barSpace = 0f;
        groupSpace = 0.1f;

        chart.setDescription(null);
        chart.setPinchZoom(false);
        chart.setScaleEnabled(false);
        chart.setDrawBarShadow(false);
        chart.setDrawGridBackground(false);



        int groupCount = 6;

        ArrayList xVals = new ArrayList();
        ArrayList yVals1 = new ArrayList();
        ArrayList yVals2 = new ArrayList();
        Random rand = new Random();
        for (int index=0;index<modelList.size();index++) {
            xVals.add(""+index);

           // yVals1.add(new BarEntry((int)modelList.get(index).getUsedUnits(), (float) rand.nextInt(100)));
            //yVals2.add(new BarEntry((int)modelList.get(index).getUsedUnits(), (float) rand.nextInt(100)));

  //          yVals1.add(new BarEntry(69, (float) rand.nextInt(100)));
   //         yVals2.add(new BarEntry(54, (float) rand.nextInt(100)));
            yVals1.add(new BarEntry(modelList.get(index).getUsedUnits(), modelList.get(index).getUsedUnits()));
            yVals2.add(new BarEntry(modelList.get(index).getUsedUnits(), modelList.get(index).getUsedUnits()));
        }
//        xVals.add("Jul");
//        xVals.add("Jun");
//        xVals.add("May");
//        xVals.add("Apr");
//        xVals.add("Mar");



//       // yVals1.add(new BarEntry(69, (float) rand.nextInt(100)));
//        yVals2.add(new BarEntry(54, (float) rand.nextInt(100)));
//
//        //yVals1.add(new BarEntry(79, (float) rand.nextInt(100)));
//        yVals2.add(new BarEntry(64, (float) rand.nextInt(100)));
//
//      //  yVals1.add(new BarEntry(52, (float) rand.nextInt(100)));
//        yVals2.add(new BarEntry(64, (float) rand.nextInt(100)));
//
//      //  yVals1.add(new BarEntry(59, (float) rand.nextInt(100)));
//        yVals2.add(new BarEntry(54, (float) rand.nextInt(100)));
//
//    //    yVals1.add(new BarEntry(69, (float) rand.nextInt(100)));
//        yVals2.add(new BarEntry(54, (float) rand.nextInt(100)));
//


        BarDataSet set1,set2;
        set1 = new BarDataSet(yVals1, "Consumption 2020");
        //set2 = new BarDataSet(yVals1, "Consumption 2019");
        set1.setColor(Color.parseColor("#58D68D"));
        set2 = new BarDataSet(yVals2, "Consumption 2019");
        set2.setColor(Color.parseColor("#D0ECE7"));
        BarData data = new BarData(set1,set2);
        data.setValueFormatter(new LargeValueFormatter());
        chart.setData(data);
        chart.getBarData().setBarWidth(barWidth);
        chart.getXAxis().setAxisMinimum(0);
        chart.getXAxis().setAxisMaximum(0 + chart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
        chart.groupBars(0, groupSpace, barSpace);
        chart.getData().setHighlightEnabled(false);
        chart.invalidate();

        //Draw the X-Axis and Y-Axis
        //X-axis
        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);
        xAxis.setAxisMaximum(4);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
//Y-axis
        chart.getAxisRight().setEnabled(false);
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(true);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f);
        chart.animate();

    }



    public void initlist(List<BillDetailModel> models){
        List<BillInfoModel> eList = new ArrayList<>();


        ListAdapter mFilterAdapter = new ListAdapter(getActivity(),models,mClickListener);
        mRecycler.setAdapter(mFilterAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onOkClicked(Object object) {

    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

//    private void drawBar(List<BillDetailModel> billList){
//        //barChart = findViewById(R.id.BarChart);
//        //getEntries();
//        ArrayList<BarEntry> barEntries = new ArrayList<>();
//        for (int index=0;index<billList.size();index++)
//            barEntries.add(new BarEntry(20, index));
//
//
//        BarDataSet barDataSet = new BarDataSet(barEntries, "");
//        BarData barData = new BarData(barDataSet);
//        chart.setData(barData);
//        barDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
//        barDataSet.setValueTextColor(Color.BLACK);
//        barDataSet.setValueTextSize(18f);
//    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
        if(mObject instanceof BillHistoryResponse){
            BillHistoryResponse response = (BillHistoryResponse) mObject;
            if(response!=null){
                //initlist(response.getBillList());
                drawBar(response.getBillList());
            }
        }
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
    }


    public void drawBar(List<BillDetailModel> list){
        BarData data = createChartData(list);
        configureChartAppearance();
        prepareChartData(data);
    }

    private void prepareChartData(BarData data) {
        data.setValueTextSize(12f);
        chart.setData(data);
        chart.animate();
        chart.setClickable(false);
        chart.invalidate();
    }

    private void configureChartAppearance() {
        chart.getDescription().setEnabled(false);
        chart.setDrawValueAboveBar(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return "JUL";//DAYS[(int) value];
            }
        });

        YAxis axisLeft = chart.getAxisLeft();
        axisLeft.setGranularity(10f);
        axisLeft.setAxisMinimum(0);

        YAxis axisRight = chart.getAxisRight();
        axisRight.setGranularity(10f);
        axisRight.setAxisMinimum(0);
    }

    private BarData createChartData(List<BillDetailModel> list) {
        ArrayList<BarEntry> values = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            float x = i;
            float y = list.get(i).getUsedUnits();
            values.add(new BarEntry(x, y));
        }

        BarDataSet set1 = new BarDataSet(values, "Consumption");

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);

        return data;
    }

}
