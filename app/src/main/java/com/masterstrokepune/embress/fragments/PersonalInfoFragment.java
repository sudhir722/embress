package com.masterstrokepune.embress.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.masterstrokepune.embress.BaseFragment;
import com.masterstrokepune.embress.NewCustomerActivity;
import com.masterstrokepune.embress.R;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.request.NewConnectionRequest;
import com.masterstrokepune.embress.retrofit.request.ProjectRequest;
import com.masterstrokepune.embress.retrofit.response.AreaListResponse;
import com.masterstrokepune.embress.retrofit.response.ProjectListResponse;
import com.masterstrokepune.embress.util.EmbracePref;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PersonalInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonalInfoFragment extends BaseFragment implements RetrofitServiceListener, View.OnClickListener {

    Spinner spnArea,spnProject;
    AreaListResponse mArea;
    ProjectListResponse mProject;
    RetrofitServiceListener mServiceListener;
    Button btnNext;
    EditText edt_cname,edt_fname,edt_mname;
    int areaId=-1,projectId=-1;
    NewConnectionRequest mRequest;
    public PersonalInfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PersonalInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PersonalInfoFragment newInstance(NewConnectionRequest request) {
        PersonalInfoFragment fragment = new PersonalInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data",request);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceListener = this;
        if(getArguments()!=null && getArguments().containsKey("data")){
            try {
                mRequest = (NewConnectionRequest) getArguments().getSerializable("data");
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_personal_info, container, false);
        spnArea = view.findViewById(R.id.spn_area);
        btnNext = view.findViewById(R.id.register);
        edt_cname = view.findViewById(R.id.edt_cname);
        edt_fname = view.findViewById(R.id.edt_fname);
        edt_mname = view.findViewById(R.id.edt_mname);
        btnNext.setOnClickListener(this);
        spnProject = view.findViewById(R.id.spn_proj);
        addAreaListener();
        addProjectListener();
        setData();
        return view;
    }

    private void setData() {
        try{
            edt_cname.setText(mRequest.getCustomerName());
            edt_fname.setText(mRequest.getFatherName());
            edt_mname.setText(mRequest.getMotherName());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean isValidForm(){
        if(areaId==-1){
            showErrorMessageSheet(getChildFragmentManager(),"Alert","Please select Area");
        }else if(projectId==-1){
            showErrorMessageSheet(getChildFragmentManager(),"Alert","Please select Projet");
        }else if(TextUtils.isEmpty(edt_cname.getText().toString())){
            showErrorMessageSheet(getChildFragmentManager(),"Alert","Please Enter your Name");
            edt_cname.requestFocus();
        }/*else if(TextUtils.isEmpty(edt_fname.getText().toString())){
            showErrorMessageSheet(getChildFragmentManager(),"Alert","Please Enter your Father Name");
            edt_fname.requestFocus();
        }else if(TextUtils.isEmpty(edt_mname.getText().toString())){
            showErrorMessageSheet(getChildFragmentManager(),"Alert","Please Enter your Mother Name");
            edt_mname.requestFocus();
        }*/else{
            return true;
        }
        return false;
    }

    private void addProjectListener() {
        spnProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position>0) {
                    //initProject(mProject.getProjectId(position - 1));
                    projectId = mProject.getProjectId(position - 1);
                    EmbracePref.setProjectId(getContext(),mProject.getProjectId(position - 1));
                    EmbracePref.setProjectName(getContext(),spnProject.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void addAreaListener() {
        spnArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position>0) {
                    initProject(mArea.getAreaId(position - 1));
                    areaId = mArea.getAreaId(position - 1);
                    EmbracePref.setAreaId(getContext(),mArea.getAreaId(position - 1));
                    EmbracePref.setAreaName(getContext(),spnArea.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RetrofitService.getInstance(getContext()).getAreaList(this);
    }

    private void setArea(AreaListResponse response){
        try {
            String[] arealist = new String[response.getAreaList().size()+1];
            arealist[0] = "Select Area";
            for (int index = 0; index < response.getAreaList().size(); index++) {
                arealist[index + 1] = response.getArea(index);
            }
            ArrayAdapter aa = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, arealist);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            spnArea.setAdapter(aa);
        }catch (Exception e){
            e.printStackTrace();
            //showErrorMessageSheet(getChildFragmentManager(),"Alert","Something went wrong");
        }
    }

    private void setProject(ProjectListResponse response){
        try {
            String[] projectlist = new String[response.getProjectList().size()+1];
            projectlist[0] = "Select Area";
            for (int index = 0; index < response.getProjectList().size(); index++) {
                projectlist[index + 1] = response.getProject(index);
            }
            ArrayAdapter aa = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, projectlist);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            spnProject.setAdapter(aa);
        }catch (Exception e){
            e.printStackTrace();
          //  showErrorMessageSheet(getChildFragmentManager(),"Alert","Something went wrong");
        }
    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

    private void initProject(int areaId){
        RetrofitService.getInstance(getContext()).getProjectList(new ProjectRequest(areaId),mServiceListener);
    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
        if(mObject instanceof AreaListResponse){
            AreaListResponse mAreaResponse = (AreaListResponse) mObject;
            if(mAreaResponse!=null){
                mArea = mAreaResponse;
                setArea(mAreaResponse);
            }
        }else if(mObject instanceof ProjectListResponse){
            ProjectListResponse mProjectResponse = (ProjectListResponse) mObject;
            if(mProjectResponse!=null){
                mProject = mProjectResponse;
                setProject(mProjectResponse);
            }
        }
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==btnNext.getId()){
            if(isValidForm()) {
                ((NewCustomerActivity) getActivity()).onPersonalDetailSaved(edt_cname.getText().toString(),
                        edt_fname.getText().toString(),
                        edt_mname.getText().toString());
            }
        }
    }
}