package com.masterstrokepune.embress;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.adapter.HomeRecyclerAdapter;
import com.masterstrokepune.embress.bs.BSLogoutDialogFragment;
import com.masterstrokepune.embress.fragments.ContactFragmentBs;
import com.masterstrokepune.embress.fragments.ProfileFragmentBs;
import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.iface.onMenuClicked;
import com.masterstrokepune.embress.model.BillDetailModel;
import com.masterstrokepune.embress.model.MenuInfo;
import com.masterstrokepune.embress.notifications.ForceUpdateChecker;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.request.BillDetailRequest;
import com.masterstrokepune.embress.retrofit.request.GenerateReceiptRequest;
import com.masterstrokepune.embress.retrofit.request.NotificaionRequest;
import com.masterstrokepune.embress.retrofit.response.BillHistoryResponse;
import com.masterstrokepune.embress.retrofit.response.GenerateReceiptResponse;
import com.masterstrokepune.embress.retrofit.response.GenericResponse;
import com.masterstrokepune.embress.util.EmbracePref;
import com.masterstrokepune.embress.util.LocalConstance;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.masterstrokepune.embress.util.LocalConstance.IS_LOYOUT;

public class HomeScreen extends BaseActivity implements OnDialogClickListener,onMenuClicked, RetrofitServiceListener, PaymentResultListener, View.OnClickListener, ForceUpdateChecker.OnUpdateNeededListener, PopupMenu.OnMenuItemClickListener {
    RecyclerView recyclerView;
    HomeRecyclerAdapter adapter;
    TextView txt_user,desc,paynow,txt_header,txt_desc;
    RetrofitServiceListener mServiceListener;
    CardView paymentView;
    BillDetailModel mBillInfo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);
        ((ImageView)findViewById(R.id.action_help)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHelpFile();
                //9637727729
            }
        });

        recyclerView = findViewById(R.id.homelist);
        paymentView = findViewById(R.id.lin_payment);
        txt_user = findViewById(R.id.txt_user);
        desc = findViewById(R.id.desc);
        paynow = findViewById(R.id.paynow);
        txt_desc = findViewById(R.id.txt_desc);
        txt_header = findViewById(R.id.txt_header);
        mServiceListener = this;
        txt_user.setText(EmbracePref.getDisplayName(getApplicationContext()));
        desc.setText(EmbracePref.getEmailId(getApplicationContext()));
        int numberOfColumns = 2;
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        adapter = new HomeRecyclerAdapter(this, getMenuList(),this );
        recyclerView.setAdapter(adapter);
        paynow.setOnClickListener(this);

        RetrofitService.getInstance(getApplicationContext()).history(new BillDetailRequest(EmbracePref.getCustomerId(getApplicationContext())),mServiceListener);

        ForceUpdateChecker.with(this).onUpdateNeeded(this).check();
        //startActivity(new Intent(getApplicationContext(),SmartMeterActivity.class));
    }

    public List<MenuInfo> getMenuList(){
        List<MenuInfo> menuInfoList = new ArrayList<>();
        menuInfoList.add(new MenuInfo("Smart Reading","","",R.drawable.career,1));
        menuInfoList.add(new MenuInfo("View Bill History","","",R.drawable.idea,2));
        menuInfoList.add(new MenuInfo("Add/View Complaints","","",R.drawable.medical,3));
        menuInfoList.add(new MenuInfo("Profile","","",R.drawable.ic_userprofile,4));
        menuInfoList.add(new MenuInfo("Quick Contact","","",R.drawable.contact,5));
        return menuInfoList;
    }

    @Override
    public void onMenuClick(MenuInfo filtersInfo) {
        switch (filtersInfo.getMenuId()){
            case 1:
                //Smart Reading
                startActivity(new Intent(getApplicationContext(),SmartMeterActivity.class));
                break;
         case 2:
                //History
                startActivity(new Intent(getApplicationContext(),BillHistoryActivity.class));
                break;
         case 3:
                //Smart Reading
                startActivity(new Intent(getApplicationContext(),ComplantActivity.class));
                break;
         case 4:
                //Profile
             ProfileFragmentBs bottomSheetFragment = new ProfileFragmentBs();
             bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                break;
            case 5:
                ContactFragmentBs cbs = new ContactFragmentBs();
                cbs.show(getSupportFragmentManager(), cbs.getTag());
                break;
        }
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        popup.setOnMenuItemClickListener(this);
        inflater.inflate(R.menu.menu_main, popup.getMenu());
        popup.show();
    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

    @Override
    public void onResponse(Object mObject) {
        if(mObject instanceof GenerateReceiptResponse){
            GenerateReceiptResponse response = (GenerateReceiptResponse) mObject;
            if(response!=null){
                showErrorMessageSheet(getSupportFragmentManager(), "SUCCESS", response.getMessage() + " " + response.getReceiptId(), false, new OnDialogClickListener() {
                    @Override
                    public void onOkClicked(Object object) {
                        RetrofitService.getInstance(getApplicationContext()).history(new BillDetailRequest(EmbracePref.getCustomerId(getApplicationContext())),mServiceListener);
                    }

                    @Override
                    public void onCancelClicked() {

                    }
                });
            }
        }else if(mObject instanceof BillHistoryResponse){
            BillHistoryResponse response = (BillHistoryResponse) mObject;
            paymentView.setVisibility(View.GONE);
            if(response!=null){
                if(response.getBillList()==null || response.getBillList().size()==0){

                }else if(response.getBillList().size()>=0){
                    if(response.getBillList().get(0).getStaus().equalsIgnoreCase("Bill Paid")) {
                        paymentView.setVisibility(View.GONE);
                    }else{

                        mBillInfo = response.getBillList().get(0);
                        txt_header.setText(mBillInfo.getBillDueAmount()+" "+getString(R.string.inr));
                        txt_desc.setText(mBillInfo.getUsedUnits()+" Unit Used");
                        paymentView.setVisibility(View.VISIBLE);
                    }

                }
                getFCMId();
            }
        }
        hideDialog();
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
// After successful payment Razorpay send back a unique id
        hideDialog();
        String narration ="Rezorpay(Android) "+razorpayPaymentID+" for "+mBillInfo.getPkBillvoucherEntryId();
        GenerateReceiptRequest request = new GenerateReceiptRequest(mBillInfo.getCustomerUserNo(),mBillInfo.getPkBillvoucherEntryId(),mBillInfo.getBillDueAmount(),narration,mBillInfo.getBillDueAmount(),EmbracePref.getCustomerId(getApplicationContext()),mBillInfo.getPkBillvoucherEntryId());
        RetrofitService.getInstance(getApplicationContext()).addReceipt(request,razorpayPaymentID,mServiceListener);
        Toast.makeText(HomeScreen.this, "Transaction Successful: " + razorpayPaymentID, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPaymentError(int i, String error) {
// Error message
        hideDialog();
        String message ="Unable to Complete transaction";
        try {
            JSONObject obj = new JSONObject(error);
            message = obj.getJSONObject("error").getString("description");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String mMessage = message;
        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showErrorMessageSheet(getSupportFragmentManager(), "Payment Failed", mMessage, false, new OnDialogClickListener() {
                    @Override
                    public void onOkClicked(Object object) {

                    }

                    @Override
                    public void onCancelClicked() {

                    }
                });
            }
        });
*/
        Toast.makeText(HomeScreen.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        String convertedAmount=String.valueOf(+mBillInfo.getBillDueAmount()*100);
        rezorpayCall(convertedAmount);
        showProgressDialog("");
    }

    public void rezorpayCall(String convertedAmount){
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        String razorpayKey = "rzp_live_7GG2IWz7oph7DN"; //Generate your razorpay key from Settings-> API Keys-> copy Key Id
        Checkout chackout = new Checkout();
        chackout.setKeyID(razorpayKey);
        try {
            JSONObject options = new JSONObject();
            options.put("name", EmbracePref.getDisplayName(getApplicationContext()));
            options.put("description", "Bill Payment for rs "+mBillInfo.getBillDueAmount());
            options.put("currency", "INR");
            options.put("amount", convertedAmount);

            JSONObject preFill = new JSONObject();
            preFill.put("email", EmbracePref.getEmailId(getApplicationContext()));
            preFill.put("contact", EmbracePref.getMobileNo(getApplicationContext()));
            options.put("prefill", preFill);

            chackout.open(HomeScreen.this, options);
        } catch (Exception e) {
            Toast.makeText(HomeScreen.this, "Error in payment: " + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void getFCMId() {
        /*FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            //Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        String token = task.getResult().getToken();

                        EmbracePref.setFCM(HomeScreen.this, token);
                        // Get new Instance ID token
                        if (!TextUtils.isEmpty(token)) {
                            NotificaionRequest request = new NotificaionRequest(""+EmbracePref.getCustomerId(getApplicationContext()),token);
                            RetrofitService.getInstance(getApplicationContext()).updateFCM(request,null);
                        }
                        Log.d("EMB_FCM", token);


                    }
                });*/

    }

    @Override
    public void onUpdateNeeded(String updateUrl) {
        onUpdateAvaliable();
    }

    private void onUpdateAvaliable() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HomeScreen.this);
        alertDialogBuilder.setTitle("New update available !");
        alertDialogBuilder.setMessage("Update is available to download." +
                "Downloading the latest update you will get updated features, improvements" +
                " and bug fixes of Embress");
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                HomeScreen.this.finish();
            }
        });
        alertDialogBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent updateAppIntent = new Intent(Intent.ACTION_VIEW);
                updateAppIntent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                startActivity(updateAppIntent);
                HomeScreen.this.finish();
            }
        });
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.create();
        // alertDialogBuilder.show();
    }

    public void showLogOutDialog(){
        BSLogoutDialogFragment bottomSheetFragment = new BSLogoutDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LocalConstance.CONST_TITLE, getString(R.string.app_name));
        bundle.putString(LocalConstance.CONST_MESSAGE, getString(R.string.txt_logout_message));
        bundle.putSerializable(LocalConstance.CONST_LISTENER, this);
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        showLogOutDialog();
        return false;
    }

    @Override
    public void onOkClicked(Object object) {
        if(object instanceof GenericResponse){
            GenericResponse response = (GenericResponse) object;
            if(((GenericResponse) object).getRequestCode()==IS_LOYOUT){
                EmbracePref.clear(getApplicationContext());
                //navigateLogin();
                finish();
            }
        }
    }

    @Override
    public void onCancelClicked() {

    }
}
