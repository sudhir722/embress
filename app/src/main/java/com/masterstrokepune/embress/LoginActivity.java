package com.masterstrokepune.embress;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.model.CustomerInfo;
import com.masterstrokepune.embress.model.UserModel;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.response.CustomerDetailsResponse;
import com.masterstrokepune.embress.retrofit.response.LoginResponse;
import com.masterstrokepune.embress.util.EmbracePref;


public class LoginActivity extends BaseActivity implements View.OnClickListener, RetrofitServiceListener {

    Button btnLogin;

    EditText edtUserName,edtPassword;
    RetrofitServiceListener mServiceListener;
    TextView txtForgot;
    TextView forgotpass;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        btnLogin = findViewById(R.id.cirLoginButton);
        ((ImageView)findViewById(R.id.action_help)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHelpFile();
                //9637727729
            }
        });
        forgotpass = findViewById(R.id.forgotpass);
        btnLogin.setOnClickListener(this);
        forgotpass.setOnClickListener(this);
        edtPassword = findViewById(R.id.edt_password);
        edtUserName = findViewById(R.id.edt_username);
        mServiceListener = this;
        txtForgot = findViewById(R.id.forgotpass);
        txtForgot.setOnClickListener(this);
        autoLogin();
    }

    public void autoLogin(){
        if(getIntent()!=null && getIntent().getExtras()!=null && getIntent().getExtras().containsKey("uname")){
            RetrofitService.getInstance(LoginActivity.this).checkLogin(getIntent().getExtras().getString("uname"), getIntent().getExtras().getString("pass"), mServiceListener);
        }
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.forgotpass) {
            startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
        }else if(v.getId() == R.id.cirLoginButton){
            if(isValidForm()) {
                RetrofitService.getInstance(LoginActivity.this).checkLogin(edtUserName.getText().toString(), edtPassword.getText().toString(), mServiceListener);
            }
        }
    }

    private boolean isValidForm(){
        String uName = edtUserName.getText().toString();
        String uPass = edtPassword.getText().toString();

        if(TextUtils.isEmpty(uName)){
            showErrorAlert("Error","Please Enter User name and try again");
            return false;
        }else if(TextUtils.isEmpty(uPass)){
            showErrorAlert("Error","Please Enter your password and try again");
            return false;
        }else{
            return true;
        }

    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("Please wait");
    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
        if(mObject instanceof CustomerDetailsResponse) {
            CustomerDetailsResponse custmerInfo = (CustomerDetailsResponse) mObject;
            CustomerInfo cInfo = custmerInfo.getCustomerInfo();
            EmbracePref.setCustomerId(getApplicationContext(),cInfo.getPkcustomerid());
            EmbracePref.setDisplayName(getApplicationContext(),cInfo.getCustomerName());
            EmbracePref.setMobileNo(getApplicationContext(),cInfo.getMobileNo());
            EmbracePref.setConsumerNo(getApplicationContext(),cInfo.getConsumerno());
            EmbracePref.setAddress(getApplicationContext(),cInfo.getAddress());
            EmbracePref.setEmailId(getApplicationContext(),cInfo.getEmailId());
            navigateLogin();
        }else if(mObject instanceof LoginResponse) {
            LoginResponse mUserResponse = (LoginResponse) mObject;
            if(mUserResponse!=null && mUserResponse.getData()!=null && mUserResponse.getData().get$id()>=0){
                //navigateLogin();
                UserModel userInfo = mUserResponse.getData();
                EmbracePref.setCustomerId(getApplicationContext(),userInfo.getPkcustomerid());
                EmbracePref.setUserId(getApplicationContext(),userInfo.getPkcustomerid());
                EmbracePref.setConsumerNo(getApplicationContext(),userInfo.getConsumerno());
                EmbracePref.setUserName(getApplicationContext(),userInfo.getCustomerName());
                EmbracePref.setDisplayName(getApplicationContext(),userInfo.getCustomerName());
                EmbracePref.setAddress(getApplicationContext(),userInfo.getAddress());
                EmbracePref.setMobileNo(getApplicationContext(),userInfo.getMobileNo());
                EmbracePref.setEmailId(getApplicationContext(),userInfo.getEmailId());
                EmbracePref.setUsedUnit(getApplicationContext(),userInfo.getUsedUnits());
                //RetrofitService.getInstance(getApplicationContext()).getCustomerDetails(EmbracePref.getUserId(getApplicationContext()),mServiceListener);
                navigateLogin();
            }else{
                showErrorAlert(getString(R.string.title_error),mUserResponse.getMessage());
            }
  //          if(mUserResponse!=null) {
//                if(mUserResponse.getResponseCode()== LocalConstance.ACTION_OK){

//                    if(mUserResponse.getData()!=null && mUserResponse.getData().getUserInfoModels()!=null && mUserResponse.getData().getUserInfoModels().size()>0){
//                        UserInfoModel userModel = mUserResponse.getData().getUserInfoModels().get(0);
//                        clearMenus();
//                        if(userModel.getIsValidLogin()) {
//                            if(mUserResponse.getData().getMenuList()!=null){
//                                for (int index=0;index<mUserResponse.getData().getMenuList().size();index++){
//                                    new MenuModel(mUserResponse.getData().getMenuList().get(index).getMenu_Id(),mUserResponse.getData().getMenuList().get(index).getMenuText()).save();
//                                }
//                            }
//
//                            EmbracePref.setUserId(LoginActivity.this, (int) userModel.getUser_id());
//                            EmbracePref.setUserName(LoginActivity.this, userModel.getUser_name());
//                            EmbracePref.setUserType(LoginActivity.this, userModel.getUser_Type());
//                            EmbracePref.setUserTypeName(LoginActivity.this, userModel.getUser_TypeName());
//                            EmbracePref.setUID(LoginActivity.this, userModel.getUID());
//                            EmbracePref.setUserCode(LoginActivity.this, userModel.getUser_code());
//                            EmbracePref.setUserCode(LoginActivity.this, userModel.getUser_code());
//                            EmbracePref.setDisplayName(LoginActivity.this, userModel.getDisplay_name());
//                            EmbracePref.setContactPerson(LoginActivity.this, userModel.getDisplay_name());
//                            EmbracePref.setEmailId(LoginActivity.this, userModel.getEmail_id());
//                            EmbracePref.setMobileNo(LoginActivity.this, userModel.getMobile_no());
//                            EmbracePref.setCanEdit(LoginActivity.this, userModel.getCanEdit());
//                            EmbracePref.setExternalUser(LoginActivity.this, userModel.getIsExternalUser());
//                            EmbracePref.setRefFranid(LoginActivity.this, (int) userModel.getRef_Fran_Id());
//
//                            navigateDashboard();
//                        }else{
//                            showErrorAlert(getString(R.string.title_error),getString(R.string.er_invalidlogin));
//                        }
//                    }


     //           }else{
//                    if(TextUtils.isEmpty(mUserResponse.getResponseMessage())){
//                        showErrorAlert(getString(R.string.title_error),getString(R.string.er_inff_connection));
//                    }else{
//                        showErrorAlert(getString(R.string.title_error),mUserResponse.getResponseMessage());
//                    }
   //             }


            }else{
                showErrorAlert(getString(R.string.title_error),getString(R.string.er_inff_connection));
            }
            hideDialog();
        }

    private void navigateLogin() {
        startActivity(new Intent(getApplicationContext(),HomeScreen.class));
        finish();
    }


    @Override
    public void onFailure(Object mObject, Throwable t) {
        hideDialog();
        edtPassword.setText("");
        edtUserName.setText("");
        edtUserName.requestFocus();
        showErrorAlert(getString(R.string.title_error),"Please Enter valid User Name and Password");
    }

    public void newuser(View view) {
        //startActivity(new Intent(getApplicationContext(),NewCustomerActivity.class));
        showMobileVerificaiton(getSupportFragmentManager(), new OnDialogClickListener() {
            @Override
            public void onOkClicked(Object object) {

            }

            @Override
            public void onCancelClicked() {

            }
        });
    }

}
