package com.masterstrokepune.embress.iface;

import java.io.Serializable;

public interface OnDialogClickListener extends Serializable {

    public void onOkClicked(Object object);
    public void onCancelClicked();
}
