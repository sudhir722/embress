package com.masterstrokepune.embress.iface;


import com.masterstrokepune.embress.model.MenuInfo;

public interface onMenuClicked {

    void onMenuClick(MenuInfo filtersInfo);
  
}
