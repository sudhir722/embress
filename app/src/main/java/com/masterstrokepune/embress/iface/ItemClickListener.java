package com.masterstrokepune.embress.iface;

public interface ItemClickListener {
    void onItemClickListener(Object object);
}
