package com.masterstrokepune.embress.retrofit;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.masterstrokepune.embress.util.EmbracePref;

import java.io.IOException;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetroClient {

    private static final String ROOT_URL = "https://embracemobile.masterstrokepune.in";

    private static Retrofit getRetrofitInstance(final Context context) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        TimeZone tz = TimeZone.getDefault();
                        //System.out.println("TimeZone   "+tz.getDisplayName(false, TimeZone.SHORT)+" Timezon id :: " +tz.getID());
                        Request request = chain.request().newBuilder()
                                .addHeader("user_id", ""+ EmbracePref.getUserId(context))
                                .addHeader("tz_id",tz.getID()).addHeader("tz_name", tz.getDisplayName(false, TimeZone.SHORT)).build();
                        return chain.proceed(request);
                    }
                })
                .build();

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private static Retrofit getRetrofitInstanceFile(final Context context) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(40, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        TimeZone tz = TimeZone.getDefault();
                        //System.out.println("TimeZone   "+tz.getDisplayName(false, TimeZone.SHORT)+" Timezon id :: " +tz.getID());
                        Request request = chain.request().newBuilder()
                                .addHeader("user_id", ""+ EmbracePref.getUserId(context))
                                .addHeader("tz_id",tz.getID()).addHeader("tz_name", tz.getDisplayName(false, TimeZone.SHORT)).build();
                        return chain.proceed(request);
                    }
                })
                .build();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.interceptors().add(interceptor);


        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }


    private static Retrofit getRetrofitInstanceMap(final Context context) {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(40, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {

                        TimeZone tz = TimeZone.getDefault();
                        //System.out.println("TimeZone   "+tz.getDisplayName(false, TimeZone.SHORT)+" Timezon id :: " +tz.getID());
                        Request request = chain.request().newBuilder()
                                .addHeader("user_id", ""+EmbracePref.getUserId(context))
                                .addHeader("tz_id",tz.getID()).addHeader("tz_name", tz.getDisplayName(false, TimeZone.SHORT)).build();
                        return chain.proceed(request);
                    }
                })
                .build();

        return new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static ApiService getApiService(Context context) {
        return getRetrofitInstance(context).create(ApiService.class);
    }

    public static ApiService getApiServiceFile(Context context) {
        return getRetrofitInstanceFile(context).create(ApiService.class);
    }


}
