package com.masterstrokepune.embress.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.masterstrokepune.embress.model.CustomerModel;

import java.io.Serializable;
import java.util.List;

public class ConnectionResponse implements Serializable {

    @SerializedName("custdetails1")
    @Expose
    List<CustomerModel> connectionStatusList1;

    @SerializedName("custdetails")
    @Expose
    List<CustomerModel> connectionStatusList;

    public List<CustomerModel> getConnectionStatusList1() {
        return connectionStatusList1;
    }

    public void setConnectionStatusList1(List<CustomerModel> connectionStatusList1) {
        this.connectionStatusList1 = connectionStatusList1;
    }

    public ConnectionResponse(List<CustomerModel> connectionStatusList) {
        this.connectionStatusList = connectionStatusList;
    }

    public List<CustomerModel> getConnectionStatusList() {
        return connectionStatusList;
    }

    public void setConnectionStatusList(List<CustomerModel> connectionStatusList) {
        this.connectionStatusList = connectionStatusList;
    }
}
