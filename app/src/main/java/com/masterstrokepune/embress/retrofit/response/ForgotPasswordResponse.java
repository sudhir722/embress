package com.masterstrokepune.embress.retrofit.response;

public class ForgotPasswordResponse {

    String msg;

    public ForgotPasswordResponse() {
    }

    public ForgotPasswordResponse(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
