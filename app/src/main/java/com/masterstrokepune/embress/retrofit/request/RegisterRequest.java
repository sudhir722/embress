package com.masterstrokepune.embress.retrofit.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterRequest {

    @SerializedName("ConsumerNo")
    @Expose
    private String consumerNo;

    @SerializedName("MobileNo")
    @Expose
    private String mobile;

    @SerializedName("EmailId")
    @Expose
    private String email;


    @SerializedName("LoginName")
    @Expose
    private String loginname;

    @SerializedName("ConfirmPassword")
    @Expose
    private String password;

    @SerializedName("Password")
    @Expose
    private String pass;

    @SerializedName("id")
    @Expose
    private int  id;

    public RegisterRequest(String consumerNo, String mobile, String email, String loginname, String password, String pass, int id) {
        this.consumerNo = consumerNo;
        this.mobile = mobile;
        this.email = email;
        this.loginname = loginname;
        this.password = password;
        this.pass = pass;
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
