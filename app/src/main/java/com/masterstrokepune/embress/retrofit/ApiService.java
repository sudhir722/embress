package com.masterstrokepune.embress.retrofit;


import com.masterstrokepune.embress.retrofit.request.AddCommantRequest;
import com.masterstrokepune.embress.retrofit.request.BillDetailRequest;
import com.masterstrokepune.embress.retrofit.request.BillInfoRequest;
import com.masterstrokepune.embress.retrofit.request.ComplaintHistoryRequest;
import com.masterstrokepune.embress.retrofit.request.ComplantRequest;
import com.masterstrokepune.embress.retrofit.request.CustomerDetailRequest;
import com.masterstrokepune.embress.retrofit.request.ForgotPasswordRequest;
import com.masterstrokepune.embress.retrofit.request.ForgotUserNameRequest;
import com.masterstrokepune.embress.retrofit.request.GenerateReceiptRequest;
import com.masterstrokepune.embress.retrofit.request.LoginRequest;
import com.masterstrokepune.embress.retrofit.request.MeterReadingRequest;
import com.masterstrokepune.embress.retrofit.request.MobileVerificationRequest;
import com.masterstrokepune.embress.retrofit.request.NewConnectionRequest;
import com.masterstrokepune.embress.retrofit.request.NotificaionRequest;
import com.masterstrokepune.embress.retrofit.request.ProjectRequest;
import com.masterstrokepune.embress.retrofit.request.RegisterRequest;
import com.masterstrokepune.embress.retrofit.response.AreaListResponse;
import com.masterstrokepune.embress.retrofit.response.BillHistoryResponse;
import com.masterstrokepune.embress.retrofit.response.BillInfoResponse;
import com.masterstrokepune.embress.retrofit.response.CategoryResponse;
import com.masterstrokepune.embress.retrofit.response.ComplaintHistoryResponse;
import com.masterstrokepune.embress.retrofit.response.ConnectionResponse;
import com.masterstrokepune.embress.retrofit.response.CustomerDetailsResponse;
import com.masterstrokepune.embress.retrofit.response.DocumentListModel;
import com.masterstrokepune.embress.retrofit.response.ForgotPasswordResponse;
import com.masterstrokepune.embress.retrofit.response.GenerateReceiptResponse;
import com.masterstrokepune.embress.retrofit.response.LoginResponse;
import com.masterstrokepune.embress.retrofit.response.MeterReadingResponse;
import com.masterstrokepune.embress.retrofit.response.ProjectListResponse;
import com.masterstrokepune.embress.retrofit.response.RegistrationResponse;
import com.masterstrokepune.embress.retrofit.response.TicketResponse;
import com.masterstrokepune.embress.retrofit.response.UploadImageResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiService {

    @POST("/api/Login/UserLogin")
    Call<LoginResponse> checkLogin(@Body LoginRequest body);

    @POST("/api/UserRegistration")
    Call<RegistrationResponse> register(@Body RegisterRequest body);

    @POST("/api/GetBillDetails")
    Call<BillHistoryResponse> billHistory(@Body BillDetailRequest body);

    @POST("/api/GetCustomerDetails")
    Call<CustomerDetailsResponse> getCustomerDetails(@Body CustomerDetailRequest body);

    @POST("/api/DisplayCategory")
    Call<CategoryResponse> getCategoryList();

    @POST("/api/AddTicketDetails")
    Call<TicketResponse> addTicket(@Body ComplantRequest request);

    @POST("/api/DisplayComplaintHistoy")
    Call<ComplaintHistoryResponse> getComplaintList(@Body ComplaintHistoryRequest request);

    @POST("/api/AddTicketStatus")
    Call<Object> insertCommant(@Body AddCommantRequest request);

    @POST("/api/ForgotPassword")
    Call<ForgotPasswordResponse> forgotPassword(@Body ForgotPasswordRequest request);

    @POST("/api/ForgotLoginName")
    Call<ForgotPasswordResponse> forgotLoginName(@Body ForgotUserNameRequest request);

    @POST("/api/AddReceiptDetails")
    Call<GenerateReceiptResponse> addReceipt(@Body GenerateReceiptRequest request);

    @POST("/api/UpdateLogin")
    Call<Object> updateFCM(@Body NotificaionRequest request);

    @POST("/api/AddMeterrading")
    Call<MeterReadingResponse> addMeterReading(@Body MeterReadingRequest request);

    @POST("/api/DisplayBillDetails")
    Call<BillInfoResponse> getBillDetails(@Body BillInfoRequest request);

    @POST("/api/DisplayDocumentDetails")
    Call<DocumentListModel> getKycDocuments();

    @POST("/api/DisplayArea")
    Call<AreaListResponse> getAreaList();

    @POST("/api/DisplayProject")
    Call<ProjectListResponse> getProjectList(@Body ProjectRequest request);

    @POST("/api/GetCustomersVerifiedStatus")
    Call<ConnectionResponse> getMobileVerification(@Body MobileVerificationRequest request);

    @POST("/AddKYCCustomer/AddKYCDetails")
    Call<String> onNewCustomer(@Body NewConnectionRequest request);

    @Multipart
    @Headers({
            "Content-Type: Multiprt/form-data"
    })
    @POST("/Readingupload/UploadImage")
    Call<Object> uploadMeterPic(
            @Part("pkcustomerid") RequestBody pkcustomerid,
            @Part("metereading") RequestBody meterreading,
            @Part MultipartBody.Part file
    );

    @Multipart
    @POST("/ReadingUpload/UploadImage")
    @Headers({
            "Content-Type: Multiprt/form-data"
    })
    Call<Object> upload(
                        @Part("file\"; filename=\"pp.png\" ") RequestBody file,
                        @Part("pkcustomerid") RequestBody fname,
                        @Part("meterreading") RequestBody id);

    @Multipart
    @POST("/ReadingUpload/UploadImage")
    Call<Object> addRecord(@Part MultipartBody.Part file, @Part("pkcustomerid") RequestBody name,@Part("metereading") RequestBody reding);

    @Multipart
    @POST("/UploadKYCDocument/UploadKYCDoc")
    Call<UploadImageResponse> uploadDocument(@Part MultipartBody.Part image);

}
