package com.masterstrokepune.embress.retrofit.request;

public class NotificaionRequest {

    String PkCustomerId;
    String FCMIId;

    public NotificaionRequest(String pkMbUser, String FCMIId) {
        PkCustomerId = pkMbUser;
        this.FCMIId = FCMIId;
    }

    public String getPkMbUser() {
        return PkCustomerId;
    }

    public void setPkMbUser(String pkMbUser) {
        PkCustomerId = pkMbUser;
    }

    public String getFCMIId() {
        return FCMIId;
    }

    public void setFCMIId(String FCMIId) {
        this.FCMIId = FCMIId;
    }
}
