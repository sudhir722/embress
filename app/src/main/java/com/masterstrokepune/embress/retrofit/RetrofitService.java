package com.masterstrokepune.embress.retrofit;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.masterstrokepune.embress.retrofit.request.AddCommantRequest;
import com.masterstrokepune.embress.retrofit.request.BillDetailRequest;
import com.masterstrokepune.embress.retrofit.request.BillInfoRequest;
import com.masterstrokepune.embress.retrofit.request.ComplaintHistoryRequest;
import com.masterstrokepune.embress.retrofit.request.ComplantRequest;
import com.masterstrokepune.embress.retrofit.request.CustomerDetailRequest;
import com.masterstrokepune.embress.retrofit.request.ForgotPasswordRequest;
import com.masterstrokepune.embress.retrofit.request.ForgotUserNameRequest;
import com.masterstrokepune.embress.retrofit.request.GenerateReceiptRequest;
import com.masterstrokepune.embress.retrofit.request.LoginRequest;
import com.masterstrokepune.embress.retrofit.request.MeterReadingRequest;
import com.masterstrokepune.embress.retrofit.request.MobileVerificationRequest;
import com.masterstrokepune.embress.retrofit.request.NewConnectionRequest;
import com.masterstrokepune.embress.retrofit.request.NotificaionRequest;
import com.masterstrokepune.embress.retrofit.request.ProjectRequest;
import com.masterstrokepune.embress.retrofit.request.RegisterRequest;
import com.masterstrokepune.embress.retrofit.response.AreaListResponse;
import com.masterstrokepune.embress.retrofit.response.BillHistoryResponse;
import com.masterstrokepune.embress.retrofit.response.BillInfoResponse;
import com.masterstrokepune.embress.retrofit.response.CategoryResponse;
import com.masterstrokepune.embress.retrofit.response.ComplaintHistoryResponse;
import com.masterstrokepune.embress.retrofit.response.ConnectionResponse;
import com.masterstrokepune.embress.retrofit.response.CustomerDetailsResponse;
import com.masterstrokepune.embress.retrofit.response.DocumentListModel;
import com.masterstrokepune.embress.retrofit.response.ForgotPasswordResponse;
import com.masterstrokepune.embress.retrofit.response.GenerateReceiptResponse;
import com.masterstrokepune.embress.retrofit.response.LoginResponse;
import com.masterstrokepune.embress.retrofit.response.MeterReadingResponse;
import com.masterstrokepune.embress.retrofit.response.ProjectListResponse;
import com.masterstrokepune.embress.retrofit.response.RegistrationResponse;
import com.masterstrokepune.embress.retrofit.response.TicketResponse;
import com.masterstrokepune.embress.retrofit.response.UploadImageResponse;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitService {

    String sessionId;
    private static RetrofitService ourInstance ;
    ApiService mApi;
    private Context mcontext;

    public static RetrofitService getInstance(Context mContext) {
        if(ourInstance==null){
            ourInstance = new RetrofitService(mContext);
        }
        return ourInstance;
    }

    private RetrofitService(Context context) {
        //Creating an object of our api interface
        mcontext  = context;
        mApi = RetroClient.getApiService(context);
    }

    public void checkLogin(String userName, String password,
                           final RetrofitServiceListener mListener){
        final LoginResponse info = new LoginResponse();
        if(TextUtils.isEmpty(userName)){
            mListener.onFailure(info,null);
            return;
        }
        mApi = RetroClient.getApiService(mcontext);
        mListener.onRequestStarted(null);
        Call<LoginResponse> call = mApi.checkLogin(new LoginRequest(userName,password));
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    LoginResponse userInfo  = response.body();
                    mListener.onResponse(userInfo);
                }else{
                    mListener.onFailure(info,null);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void getMobileVerificationStatus(MobileVerificationRequest request,
                                     final RetrofitServiceListener mListener){
        final LoginResponse info = new LoginResponse();
        mApi = RetroClient.getApiService(mcontext);
        mListener.onRequestStarted(null);
        Call<ConnectionResponse> call = mApi.getMobileVerification(request);
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<ConnectionResponse>() {
            @Override
            public void onResponse(Call<ConnectionResponse> call, Response<ConnectionResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    ConnectionResponse responseBody  = response.body();
                    if(responseBody instanceof ConnectionResponse){
                        ConnectionResponse connectionResponse = (ConnectionResponse) responseBody;
                        mListener.onResponse(connectionResponse);
                    }/*else if(responseBody instanceof String){
                        String connectionResponse = (String) responseBody;
                        mListener.onResponse(connectionResponse);
                    }*/else{
                        Object obj1,obj2;
                        JSONObject jsonObj;
                        try {
                            jsonObj = new JSONObject(String.valueOf(response.body().toString()));
                            Gson gson = new Gson();
                            ConnectionResponse connectionResponse = (ConnectionResponse) gson.fromJson(String.valueOf(jsonObj),ConnectionResponse.class);
                            mListener.onResponse(connectionResponse);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //mListener.onResponse(userInfo);
                }else{
                    mListener.onFailure("Service Unavaliable, Please try again later",null);
                }
            }

            @Override
            public void onFailure(Call<ConnectionResponse> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void getCategoryList(final RetrofitServiceListener mListener){
        final CategoryResponse info = new CategoryResponse();
        mListener.onRequestStarted(null);
        mApi = RetroClient.getApiService(mcontext);
        Call<CategoryResponse> call = mApi.getCategoryList();
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    CategoryResponse userInfo  = response.body();
                    mListener.onResponse(userInfo);
                }else{
                    mListener.onFailure(info,null);
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void addComments(AddCommantRequest request,final RetrofitServiceListener mListener){
        final Object info = new Object();
        mApi = RetroClient.getApiService(mcontext);
        mListener.onRequestStarted(null);
        Call<Object> call = mApi.insertCommant(request);
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    Object userInfo  = response.body();
                    mListener.onResponse(userInfo);
                }else{
                    mListener.onFailure(info,null);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void getComplaintHistory(int pkid, final RetrofitServiceListener mListener){
        final ComplaintHistoryResponse info = new ComplaintHistoryResponse();
        mListener.onRequestStarted(null);
        mApi = RetroClient.getApiService(mcontext);
        Call<ComplaintHistoryResponse> call = mApi.getComplaintList(new ComplaintHistoryRequest(pkid));
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<ComplaintHistoryResponse>() {
            @Override
            public void onResponse(Call<ComplaintHistoryResponse> call, Response<ComplaintHistoryResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    ComplaintHistoryResponse userInfo  = response.body();
                    mListener.onResponse(userInfo);
                }else{
                    mListener.onFailure(info,null);
                }
            }

            @Override
            public void onFailure(Call<ComplaintHistoryResponse> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void addTicket(ComplantRequest request, final RetrofitServiceListener mListener){
        final TicketResponse info = new TicketResponse();
        mListener.onRequestStarted(null);
        Call<TicketResponse> call = mApi.addTicket(request);
        /**
         * Enqueue Callback will be call when get response...
         */
        mApi = RetroClient.getApiService(mcontext);
        call.enqueue(new Callback<TicketResponse>() {
            @Override
            public void onResponse(Call<TicketResponse> call, Response<TicketResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    TicketResponse userInfo  = response.body();
                    mListener.onResponse(userInfo);
                }else{
                    mListener.onFailure(info,null);
                }
            }

            @Override
            public void onFailure(Call<TicketResponse> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void register(RegisterRequest request,
                         final RetrofitServiceListener mListener){
        final LoginResponse info = new LoginResponse();
        mApi = RetroClient.getApiService(mcontext);
        mListener.onRequestStarted(null);
        Call<RegistrationResponse> call = mApi.register(request);
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    RegistrationResponse userInfo  = response.body();
                    mListener.onResponse(userInfo);
                }else{
                    mListener.onFailure(info,null);
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void getCustomerDetails(int custId,
                                   final RetrofitServiceListener mListener){
        final CustomerDetailsResponse info = new CustomerDetailsResponse();
        mApi = RetroClient.getApiService(mcontext);
        mListener.onRequestStarted(null);
        Call<CustomerDetailsResponse> call = mApi.getCustomerDetails(new CustomerDetailRequest(custId));
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<CustomerDetailsResponse>() {
            @Override
            public void onResponse(Call<CustomerDetailsResponse> call, Response<CustomerDetailsResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    CustomerDetailsResponse userInfo  = response.body();
                    mListener.onResponse(userInfo);
                }else{
                    mListener.onFailure(info,null);
                }
            }

            @Override
            public void onFailure(Call<CustomerDetailsResponse> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void forgotPassword(String userid,
                                   final RetrofitServiceListener mListener){
        final ForgotPasswordResponse info = new ForgotPasswordResponse();
        mApi = RetroClient.getApiService(mcontext);
        mListener.onRequestStarted(null);
        Call<ForgotPasswordResponse> call = mApi.forgotPassword(new ForgotPasswordRequest(userid));
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    ForgotPasswordResponse userInfo  = response.body();
                    mListener.onResponse(userInfo);
                }else{
                    mListener.onFailure(info,null);
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void forgotUserName(String userid,
                                   final RetrofitServiceListener mListener){
        final ForgotPasswordResponse info = new ForgotPasswordResponse();
        mApi = RetroClient.getApiService(mcontext);
        mListener.onRequestStarted(null);
        Call<ForgotPasswordResponse> call = mApi.forgotLoginName(new ForgotUserNameRequest(userid));
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    ForgotPasswordResponse userInfo  = response.body();
                    mListener.onResponse(userInfo);
                }else{
                    mListener.onFailure(info,null);
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void history(BillDetailRequest request,
                        final RetrofitServiceListener mListener){
        final LoginResponse info = new LoginResponse();

        mListener.onRequestStarted(null);
        mApi = RetroClient.getApiService(mcontext);
        Call<BillHistoryResponse> call = mApi.billHistory(request);
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<BillHistoryResponse>() {
            @Override
            public void onResponse(Call<BillHistoryResponse> call, Response<BillHistoryResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    BillHistoryResponse userInfo  = response.body();
                    mListener.onResponse(userInfo);
                }else{
                    mListener.onFailure(info,null);
                }
            }

            @Override
            public void onFailure(Call<BillHistoryResponse> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void addReceipt(GenerateReceiptRequest request,final String paymentId,
                           final RetrofitServiceListener mListener){
        final GenerateReceiptResponse info = new GenerateReceiptResponse();

//        "$id" -> "1"
//        "msg" -> "Receipt Generated successfully..Receipt No is"
//        "getpkReciptid" -> {Double@15217} 17448.0
        mListener.onRequestStarted(null);
        mApi = RetroClient.getApiService(mcontext);
        Call<GenerateReceiptResponse> call = mApi.addReceipt(request);
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<GenerateReceiptResponse>() {
            @Override
            public void onResponse(Call<GenerateReceiptResponse> call, Response<GenerateReceiptResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    new AsyncTask<Void,Void,Void>(){

                        @Override
                        protected Void doInBackground(Void... voids) {
                            RazorpayClient razorpay = null;
                            try {
                                razorpay = new RazorpayClient("rzp_live_7GG2IWz7oph7DN", "XwL4G7AfHdeAGzRFuinXcbBM");
                            } catch (RazorpayException e) {
                                e.printStackTrace();
                            }
                            try {
                                int convertedAmount= (int)request.getBillAmount()*100;
                                JSONObject captureRequest = new JSONObject();
                                captureRequest.put("amount", convertedAmount);
                                captureRequest.put("currency", "INR");

                                Payment payment = razorpay.Payments.capture(paymentId, captureRequest);
                                System.out.println("Payment Captured");
                            } catch (RazorpayException | JSONException e) {
                                // Handle Exception
                                System.out.println(e.getMessage());
                            }
                            return null;
                        }
                    }.execute();

                    GenerateReceiptResponse userInfo  = response.body();
                    mListener.onResponse(userInfo);
                }
            }

            @Override
            public void onFailure(Call<GenerateReceiptResponse> call, Throwable t) {
                mListener.onFailure(info,t);
            }
        });
    }

    public void updateFCM(NotificaionRequest request,
                          final RetrofitServiceListener mListener){
        final Object info = new Object();

        if(mListener!=null)
            mListener.onRequestStarted(null);
        mApi = RetroClient.getApiService(mcontext);
        Call<Object> call = mApi.updateFCM(request);
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    Object userInfo  = response.body();
                    if(mListener!=null)
                        mListener.onResponse(userInfo);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                if(mListener!=null)
                    mListener.onFailure(info,t);
            }
        });
    }

    public void addMeterReadingRequest(MeterReadingRequest request,
                                       final RetrofitServiceListener mListener){
        final String info = new String();

        if(mListener!=null)
            mListener.onRequestStarted(null);
        mApi = RetroClient.getApiService(mcontext);
        Call<MeterReadingResponse> call = mApi.addMeterReading(request);
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<MeterReadingResponse>() {
            @Override
            public void onResponse(Call<MeterReadingResponse> call, Response<MeterReadingResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    MeterReadingResponse obj  = response.body();
                    if(mListener!=null)
                        mListener.onResponse(obj);
                }
            }

            @Override
            public void onFailure(Call<MeterReadingResponse> call, Throwable t) {
                if(mListener!=null)
                    mListener.onFailure(info,t);
            }
        });
    }

    public void getBillDetails(BillInfoRequest request,
                               final RetrofitServiceListener mListener){
        final Object info = new Object();

        if(mListener!=null)
            mListener.onRequestStarted(null);
        mApi = RetroClient.getApiService(mcontext);
        Call<BillInfoResponse> call = mApi.getBillDetails(request);
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<BillInfoResponse>() {
            @Override
            public void onResponse(Call<BillInfoResponse> call, Response<BillInfoResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    BillInfoResponse obj  = response.body();
                    if(mListener!=null)
                        mListener.onResponse(obj);
                }
            }

            @Override
            public void onFailure(Call<BillInfoResponse> call, Throwable t) {
                if(mListener!=null)
                    mListener.onFailure(info,t);
            }
        });
    }

    public void getKycDocumentList(final RetrofitServiceListener mListener){
        final DocumentListModel info = new DocumentListModel();
        if(mListener!=null)
            mListener.onRequestStarted(null);
        mApi = RetroClient.getApiService(mcontext);
        Call<DocumentListModel> call = mApi.getKycDocuments();
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<DocumentListModel>() {
            @Override
            public void onResponse(Call<DocumentListModel> call, Response<DocumentListModel> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    DocumentListModel obj  = response.body();
                    if(mListener!=null)
                        mListener.onResponse(obj);
                }
            }

            @Override
            public void onFailure(Call<DocumentListModel> call, Throwable t) {
                if(mListener!=null)
                    mListener.onFailure(info,t);
            }
        });
    }

    public void getAreaList(final RetrofitServiceListener mListener){
        final AreaListResponse info = new AreaListResponse();
        if(mListener!=null)
            mListener.onRequestStarted(null);
        mApi = RetroClient.getApiService(mcontext);
        Call<AreaListResponse> call = mApi.getAreaList();
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<AreaListResponse>() {
            @Override
            public void onResponse(Call<AreaListResponse> call, Response<AreaListResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    AreaListResponse obj  = response.body();
                    if(mListener!=null)
                        mListener.onResponse(obj);
                }else{
                    if(mListener!=null)
                        mListener.onResponse("Servce Unavaliable please try again later");
                }
            }

            @Override
            public void onFailure(Call<AreaListResponse> call, Throwable t) {
                if(mListener!=null)
                    mListener.onFailure(info,t);
            }
        });
    }

    public void addNewCustomer(NewConnectionRequest request, final RetrofitServiceListener mListener){
        final Object info = new Object();
        Gson gson = new Gson();
        String req = gson.toJson(request);
        Log.d("REQ_E",gson.toJson(request));
        if(mListener!=null)
            mListener.onRequestStarted(null);
        mApi = RetroClient.getApiService(mcontext);
        Call<String> call = mApi.onNewCustomer(request);
        /**New
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    String obj  = response.body();
                    if(mListener!=null)
                        mListener.onResponse(obj);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if(mListener!=null)
                    mListener.onFailure(info,t);
            }
        });
    }

    public void getProjectList(ProjectRequest request,final RetrofitServiceListener mListener){
        final ProjectListResponse info = new ProjectListResponse();
        if(mListener!=null)
            mListener.onRequestStarted(null);
        mApi = RetroClient.getApiService(mcontext);
        Call<ProjectListResponse> call = mApi.getProjectList(request);
        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<ProjectListResponse>() {
            @Override
            public void onResponse(Call<ProjectListResponse> call, Response<ProjectListResponse> response) {

                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    ProjectListResponse obj  = response.body();
                    if(mListener!=null)
                        mListener.onResponse(obj);
                }
            }

            @Override
            public void onFailure(Call<ProjectListResponse> call, Throwable t) {
                if(mListener!=null)
                    mListener.onFailure(info,t);
            }
        });
    }

    public void upload(File file,int custId,double reading){
       // File file = new File(imageUri.getPath());

        RequestBody fbody = RequestBody.create(file,MediaType.parse("image/*"));

        RequestBody name = RequestBody.create(custId+"",MediaType.parse("text/plain"));

        RequestBody id = RequestBody.create(reading+"",MediaType.parse("text/plain"));
        mApi = RetroClient.getApiService(mcontext);
        Call<Object> call = mApi.upload(
                fbody,
                name,
                id);

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.d("","");
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d("","");
            }
        });
    }

    public void uploadMeterPicture(Context context, Uri fileUri, File file, int pkCUstomerId,double meterreading, RetrofitServiceListener mListener) {
        // create upload service client
        // create RequestBody instance from file

        RequestBody requestFile =
                RequestBody.create(file,MediaType.parse("multipart/form-data"));
        RequestBody custId = RequestBody.create(MediaType.parse("multipart/form-data"), ""+pkCUstomerId);
        RequestBody reading = RequestBody.create(MediaType.parse("multipart/form-data"), ""+meterreading);
        MultipartBody.Part body = MultipartBody.Part.createFormData("imageFile", file.getName(), requestFile);
        mApi = RetroClient.getApiService(mcontext);

        Call<Object> responseBodyCall = mApi.addRecord(body, custId,reading );
        responseBodyCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.v("Upload", "success");
                Object obj  = response.body();
                if(obj instanceof String) {
                    String resp = (String) obj;
                    if (mListener != null) {
                        MeterReadingResponse response1 = new MeterReadingResponse();
                        response1.setMsg(resp);
                        mListener.onResponse(response1);
                        }
                }else{
                    if (mListener != null) {
                        MeterReadingResponse response1 = new MeterReadingResponse();
                        response1.setMsg("");
                        mListener.onResponse(response1);
                    }
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.v("Upload", "success");
                if (mListener != null) {
                    MeterReadingResponse response1 = new MeterReadingResponse();
                    response1.setMsg("Something went Wrong..Please try again");
                    mListener.onResponse(response1);
                }
            }
        });

    }



    public String getRealPathFromURI(Uri contentUri) {
        Log.d("imin", "onClick: in image conversion");

        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = mcontext.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            Log.d("imin", "onClick: in image conversion try");

            return cursor.getString(column_index);
        } finally {
            Log.d("imin", "onClick: in image conversion finally");

            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void uploadDocument(File file,RetrofitServiceListener listener){
        RequestBody requestFile =
                RequestBody.create(file,MediaType.parse("multipart/form-data"));

        if(listener!=null){
                listener.onRequestStarted(null);
            }
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body = MultipartBody.Part.createFormData("imageFile", file.getName(), requestFile);
            mApi = RetroClient.getApiService(mcontext);

            Call<UploadImageResponse> responseBodyCall = mApi.uploadDocument(body);
        responseBodyCall.enqueue(new Callback<UploadImageResponse>() {
                @Override
            public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                if(response.isSuccessful()){
                    if(listener!=null){
                        try {
                            //UploadImageResponse res = response.body();
                            listener.onResponse(response.body());
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    Log.d("SUCCESS","SUCCESS");
                }else{
                    listener.onFailure("Unable to upload Image ",null);
                }
            }

            @Override
            public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                Log.d("FAIL","FAIL");
                if(listener!=null){
                    listener.onFailure(t.getLocalizedMessage(),t);
                }
            }
        });
    }

}
