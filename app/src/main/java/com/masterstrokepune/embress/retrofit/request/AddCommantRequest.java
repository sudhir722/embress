package com.masterstrokepune.embress.retrofit.request;

public class AddCommantRequest {
    int PkTicketId;
    String TicketSolution;
    int Flag;
    int PkCustomerid;
    int PkStatusId;

    public AddCommantRequest(int pkTicketId, String ticketSolution, int flag, int pkCustomerid, int pkStatusId) {
        PkTicketId = pkTicketId;
        TicketSolution = ticketSolution;
        Flag = flag;
        PkCustomerid = pkCustomerid;
        PkStatusId = pkStatusId;
    }

    public int getPkTicketId() {
        return PkTicketId;
    }

    public void setPkTicketId(int pkTicketId) {
        PkTicketId = pkTicketId;
    }

    public String getTicketSolution() {
        return TicketSolution;
    }

    public void setTicketSolution(String ticketSolution) {
        TicketSolution = ticketSolution;
    }

    public int getFlag() {
        return Flag;
    }

    public void setFlag(int flag) {
        Flag = flag;
    }

    public int getPkCustomerid() {
        return PkCustomerid;
    }

    public void setPkCustomerid(int pkCustomerid) {
        PkCustomerid = pkCustomerid;
    }

    public int getPkStatusId() {
        return PkStatusId;
    }

    public void setPkStatusId(int pkStatusId) {
        PkStatusId = pkStatusId;
    }
}
