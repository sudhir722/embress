package com.masterstrokepune.embress.retrofit.request;

public class MeterReadingRequest {

    int pkcustomerid;
    String metereading;
    String DocUrl;

    public MeterReadingRequest(int pkcustomerid, String metereading, String docUrl) {
        this.pkcustomerid = pkcustomerid;
        this.metereading = metereading;
        DocUrl = docUrl;
    }

    public int getPkcustomerid() {
        return pkcustomerid;
    }

    public void setPkcustomerid(int pkcustomerid) {
        this.pkcustomerid = pkcustomerid;
    }

    public String getMetereading() {
        return metereading;
    }

    public void setMetereading(String metereading) {
        this.metereading = metereading;
    }

    public String getDocUrl() {
        return DocUrl;
    }

    public void setDocUrl(String docUrl) {
        DocUrl = docUrl;
    }
}
