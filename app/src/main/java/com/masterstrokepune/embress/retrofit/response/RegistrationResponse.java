package com.masterstrokepune.embress.retrofit.response;

public class RegistrationResponse {

    String msg;
    String userMsg;

    public RegistrationResponse(String msg, String userMsg) {
        this.msg = msg;
        this.userMsg = userMsg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUserMsg() {
        return userMsg;
    }

    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }
}
