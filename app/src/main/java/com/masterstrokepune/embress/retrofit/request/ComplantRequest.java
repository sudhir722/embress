package com.masterstrokepune.embress.retrofit.request;

public class ComplantRequest {
        String CategoryName;
        String QueryExplanation;
        int PkCustomerid;

    public ComplantRequest(String categoryName, String queryExplamation, int pkCustomerid) {
        CategoryName = categoryName;
        QueryExplanation = queryExplamation;
        PkCustomerid = pkCustomerid;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getQueryExplamation() {
        return QueryExplanation;
    }

    public void setQueryExplamation(String queryExplamation) {
        QueryExplanation = queryExplamation;
    }

    public int getPkCustomerid() {
        return PkCustomerid;
    }

    public void setPkCustomerid(int pkCustomerid) {
        PkCustomerid = pkCustomerid;
    }
}
