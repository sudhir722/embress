package com.masterstrokepune.embress.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.masterstrokepune.embress.model.CategoryModel;
import com.masterstrokepune.embress.model.UserModel;

import java.util.List;

public class CategoryResponse {

    @SerializedName("$id")
    @Expose
    String id;
    @SerializedName("list")
    @Expose
    List<CategoryModel> categoryModelsList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<CategoryModel> getCategoryModelsList() {
        return categoryModelsList;
    }

    public void setCategoryModelsList(List<CategoryModel> categoryModelsList) {
        this.categoryModelsList = categoryModelsList;
    }
}
