package com.masterstrokepune.embress.retrofit.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.masterstrokepune.embress.model.DocInfo;

import java.io.Serializable;
import java.util.List;

public class NewConnectionRequest implements Serializable {
    int PkAreaId;

    @SerializedName("TowerNo")
    @Expose
    String tower;
    String ReasonForReject = "init";
    int PkProjectId;
    String CustomerName;
    String FatherName;
    String MotherName;
    String FlatNo;
    String  Address;
    String MobileNo;
    String EmailId;
    double SecuirityDeposite;
    String ChequeNo;
    String BankName;
    String ClearingDate;
    String PaymentMode = "Chq";
    int VerifiedStatus;
    @SerializedName("docList")
    @Expose
    List<DocumentRequest> docInfoList;

    public NewConnectionRequest() {
    }

    public String getTower() {
        return tower;
    }

    public void setTower(String tower) {
        this.tower = tower;
    }

    public NewConnectionRequest(int pkAreaId, int pkProjectId, String customerName, String fatherName, String motherName, String flatNo, String address, String mobileNo, String emailId, double securityDeposit, String chequeNo, String bankName, String clearningDate, String paymentMode, int verifiedStatus, List<DocumentRequest> docInfoList) {
        PkAreaId = pkAreaId;
        PkProjectId = pkProjectId;
        CustomerName = customerName;
        FatherName = fatherName;
        MotherName = motherName;
        FlatNo = flatNo;
        Address = address;
        MobileNo = mobileNo;
        EmailId = emailId;
        this.SecuirityDeposite = securityDeposit;
        ChequeNo = chequeNo;
        BankName = bankName;
        this.ClearingDate = clearningDate;
        PaymentMode = paymentMode;
        VerifiedStatus = verifiedStatus;
        this.docInfoList = docInfoList;
    }

    public int getPkAreaId() {
        return PkAreaId;
    }

    public void setPkAreaId(int pkAreaId) {
        PkAreaId = pkAreaId;
    }

    public int getPkProjectId() {
        return PkProjectId;
    }

    public void setPkProjectId(int pkProjectId) {
        PkProjectId = pkProjectId;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getMotherName() {
        return MotherName;
    }

    public void setMotherName(String motherName) {
        MotherName = motherName;
    }

    public String getFlatNo() {
        return FlatNo;
    }

    public void setFlatNo(String flatNo) {
        FlatNo = flatNo;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }



    public String getChequeNo() {
        return ChequeNo;
    }

    public void setChequeNo(String chequeNo) {
        ChequeNo = chequeNo;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }


    public double getSecuirityDeposite() {
        return SecuirityDeposite;
    }

    public void setSecuirityDeposite(double secuirityDeposite) {
        SecuirityDeposite = secuirityDeposite;
    }

    public String getClearingDate() {
        return ClearingDate;
    }

    public void setClearingDate(String clearingDate) {
        ClearingDate = clearingDate;
    }

    public String getPaymentMode() {
        return PaymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        PaymentMode = paymentMode;
    }

    public int getVerifiedStatus() {
        return VerifiedStatus;
    }

    public void setVerifiedStatus(int verifiedStatus) {
        VerifiedStatus = verifiedStatus;
    }

    public List<DocumentRequest> getDocInfoList() {
        return docInfoList;
    }

    public void setDocInfoList(List<DocumentRequest> docInfoList) {
        this.docInfoList = docInfoList;
    }
}
