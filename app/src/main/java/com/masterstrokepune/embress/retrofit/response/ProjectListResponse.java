package com.masterstrokepune.embress.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProjectListResponse {

    @SerializedName("project")
    @Expose
    List<ProjectInfo> projectList;

    public List<ProjectInfo> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<ProjectInfo> projectList) {
        this.projectList = projectList;
    }

    public int getProjectId(int index){
        return this.projectList.get(index).getPkProjectId();
    }
    public String getProject(int index){
        return this.projectList.get(index).getPkProjectName();
    }

    class ProjectInfo{
        int pkProjectId;
        String pkProjectName;

        public int getPkProjectId() {
            return pkProjectId;
        }

        public void setPkProjectId(int pkProjectId) {
            this.pkProjectId = pkProjectId;
        }

        public String getPkProjectName() {
            return pkProjectName;
        }

        public void setPkProjectName(String pkProjectName) {
            this.pkProjectName = pkProjectName;
        }
    }
}
