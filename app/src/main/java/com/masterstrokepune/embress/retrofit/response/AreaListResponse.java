package com.masterstrokepune.embress.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AreaListResponse {

    @SerializedName("area")
    @Expose
    List<AreaInfo> areaList;

    public List<AreaInfo> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<AreaInfo> areaList) {
        this.areaList = areaList;
    }
    public String getArea(int position){
        return this.getAreaList().get(position).AreaName;
    }
    public int getAreaId(int position){
        if(position<=this.getAreaList().size())
            return this.getAreaList().get(position).PkAreaId;
        return 0;
    }

    class AreaInfo{
        int PkAreaId;
        String AreaName;

        public int getPkAreaId() {
            return PkAreaId;
        }

        public void setPkAreaId(int pkAreaId) {
            PkAreaId = pkAreaId;
        }

        public String getAreaName() {
            return AreaName;
        }

        public void setAreaName(String areaName) {
            AreaName = areaName;
        }
    }
}
