package com.masterstrokepune.embress.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.masterstrokepune.embress.model.UserModel;

public class LoginResponse {


    @SerializedName("msg")
    @Expose
    String message;
    @SerializedName("customerdetails")
    @Expose
    UserModel data;

    public LoginResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserModel getData() {
        return data;
    }

    public void setData(UserModel data) {
        this.data = data;
    }
}
