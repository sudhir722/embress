package com.masterstrokepune.embress.retrofit.request;

public class MobileVerificationRequest {
    String MobileNo;

    public MobileVerificationRequest(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }
}
