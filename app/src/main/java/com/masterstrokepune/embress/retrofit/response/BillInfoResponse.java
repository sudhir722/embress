package com.masterstrokepune.embress.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.masterstrokepune.embress.model.BillChargesModel;
import com.masterstrokepune.embress.model.BillInfoModel;

import java.util.List;

public class BillInfoResponse {

    @SerializedName("list")
    @Expose
    List<BillChargesModel> models;

    public List<BillChargesModel> getModels() {
        return models;
    }

    public void setModels(List<BillChargesModel> models) {
        this.models = models;
    }
}
