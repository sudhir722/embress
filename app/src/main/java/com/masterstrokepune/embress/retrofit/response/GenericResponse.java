package com.masterstrokepune.embress.retrofit.response;

public class GenericResponse {
    int requestCode;
    String message;

    public GenericResponse(int requestCode, String message) {
        this.requestCode = requestCode;
        this.message = message;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
