package com.masterstrokepune.embress.retrofit.request;

public class ForgotPasswordRequest {

    String UserName;

    public ForgotPasswordRequest(String userName) {
        UserName = userName;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }
}
