package com.masterstrokepune.embress.retrofit.request;

public class BillDetailRequest {
    int GetBillDetailsId;

    public BillDetailRequest(int getBillDetailsId) {
        GetBillDetailsId = getBillDetailsId;
    }

    public int getGetBillDetailsId() {
        return GetBillDetailsId;
    }

    public void setGetBillDetailsId(int getBillDetailsId) {
        GetBillDetailsId = getBillDetailsId;
    }
}
