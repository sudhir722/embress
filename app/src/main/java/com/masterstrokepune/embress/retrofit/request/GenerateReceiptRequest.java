package com.masterstrokepune.embress.retrofit.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenerateReceiptRequest {

    @SerializedName("ConsumerNo")
    @Expose
    String consumerNo;

    @SerializedName("BillNo")
    @Expose
    int billNo;

    @SerializedName("BillAmount")
    @Expose
    double BillAmount;

    @SerializedName("Narration")
    @Expose
    String narration;

    @SerializedName("Amount")
    @Expose
    double amount;

    @SerializedName("PkCustomerId")
    @Expose
    int custId;

    @SerializedName("PkReceiptvoucherdetailId")
    @Expose
    int pkReceiptId;

    public GenerateReceiptRequest(String consumerNo, int billNo, double billAmount, String narration, double amount, int custId, int pkReceiptId) {
        this.consumerNo = consumerNo;
        this.billNo = billNo;
        BillAmount = billAmount;
        this.narration = narration;
        this.amount = amount;
        this.custId = custId;
        this.pkReceiptId = pkReceiptId;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public int getBillNo() {
        return billNo;
    }

    public void setBillNo(int billNo) {
        this.billNo = billNo;
    }

    public double getBillAmount() {
        return BillAmount;
    }

    public void setBillAmount(double billAmount) {
        BillAmount = billAmount;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public int getPkReceiptId() {
        return pkReceiptId;
    }

    public void setPkReceiptId(int pkReceiptId) {
        this.pkReceiptId = pkReceiptId;
    }
}
