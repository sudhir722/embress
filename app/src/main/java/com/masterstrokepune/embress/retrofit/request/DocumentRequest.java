package com.masterstrokepune.embress.retrofit.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentRequest {
    String MobileNo;
    int docid;
    String DocUrl;
    int AreaId;
    @SerializedName("ProjectId")
    @Expose
    int PkProjectId;
    int Verifiedstatus;

    public DocumentRequest() {
    }

    public DocumentRequest(String mobileNo, int docid, String url, int areaId, int pkProjectId, int verifiedStatus) {
        MobileNo = mobileNo;
        this.docid = docid;
        this.DocUrl = url;
        this.AreaId = areaId;
        PkProjectId = pkProjectId;
        this.Verifiedstatus = verifiedStatus;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public int getDocid() {
        return docid;
    }

    public void setDocid(int docid) {
        this.docid = docid;
    }

    public String getDocUrl() {
        return DocUrl;
    }

    public void setDocUrl(String docUrl) {
        DocUrl = docUrl;
    }

    public int getAreaId() {
        return AreaId;
    }

    public void setAreaId(int areaId) {
        AreaId = areaId;
    }

    public int getPkProjectId() {
        return PkProjectId;
    }

    public void setPkProjectId(int pkProjectId) {
        PkProjectId = pkProjectId;
    }

    public int getVerifiedstatus() {
        return Verifiedstatus;
    }

    public void setVerifiedstatus(int verifiedstatus) {
        Verifiedstatus = verifiedstatus;
    }
}
