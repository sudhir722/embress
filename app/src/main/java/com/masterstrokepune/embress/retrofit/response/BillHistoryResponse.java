package com.masterstrokepune.embress.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.masterstrokepune.embress.model.BillDetailModel;

import java.util.List;

public class BillHistoryResponse {

    String msg;

    @SerializedName("list")
    @Expose
    List<BillDetailModel> billList;

    public BillHistoryResponse(String msg, List<BillDetailModel> billList) {
        this.msg = msg;
        this.billList = billList;
    }

    public String getMsg() {
        return msg;
    }

    public List<BillDetailModel> getBillList() {
        return billList;
    }
}
