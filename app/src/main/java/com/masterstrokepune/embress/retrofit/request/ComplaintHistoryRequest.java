package com.masterstrokepune.embress.retrofit.request;

public class ComplaintHistoryRequest {
    int pkcustomerid;

    public ComplaintHistoryRequest(int pkcustomerid) {
        this.pkcustomerid = pkcustomerid;
    }

    public int getPkcustomerid() {
        return pkcustomerid;
    }

    public void setPkcustomerid(int pkcustomerid) {
        this.pkcustomerid = pkcustomerid;
    }
}
