package com.masterstrokepune.embress.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.masterstrokepune.embress.model.ComplaintModel;
import com.masterstrokepune.embress.model.TicketList;

import java.util.List;

public class ComplaintHistoryResponse {

    @SerializedName("TicketlistIndetails")
    @Expose
    List<ComplaintModel> complaintModelList;

    @SerializedName("Ticketlist")
    @Expose
    List<TicketList> ticketList;

    public List<ComplaintModel> getComplaintModelList() {
        return complaintModelList;
    }

    public void setComplaintModelList(List<ComplaintModel> complaintModelList) {
        this.complaintModelList = complaintModelList;
    }

    public List<TicketList> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<TicketList> ticketList) {
        this.ticketList = ticketList;
    }
}
