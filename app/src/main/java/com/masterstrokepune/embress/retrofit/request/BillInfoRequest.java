package com.masterstrokepune.embress.retrofit.request;

public class BillInfoRequest {
    int pkcustomerid;

    public BillInfoRequest(int pkcustomerid) {
        this.pkcustomerid = pkcustomerid;
    }

    public int getPkcustomerid() {
        return pkcustomerid;
    }

    public void setPkcustomerid(int pkcustomerid) {
        this.pkcustomerid = pkcustomerid;
    }
}
