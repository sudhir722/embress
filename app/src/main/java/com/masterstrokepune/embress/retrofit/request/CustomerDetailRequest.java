package com.masterstrokepune.embress.retrofit.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerDetailRequest {

    @SerializedName("GetCustomerDetailsId")
    @Expose
    private int custId;

    public CustomerDetailRequest(int custId) {
        this.custId = custId;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }
}
