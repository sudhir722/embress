package com.masterstrokepune.embress.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//        "$id" -> "1"
//        "msg" -> "Receipt Generated successfully..Receipt No is"
//        "getpkReciptid" -> {Double@15217} 17448.0
public class GenerateReceiptResponse {

    @SerializedName("$id")
    @Expose
    String id;
    @SerializedName("msg")
    @Expose
    String message;
    @SerializedName("getpkReciptid")
    @Expose
    double receiptId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(double receiptId) {
        this.receiptId = receiptId;
    }
}
