package com.masterstrokepune.embress.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.masterstrokepune.embress.model.DocInfo;

import java.util.List;

public class DocumentListModel {

    @SerializedName("docdetails")
    @Expose
    List<DocInfo> docdetails;

    public List<DocInfo> getDocdetails() {
        return docdetails;
    }

    public void setDocdetails(List<DocInfo> docdetails) {
        this.docdetails = docdetails;
    }


}
