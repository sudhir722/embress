package com.masterstrokepune.embress.retrofit.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.masterstrokepune.embress.model.CustomerInfo;

public class CustomerDetailsResponse {

    @SerializedName("msg")
    @Expose
    private String message;

    @SerializedName("customerdetails")
    @Expose
    private CustomerInfo customerInfo;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(CustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }
}
