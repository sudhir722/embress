package com.masterstrokepune.embress.retrofit.request;

public class ProjectRequest {
    int PkAreaId;

    public ProjectRequest(int pkAreaId) {
        PkAreaId = pkAreaId;
    }

    public int getPkAreaId() {
        return PkAreaId;
    }

    public void setPkAreaId(int pkAreaId) {
        PkAreaId = pkAreaId;
    }


}
