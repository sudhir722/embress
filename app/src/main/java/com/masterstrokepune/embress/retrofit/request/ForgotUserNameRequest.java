package com.masterstrokepune.embress.retrofit.request;

public class ForgotUserNameRequest {
    String EmailId;

    public ForgotUserNameRequest(String emailId) {
        EmailId = emailId;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }
}
