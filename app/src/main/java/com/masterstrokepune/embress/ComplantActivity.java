package com.masterstrokepune.embress;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.masterstrokepune.embress.adapter.ComplaintHistoryAdapter;
import com.masterstrokepune.embress.bs.BSLogoutDialogFragment;
import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.iface.onClick;
import com.masterstrokepune.embress.model.ComplaintModel;
import com.masterstrokepune.embress.model.TicketList;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.response.ComplaintHistoryResponse;
import com.masterstrokepune.embress.retrofit.response.GenericResponse;
import com.masterstrokepune.embress.util.EmbracePref;
import com.masterstrokepune.embress.util.LocalConstance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.masterstrokepune.embress.util.LocalConstance.IS_LOYOUT;

public class ComplantActivity extends BaseActivity implements View.OnClickListener, onClick, RetrofitServiceListener, PopupMenu.OnMenuItemClickListener {

    FloatingActionButton floating_action_button;
    ComplaintHistoryAdapter eventAdapter;
    List<TicketList> list = new ArrayList<>();
    private RecyclerView eventRecycler;
    RetrofitServiceListener mServiceListener;
    TextView txtAddComplaint;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complant);
        floating_action_button = findViewById(R.id.floating_action_button);
        txtAddComplaint = findViewById(R.id.comp);
        txtAddComplaint.setOnClickListener(this);
        floating_action_button.setOnClickListener(this);
        eventRecycler = findViewById(R.id.recyclerView);
        mServiceListener = this;
        RetrofitService.getInstance(getApplicationContext()).getComplaintHistory(EmbracePref.getCustomerId(getApplicationContext()),mServiceListener);

    }


    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        popup.setOnMenuItemClickListener(this);
        inflater.inflate(R.menu.menu_main, popup.getMenu());
        popup.show();
    }

    public void init(){
        eventAdapter = new ComplaintHistoryAdapter(getApplicationContext(),list,this);
        eventRecycler.setAdapter(eventAdapter);
        eventRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        eventAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(getApplicationContext(),AddComplentActivity.class));
    }

    @Override
    public void onOkClicked(Object object) {
        hideDialog();
        if(object instanceof TicketList){
            TicketList list = (TicketList) object;
            Intent intent = new Intent(getApplicationContext(),TicketDetails.class);
            intent.putExtra("list", (Serializable) list.getComplaintModelList());
            intent.putExtra("extras", list.getQueryexplanation());
            intent.putExtra("date", list.getDate());
            intent.putExtra("tid", list.getPkTicketDetailsId());
            startActivity(intent);
        }
    }

    @Override
    public void onRequestStarted(Object mObject) {
        showProgressDialog("");
    }

    @Override
    public void onResponse(Object mObject) {
        hideDialog();
        if(mObject instanceof ComplaintHistoryResponse){
            ComplaintHistoryResponse mResponse = (ComplaintHistoryResponse) mObject;
            for (int index=0; index<mResponse.getTicketList().size();index++){
                TicketList ticket = mResponse.getTicketList().get(index);
                for(int j=0;j<mResponse.getComplaintModelList().size();j++) {
                    ComplaintModel comments = mResponse.getComplaintModelList().get(j);
                    if (ticket.getPkTicketDetailsId()==comments.getPkTicketDetailsId()){
                        ticket.setStatus(comments.getStatusName());
                        if(ticket.getComplaintModelList()==null){
                            ticket.setComplaintModelList(new ArrayList<ComplaintModel>());
                        }
                        ticket.getComplaintModelList().add(comments);
                    }
                }
            }
            list.clear();
            list.addAll(mResponse.getTicketList());
            init();
        }
    }

    @Override
    public void onFailure(Object mObject, Throwable t) {

    }

    public void showLogOutDialog(){
        BSLogoutDialogFragment bottomSheetFragment = new BSLogoutDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LocalConstance.CONST_TITLE, getString(R.string.app_name));
        bundle.putString(LocalConstance.CONST_MESSAGE, getString(R.string.txt_logout_message));
        bundle.putSerializable(LocalConstance.CONST_LISTENER, new OnDialogClickListener() {
            @Override
            public void onOkClicked(Object object) {
                GenericResponse response = (GenericResponse) object;
                if(((GenericResponse) object).getRequestCode()==IS_LOYOUT){
                    EmbracePref.clear(getApplicationContext());
                    //navigateLogin();
                    finish();
                }
            }

            @Override
            public void onCancelClicked() {

            }
        });
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        showLogOutDialog();
        return false;
    }
}