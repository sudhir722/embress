package com.masterstrokepune.embress;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.masterstrokepune.embress.adapter.MenuAdapter;
import com.masterstrokepune.embress.fragments.ContactInfoFragment;
import com.masterstrokepune.embress.fragments.DepositFragment;
import com.masterstrokepune.embress.fragments.DocumentListFragment;
import com.masterstrokepune.embress.fragments.PersonalInfoFragment;
import com.masterstrokepune.embress.iface.OnDialogClickListener;
import com.masterstrokepune.embress.iface.onMenuClicked;
import com.masterstrokepune.embress.model.DocInfo;
import com.masterstrokepune.embress.model.MenuInfo;
import com.masterstrokepune.embress.model.MenuModel;
import com.masterstrokepune.embress.retrofit.RetrofitService;
import com.masterstrokepune.embress.retrofit.RetrofitServiceListener;
import com.masterstrokepune.embress.retrofit.request.DocumentRequest;
import com.masterstrokepune.embress.retrofit.request.NewConnectionRequest;
import com.masterstrokepune.embress.retrofit.response.GenericResponse;
import com.masterstrokepune.embress.util.EmbracePref;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.masterstrokepune.embress.util.LocalConstance.IS_LOYOUT;

public class NewCustomerActivity extends BaseActivity implements onMenuClicked, RetrofitServiceListener, OnDialogClickListener {

    onMenuClicked mMenuClickListener;
    List<MenuInfo> mFilterList = new ArrayList<>();
    MenuAdapter mFilterAdapter;
    ImageView icon;
    boolean isExisting;
    NewConnectionRequest mConnectionRequest = new NewConnectionRequest();

    public  void rotateImage(){
        RotateAnimation rotate = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(5000);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setInterpolator(new LinearInterpolator());

        icon.startAnimation(rotate);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);

        icon = findViewById(R.id.action_home);
        rotateImage();

        mMenuClickListener = this;

        if(getIntent()!=null && getIntent().getExtras()!=null && getIntent().getExtras().containsKey("existing")){
            isExisting = getIntent().getExtras().getBoolean("existing");
        }

        ((TextView)findViewById(R.id.usertype)).setText(EmbracePref.getDisplayName(this));

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setHomeScreen();
            }
        });
        initMenu();

        setHomeScreen();
        //RetrofitService.getInstance(getApplicationContext()).getKycDocumentList(this);

    }




    public void setHomeScreen(){
        PersonalInfoFragment fragmentDemo = new PersonalInfoFragment();
        //above part is to determine which fragment is in your frame_container
        setFragment(fragmentDemo);
        if(mFilterList!=null){
            for (int index=0;index<mFilterList.size();index++){
                mFilterList.get(index).setSelection(false);
            }
        }
        if(mFilterAdapter!=null){
            mFilterAdapter.notifyDataSetChanged();
        }
    }



    // This could be moved into an abstract BaseActivity
    // class for being re-used by several instances
    protected void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.home_fragment, fragment);
        fragmentTransaction.commit();
    }


    public static final int MENU_PERSONAL_INFO = 0;
    public static final int MENU_CONTACT_INFO = 1;
    public static final int MENU_PAYMENT_INFO = 2;
    public static final int MENU_DOC = 3;


    public void initMenu(){
        if(mFilterList==null){
            mFilterList = new ArrayList<>();
        }
        mFilterList.clear();
        mFilterList.add(new MenuInfo("PERSONAL INFO","#fcf3cf", "#fcf3cf",R.drawable.ic_pd, MENU_PERSONAL_INFO));
        mFilterList.add(new MenuInfo("CONTACT INFO","#d1f2eb", "#d1f2eb",R.drawable.ic_information, MENU_CONTACT_INFO));
        mFilterList.add(new MenuInfo("UPLOAD DOCUMENT","#d1f2eb", "#d1f2eb",R.drawable.ic_uploaddoc, MENU_DOC));
        mFilterList.add(new MenuInfo("PAYMENT INFO","#E8F5E9", "#E8F5E9",R.drawable.ic_payment, MENU_PAYMENT_INFO));



        RecyclerView mRecycler = findViewById(R.id.rec_menu);
        mFilterAdapter = new MenuAdapter(getApplicationContext(), mFilterList, mMenuClickListener);
        mRecycler.setAdapter(mFilterAdapter);
        setSelection(0);

        mRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
    }

    private List<MenuModel> getAllMenus() {
        List<MenuModel> list = new ArrayList<>();
        return list;
    }

    public void onPersonalDetailSaved(String customerName,String fatherName,String motherName){
        mConnectionRequest.setPkAreaId(EmbracePref.getAreaId(getApplicationContext()));
        mConnectionRequest.setPkProjectId(EmbracePref.getProjectId(getApplicationContext()));
        mConnectionRequest.setCustomerName(customerName);
        mConnectionRequest.setFatherName(fatherName);
        mConnectionRequest.setMotherName(motherName);
        setFragment(new ContactInfoFragment());
        setSelection(1);
    }

    public void onContactDetailSaved(String towerno,String flatno, String address, String mobile,String email) {
        mConnectionRequest.setTower(towerno);
        mConnectionRequest.setFlatNo(flatno);
        mConnectionRequest.setAddress(address);
        mConnectionRequest.setEmailId(email);
        mConnectionRequest.setMobileNo(mobile);
        setFragment(new DocumentListFragment());
        setSelection(2);
    }
    public void onDocumentUploaded(List<DocInfo> docInfoList) {

        List<DocumentRequest> docList = new ArrayList<>();
        boolean isAllDocsUploaded = true;
        for (int index=0;index<docInfoList.size();index++){
            if(TextUtils.isEmpty(docInfoList.get(index).getDocumentUrl()))
                isAllDocsUploaded = false;
            docList.add(new DocumentRequest(mConnectionRequest.getMobileNo(),docInfoList.get(index).getPkDocumentId(),
                    docInfoList.get(index).getDocumentUrl(),
                    EmbracePref.getAreaId(getApplicationContext()),
                    EmbracePref.getProjectId(getApplicationContext()),0));
        }
        mConnectionRequest.setDocInfoList(docList);
        if(isAllDocsUploaded) {
            setFragment(new DepositFragment());
            setSelection(3);
        }
    }

    public void onDeposited(double amount,String chequeNumber,String bankName) {
        mConnectionRequest.setSecuirityDeposite(amount);
        mConnectionRequest.setChequeNo(chequeNumber);
        mConnectionRequest.setBankName(bankName);
        mConnectionRequest.setPaymentMode("Chq");
        SimpleDateFormat sdf =new SimpleDateFormat("MM-dd-yyyy");
        mConnectionRequest.setClearingDate(sdf.format(new Date()));
        if(isValidForm()) {
            RetrofitService.getInstance(getApplicationContext()).addNewCustomer(mConnectionRequest, this);
        }
    }

    public void showMessage(String message){
        showErrorMessageSheet(getSupportFragmentManager(),"Alert",message);
    }

    private boolean isValidForm() {
        if(TextUtils.isEmpty(mConnectionRequest.getCustomerName())){
            showMessage("Please Enter Customer Name");
            return false;
        }/*else if(TextUtils.isEmpty(mConnectionRequest.getFatherName())){
            showMessage("Please Enter Father Name");
            return false;
        }else if(TextUtils.isEmpty(mConnectionRequest.getMotherName())){
            showMessage("Please Enter Moher Name");
            return false;
        }*/else if(TextUtils.isEmpty(mConnectionRequest.getFlatNo())){
            showMessage("Please Enter Flat Number");
            return false;
        }else if(TextUtils.isEmpty(mConnectionRequest.getAddress())){
            showMessage("Please Enter Address");
            return false;
        }else if(TextUtils.isEmpty(mConnectionRequest.getMobileNo())){
            showMessage("Please Enter Mobile Number");
            return false;
        }else if(TextUtils.isEmpty(mConnectionRequest.getEmailId())){
            showMessage("Please Enter Email Address");
            return false;
        }else if(!isAllDocumentsUploaded()){
            showMessage("Please Submit your Document");
            return false;
        }else if(TextUtils.isEmpty(mConnectionRequest.getBankName())){
            showMessage("Please Enter Bank Name");
            return false;
        }else if(TextUtils.isEmpty(mConnectionRequest.getChequeNo())){
            showMessage("Please Enter Cheque Number");
            return false;
        }
        return true;
    }

    private boolean isAllDocumentsUploaded() {
        boolean flag = true;
        List<DocumentRequest> docList = mConnectionRequest.getDocInfoList();
        int uploadedDocCount = 0;

        if(docList!=null){

            for (int index=0;index<docList.size();index++){
                if(docList.get(index).getDocid()==1){
                    if(TextUtils.isEmpty(docList.get(index).getDocUrl())){
                        flag=false;
                        break;
                    }
                }
                if(!TextUtils.isEmpty(docList.get(index).getDocUrl())){
                    uploadedDocCount++;
                }
            }
        }
        if(flag && uploadedDocCount>=1){
            flag =true;
        }else{
            flag=false;
        }
        return flag;
    }

    public void setSelection(int position){
        if(mFilterList!=null && mFilterList.size()>0) {
            for (int index=0;index<mFilterList.size();index++) {
                mFilterList.get(index).setSelection(false);
                if(index == position) {
                    mFilterList.get(index).setSelection(true);

                }
            }
            mFilterAdapter.notifyDataSetChanged();
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onMenuClick(MenuInfo filtersInfo) {

        if(mFilterList!=null && mFilterList.size()>0) {
            for (int index=0;index<mFilterList.size();index++) {
                if(mFilterList.get(index).getMenuId() == filtersInfo.getMenuId()) {
                    mFilterList.get(index).setSelection(true);
                    mFilterAdapter.notifyDataSetChanged();
                }
            }
        }
        switch (filtersInfo.getMenuId()){
            case MENU_PERSONAL_INFO:
                setFragment(PersonalInfoFragment.newInstance(mConnectionRequest));
                break;
            case MENU_CONTACT_INFO:
                setFragment(ContactInfoFragment.newInstance(mConnectionRequest));
                break;
            case MENU_PAYMENT_INFO:
                setFragment(new DepositFragment());
                break;
             case MENU_DOC:
                setFragment(new DocumentListFragment());
                break;
        }
    }

    @Override
    public void onRequestStarted(Object mObject) {
       // showProgressDialog("");
    }

    @Override
    public void onResponse(Object mObject) {
        if(mObject instanceof String){
            String response = (String) mObject;
            showErrorMessageSheet(getSupportFragmentManager(), "Alert", response, true, new OnDialogClickListener() {
                @Override
                public void onOkClicked(Object object) {
                    finish();
                }

                @Override
                public void onCancelClicked() {

                }
            });
        }

    }

    @Override
    public void onFailure(Object mObject, Throwable t) {

    }

    @Override
    public void onOkClicked(Object object) {
        if(object instanceof GenericResponse){
            GenericResponse response = (GenericResponse) object;
            if(((GenericResponse) object).getRequestCode()==IS_LOYOUT){
                EmbracePref.clear(getApplicationContext());
                //navigateLogin();
                finish();
            }
        }
    }

    @Override
    public void onCancelClicked() {

    }


    public void showprofile(View view) {

    }

    public void onaboutus(View view) {

    }


}
